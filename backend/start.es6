var Varavel= core.org.voxsoftware.Varavel
if(!Varavel)
	Varavel= require("varavel")
var Fs= core.System.IO.Fs
import Path from 'path'
var func= async ()=>{
	try{
    	global.appDir= __dirname+"/.."
    	global.projectDir= __dirname + "/../backend-dist"
		var f= Path.join(global.projectDir, "env.dt")
		if(!Fs.sync.exists(f)){
			Fs.sync.writeFile(f, Fs.sync.readFile(Path.join(global.appDir, "backend", "env.dt")))
		}
		await Varavel.Project.Application.start(global.projectDir)
		if(global.serverStarted)
			global.serverStarted()
	}
	catch(e){
		vw.error(e)
	}

}
func()
