/*global core,vw,Varavel*/
var he= core.VW.E6Html.HtmlEncode
var Fs= core.System.IO.Fs
class E6HtmlFile{
	constructor(file){
		this.file=file
		E6HtmlFile.cache[file]= this
	}
	
	static get(file){
		if(!E6HtmlFile.cache[file]){
			new E6HtmlFile(file)
		}
		return E6HtmlFile.cache[file]
	}
	
	base64decode(/*string */text, context){
		if(context &&context.transform){
			text= context.transform(text);
		}
		return new Buffer(text,'base64');
	}

	encode(/*string */text, context){
		if(context &&context.transform){
			text= context.transform(text);
		}
		return he.encode(text);
	}
	
	async invoke(context){
		
		if(!this.compiledTime){
			await this.compile(null,true)
		}
		else if(this.compiledTime && Date.now()-this.compiledTime> Varavel.Funcs.env("CACHE_STATIC_TIME", 60000)){
			await this.compile(true)
		}
		
		if(!context){
			// La salida va a la consola ...
			context= core.VW.E6Html.E6Html.createConsoleContext()
		}
		
		context.arguments= context.arguments||{}
		context.e6html= this
		var func= this.$func(context)
		return await func()
	}
	
	async compile_2(secure, newmode){
		try{
			var sto= new Varavel.Project.Modules.Api.Libraries.Storage()
			
			//var buk= sto.bucket()
			var url, req, response,code,parser,conversion
			vw.warning("FILE",this.file)
			
			/*
			var filew= buk.file(this.file)
			var utime=null
			var meta= await filew.getMetadata()
			meta=meta[0]
			meta.updated=new Date(meta.updated)
			meta.updated= meta.updated.getTime()
			*/
			
			var stat= await Fs.async.stat(this.file)
			stat.updated= stat.mtime.getTime()
			
			if(this.$func){
				if(this.lastMetatime && this.lastMetatime>= stat.updated){
					return 
				}
			}
			
			if(newmode && this.compiledTime){
				this.compiledTime= Date.now()
				return 
			}
				
			var utime= this.lastMetatime
			this.lastMetatime=meta.updated
			
			/*url= await filew.getSignedUrl({
				action:'read',
				expires: Date.now()+1000000
			})
			url=url[0]
			if(newmode && this.compiledTime)
				return 
				
			req= new core.VW.Http.Request(url)
			response=await req.getResponseAsync()
			code=response.body
			*/
			
			code= await Fs.async.readFile(this.file, 'utf8')
			this.compiledTime= Date.now()
			parser= new core.VW.E6Html.Parser()
			conversion= parser.parse(code)
			this.conversion=conversion
			
			this.$func= eval(conversion.code)
			conversion=null 
			code=null 
		
		}catch(e){
			if(utime!==null)
				this.lastMetatime= utime
				
			if(!secure)
				throw e
			else 
				vw.error("Error en archivo: ", e)
		}
	}
	
	async compile(secure, newmode){
		try{
			
			var parser, code, conversion
			if(this.compiling)
				return 
			this.compiling= true 
			/*var sto= new Varavel.Project.Modules.Api.Libraries.Storage()
			var buk= sto.bucket(), url, req, response,code,parser,conversion
			vw.warning("FILE",this.file)
			
			var utime=null
			
			var task= new core.VW.Task()
			var site= new Varavel.Project.App.Site()
			site.getFile({
				fullpath:this.file, 
				error404: function(){
					task.result=task.result||{}
					task.result.noexists=true
					task.finish()
				}, 
				error: function(e){
					task.exception= e
					task.finish()
				}, 
				success: function(r){
					task.result= r
					task.finish()
				}
			})
			var result= await task
			*/
			
			
			var utime=null
			
			/*
			var filew= buk.file(this.file)
			var utime=null
			var meta= await filew.getMetadata()
			meta=meta[0]
			meta.updated=new Date(meta.updated)
			meta.updated= meta.updated.getTime()
			*/
			var metaUpdated
			
			/*
			if(result.noexists)
				throw new core.System.Exception("FILE NOT FOUND: " + this.file)
			*/
			
			
			var stat= await Fs.async.stat(this.file)
			metaUpdated= stat.mtime.getTime()
			
			
			if(this.$func){
				if(this.lastMetatime && this.lastMetatime>= metaUpdated){
					
					return 
				}
			}
			
			if(newmode && this.compiledTime){
				this.compiledTime= Date.now()
				return 
			}
				
			utime= this.lastMetatime
			this.lastMetatime=metaUpdated
			/*url= await filew.getSignedUrl({
				action:'read',
				expires: Date.now()+1000000
			})
			url=url[0]
			*/
			if(newmode && this.compiledTime)
				return 
				
				
			/*req= new core.VW.Http.Request(url)
			response=await req.getResponseAsync()
			code=response.body*/
			
			code= await Fs.async.readFile(this.file,'utf8')
			
			this.compiledTime= Date.now()
			parser= new core.VW.E6Html.Parser()
			conversion= parser.parse(code)
			this.conversion=conversion
			this.$func= eval(conversion.code)
			conversion=null 
			code=null 
		
		}catch(e){
			
			if(utime!==null)
				this.lastMetatime= utime
				
			if(!secure)
				throw e
			else 
				vw.error("Error en archivo: ", e)
		}
		finally{
			this.compiling= false
		}
	}
	
}
E6HtmlFile.cache={}
export default E6HtmlFile
