/*global core */
var fs= require("fs")
var Path= require("path")
var Fs= core.System.IO.Fs

class E6HtmlFolder{
	
	constructor(folder){
		this.$folder= folder
	}
	
	async getFileName(file){
		var folder= this.$folder, exists
		var nf
		var file2 = Path.join(folder, file)
		nf=file2
		E6HtmlFolder.caching= E6HtmlFolder.caching||{}
		if(E6HtmlFolder.caching[file2])
			return E6HtmlFolder.caching[file2]
		
		
		if(!await Fs.async.exists(file2)){
			for(var i=0;i<E6HtmlFolder.extensions.length;i++){
				var ext= E6HtmlFolder.extensions[i]
				file2 = Path.join(folder, file+ext)
				
				if(await Fs.async.exists(file2)){
					exists=true;
					break;
				}
			}
		}
		
		if(!exists && !await Fs.async.exists(file2)){
			throw new core.System.IO.FileNotFoundException(file)
		}
		E6HtmlFolder.caching[nf]= file2
		return file2
		
		
	}
	
	async get(file){
		file=await this.getFileName(file)	
		return Varavel.Project.App.E6HtmlFile.get(file)
	}
	
	async invoke(file, context, args){
		var context= context || core.VW.E6Html.E6Html.createConsoleContext()
		context.arguments=args
		context.e6htmlFolder= this
		var f=await this.get(file)
		return await f.invoke(context)
	}
	
}
E6HtmlFolder.extensions=[".es6.html"]
export default E6HtmlFolder