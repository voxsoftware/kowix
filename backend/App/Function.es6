/* global vw,Varavel,core */

import Vm from 'vm'
var runInContext= function(script,options){
	return script.runInThisContext(options)
}
var runCodeInContext= function(code,options){
	return eval(code)
	return Vm.runInThisContext(code,options)
}

var VaravelNamespace= core.org.voxsoftware.Varavel

import Url from 'url'
var Fs= core.System.IO.Fs
import fs from 'fs'
import Path from 'path'
var times=0

import Module from 'module'

class Function {
	
	constructor(idFunction, context, token){
		this.idFunction=idFunction
		this.context= context
		this.token= null //token|| (context.context?context.context._body.dbtoken:"NULL")
		this.idSite= context.IdSite
	}
	
	
	
	

	async load(options){
		
		//var  code, cache, script, vars, 
		
		var func,compiledFunc, f1, rid
		this.idFunction= this.idFunction.replace(/\@/g, '/')
		rid= this.idSite + "."+ this.idFunction
		if(typeof this.idFunction=="string"){
			var site
			
			
			if(this.context.context){
				site= this.context.context.site 
				if(!site){
					site=Varavel.Project.App.Site.getFromArgs(this.context.context)
					await site.getRealSite()
				}
			}
			else{
				site= Varavel.Project.App.Site.getFromId(this.idSite)
				await site.getRealSite()
			}
			func= await site.getFunction(this.idFunction,undefined, options)
		}
		if(!func)
			throw Varavel.Project.App.Exception.create("The function "+this.idFunction+" doesn't exists").putCode("FUNCTION_NOT_EXISTS").end()
			
		
		this.loading= Date.now()
		
		/*
		if(!script){
			
			if(!this.context.module){
				this.context.require= require
				this.context.module= this.context.module || new Module(null,module)
				this.context.module.filename= this.functionFilename || func.file || (__dirname + "/" + rid)
			}
			
			if(!this.context.console){
				this.context.console= console
			}
			this.context.__dirname= Path.dirname(this.context.module.filename)
			this.context.__filename= this.context.module.filename
			
			
			
			vars=''
			for(var id in this.context){
				vars+=`var ${id}= global.${id};`
			}
			code=`
			(function(global){
			${vars}
			${code}
			})
			`
			func.jsCode= script= code
		}*/
		
		
		try{
			if(!(f1=func.compiled0)){
				delete require.cache[require.resolve(func.file)]
				f1= require(func.file)
				func.compiled0= f1
			}
			compiledFunc= func.compiled0(this.context)
		}
		catch(e){
			throw e
		}
		func.compiledFunc=compiledFunc
		this.func= func
	}




	async static availablePaths(){
		if(Function.avpath){
			return Function.avpath
		}

		var tname= Path.basename(global.appDir)
		var adir=[]
		var stat, ufile
		var projects= Path.normalize(Path.join(global.appDir, ".."))
		var dirs= await Fs.async.readdir(projects)
		for(var i=0;i<dirs.length;i++){
			ufile=Path.join(projects, dirs[i])
			if(dirs[i]!= tname){
				stat= await Fs.async.stat(ufile)
				if(stat.isDirectory())
					adir.push(ufile)
			}
		}
		adir.push("/usr")
		adir.push("/bin")
		adir.push("/var/log")
		return Function.avpath=adir

	}

	execute(){
		try{
			return this.func.compiledFunc.apply(this.compiledFunc, arguments)
		}catch(e){
			throw new core.System.Exception("Fail executing: " + this.idFunction + ", " + e.message)
		}
	}
	async invoke(body){
		//if(!this.loading || this.loading<Date.now()-200)
		await this.load(body )
		return await this.execute(body)
	}
}
Function.loadTimes={}
Function.script={}
Function.cacheFile={}
export default Function
