/*global Varavel,vw,core*/
var VaravelNamespace= core.org.voxsoftware.Varavel
import Url from 'url'
var Fs= core.System.IO.Fs
import fs from 'fs'
import Path from 'path'
var times=0
import {PluginManager} from 'live-plugin-manager'
import Os from 'os'



import Child from 'child_process'
import Semver from 'semver'


class UserContext {
	constructor(args,idsite){
		//this.args= args
		this.idSite= idsite//args.site? args.site.site : args.request.params.site || args.request.headers.host
	}
	
	static get(args,site){
		UserContext.c=UserContext.c||{}
		if(!UserContext.c[site])
			UserContext.c[site]= new UserContext(args,site)
		return UserContext.c[site]
	}
	
	getHomeDir(){
		var home= Os.homedir()
		var kowi= Path.join(home, 'kowi')
		if(!Fs.sync.exists(kowi)){
			Fs.sync.mkdir(kowi)
		}
		kowi= Path.join(kowi, "user-context")
		if(!Fs.sync.exists(kowi))
			Fs.sync.mkdir(kowi)


		var paths1= this.idSite.split("/")
		for(var i=0;i<paths1.length;i++){
			kowi= Path.join(kowi,paths1[i])
			if(!Fs.sync.exists(kowi))
				Fs.sync.mkdir(kowi)	
		}
		return kowi
	}
	
	
	getModeNpm(){
		
		return Varavel.Funcs.env("NPM_MODE",0)
		var path=Path.join(global.appDir, "..","npm")
		if(Fs.sync.exists(path))
			return path
		
		return 
	}
	
	async require(pack,version,file){
		return require(await this.resolve(pack,version,file))
	}
	async resolve(pack, version, file){

		//var args=this.args
		// user require enable that can be require custom modules for each user ...
		var home= process.env.HOME
		
		var packagefile, data, resolved
		var kowi= this.getHomeDir()


		packagefile= Path.join(kowi, "package.json")
		if(!version){
			version= pack.indexOf("@")
			if(version>=0){
				version=pack.substring(version+1)
				pack=pack.substring(0, version)
				if(version=="latest")
					version= "*"
			}
			else{
				version= "*"
			}
		}
		var needreplace, stat, stat2, needcreate
		if(!Fs.sync.exists(packagefile)){
			// create a default
			data= {
				name: "kowi-usercontext-"+ this.idSite.replace(/\//ig,'_'),
				dependencies: {}
			}
			data.dependencies[pack]= version
			needreplace= true
			needcreate=true
		}
		else{
			stat= UserContext.stats[packagefile]
			stat2= await Fs.async.stat(packagefile)
			if(stat){
				if(stat2.mtime.getTime()<= stat.mtime.getTime())
					data= stat
			}
			if(!data){
				data= JSON.parse(await Fs.async.readFile(packagefile))
				data.mtime= stat2.mtime
				UserContext.stats[packagefile]= data
			}
			data.dependencies=data.dependencies||[]
			if(data.dependencies[pack]){
				if(data.dependencies[pack]!=version){
					if(data.dependencies[pack].startsWith("^"))
						needreplace= !Semver.satisfies(data.dependencies[pack].substring(1),version)//data.dependencies[pack].substring(1)!=version
					else 
						needreplace= true
				}
			}
			else{
				needreplace= true
			}
			data.dependencies[pack]= version
		}

		if(file){
			file= Path.join(pack,file)
		}
		else{
			file= pack
		}
	
	
		resolved= Path.join(kowi, "node_modules", file)
		var r1
		if(!needreplace){
			try{
				r1= require.resolve(resolved)
				if(Fs.sync.exists(r1)){
					return r1
				}
			}catch(e){
				vw.info("ERROR:",e)
				if(r1)
					throw e
			}
			
		}


		// UPDATE FILE ...
		if(needcreate)
			await Fs.async.writeFile(packagefile, JSON.stringify(data))

		var task= new core.VW.Task()
		var p
		
		
		var mode= this.getModeNpm()
		
		if(mode===0){
			const manager = new PluginManager({
				pluginsPath: Path.join(kowi, "node_modules"),
				cwd: kowi,
				hostRequire:function(){
					
				}
			})
			await manager.install(pack, version)
		}
		else{
			if(mode){
				p= Child.spawn(process.argv[0], [mode + "/bin/npm-cli.js","install", pack+"@" + (version!='*'?version:"latest") ], {
					cwd: kowi
				})
			}
			else{
				p= Child.spawn("npm", ["install", pack+"@" + (version!='*'?version:"latest") ], {
					cwd: kowi
				})
			}
			p.stdout.on("data", function(data){
				vw.warning(data.toString())
			})
			p.stderr.on("data", function(data){
				vw.error(data.toString())
			})
			p.on("error", function(er){
				task.exception= er
				task.finish()
			})
			p.on("exit", function(){
				task.finish()
			})
			await task
		}
		return resolved
	}


	availableFolders(){

		return [
			Path.join(process.env.HOME, "kowi", "user-context", this.idSite),
			Path.join(global.appDir, "node_modules")
		]
	}


}
UserContext.stats={}
export default UserContext