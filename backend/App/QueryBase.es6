/* global vw, core, Varavel, regeneratorRuntime */
var VaravelNamespace= core.org.voxsoftware.Varavel
import Url from 'url'
var Fs= core.System.IO.Fs
import fs from 'fs'
import Path from 'path'
var times=0
import EventEmmiter from 'events'
import Os from 'os'
var uniqid= require("uniqid")


EventEmmiter.defaultMaxListeners=100
var Exception= Varavel.Project.App.Exception


class QueryBase{


	static innerUserFunction(args,c, a){
		return new Varavel.Project.App.Function(a,c,null, args.prefix)
	}

	static context(args){
		
		if(args.ctx)
			return args.ctx
			
		var nargs={}
		for(var id in args){
			nargs[id]=args[id]
		}
		args=nargs
		
		var c, site,osite,d
		osite= args.site || Varavel.Project.App.Site.getFromArgs(args)
		var d=osite.getSiteDir()
		site= osite.idSite
		var uf
		uf=function(a){
			return new Varavel.Project.App.Function(a,c,null, args.prefix)
		}
		//uf= QueryBase.innerUserFunction.bind(null, args, c)
		
		var local1 = QueryBase.vars[site]=QueryBase.vars[site]||{}
		var global1= QueryBase.vars[site + ".global"]=QueryBase.vars[site + ".global"]||{}
		c= {
			homeDir: "deprecated", //Path.join(Os.homedir(), "kowi", "user-context", site),
			UserFunction: uf,
			userFunction: uf,
			
			getSiteContext: function(project){
				var p=  new Varavel.Project.App.Project(project, args)	
				return p.getContext()
			},
			
			impersonate: function(project){
				return new Varavel.Project.App.Project(project, args)	
			},
			
			kowiCode: Varavel.Funcs.env("KOWI_CODE"),
			setTimeout,
			setInterval,
			clearTimeout,
			clearInterval,
			IdSite: site,
			Buffer,
			
			addGlobal: function(name, vari){
				c[name]= vari
				global1[name]= vari
				return vari
			},
			context: args,
			Database: Varavel.Project.App.Database_User.get(site,args).putUserFunction(uf),
			Exception: Exception,
			regeneratorRuntime,
			publicContext: local1,
			uniqid,
			core,
			constants: args.constants,
			UserContext: Varavel.Project.App.UserContext.get(args,site)
		}
		
		for(var id in global1){
			c[id]= global1[id]
		}
		return args.ctx= c
	}



	async static insert(args, options, context){
		var body= args._body
  		var table
		var ins= body.insert || body
		
		
		if(ins instanceof Array){
			var response=[],r
			for(var i=0;i<ins.length;i++){
				args._body.insert= ins[i]
				r= await QueryBase.insert(args, options, context)
				response.push(r)
			}
			return response
		}
		
		

		var result
		var rules= await args.site.getRules(options.tablename +".insert")
		var ruleLimit= await args.site.getRules("limit."+options.tablename +".insert")
		ruleLimit= ruleLimit&& ruleLimit.length? ruleLimit[0]: null
		if(ruleLimit){
			ruleLimit= new Varavel.Project.App.RuleLimit(ruleLimit, null)
		}


		// Actualizar data ...
		if(rules && rules.length){
			for(var rule of rules){
				!context && (context=this.context(args))
				rule= new Varavel.Project.App.Rule(rule, context)
				await rule.check(ins, options)
			}
		}


		if(ruleLimit)
			ins=await ruleLimit.checkData(options, ins)

		table= await context.Database.table(options.tablename)
		if(!ins._id)
			ins._id= Mongo.ObjectID()
		ins.created= Date.now()
		await table.insert(ins)
		return ins
	}



	async static borrar(args, options, context){
		var body= args._body
  		var table
		var q= body.query || {
			_id: body._id
		}
		var data
	
	
		/*
		var data= await table.find(q, {
			_id: 1
		}).toArray()
		*/


		var result
		
		var rules= await args.site.getRules(options.tablename +".delete")
		var ruleLimit= await args.site.getRules("limit."+options.tablename +".delete")
		ruleLimit= ruleLimit&& ruleLimit.length? ruleLimit[0]: null
		if(ruleLimit){
			ruleLimit= new Varavel.Project.App.RuleLimit(ruleLimit, null)
		}
	
	
		var getData= async function(){
			if(data)
				return data
				
			if(!table)
				table= await context.Database.table(options.tablename)
				
			data= await table.find(q, {
				_id: 1
			}).toArray()
			return data
		}
		
		if(true){

			options.getData= getData
			if(ruleLimit)
				await ruleLimit.checkUpdate(options,args._body)
			

			// Actualizar data ...
			if(rules && rules.length){
				for(var k=0;k<rules.length;k++){
					let rule= rules[k]
					!context && (context=this.context(args))
					rule= new Varavel.Project.App.Rule(rule, context)
					await rule.check(q, options)
				}
			}
			
			
			
			if(!data)
				data= await getData()
			
			if(!table)
				table= await context.Database.table(options.tablename)
			
			if(ruleLimit && ruleLimit.hardDelete){
				result=await table.remove({
					_id: {
						$in: data.map((a)=> a._id)
					}
				})
			}
			else{
				result=await table.update({
					_id: {
						$in: data.map((a)=> a._id)
					}
				}, {
					$set: {
						deleted: true
					}
				}, {
					multi: true
				})
			}

		}

		return result
	}


	async static edicion(args, options, context){
		var body= args._body
  		var table
		var q= body.query
		var up=body.update

		// Se editan solo a partir de _id
		var data

		var result
		//var ruleTable= await Varavel.Project.App.Database.table(args.prefix+"rules")
		if(!args.site){
			args.site= new Varavel.Project.App.Site(args)
		}
		
		
		var rules= await args.site.getRules(options.tablename +".edit")
		var ruleLimit= await args.site.getRules("limit."+options.tablename +".edit")
		ruleLimit= ruleLimit&& ruleLimit.length? ruleLimit[0]: null
		if(ruleLimit){
			ruleLimit= new Varavel.Project.App.RuleLimit(ruleLimit, null)
		}
		
		var getData= async function(){
			
			if(data)
				return data
			
			if(!table)
				table= await context.Database.table(options.tablename)
				
			data= await table.find(q, {
				_id: 1
			}).toArray()
			return data
		}
		
		if(true){
			options.getData= getData
			//options.data= data
			if(ruleLimit)
				up=await ruleLimit.checkUpdate(options,q, up)


			// Actualizar data ...
			if(rules && rules.length){
				for(var k=0;k<rules.length;k++){
					let rule= rules[k]
					!context && (context=this.context(args))
					rule= new Varavel.Project.App.Rule(rule, context)
					await rule.check(body, options)
				}
			}
			
			if(!data)
				data = await getData()
			if(!table)
				table= await context.Database.table(options.tablename)
				
			// perfeccionar info ..
			for(var id in up){
				if(!id.startsWith("$")){
					up.$set=up.$set||{}
					up.$set[id]= up[id]
					delete up[id]
				}
			}
			up.$set.updated=Date.now()
			result=await table.update({
				_id: {
					$in: data.map((a)=> a._id)
				}
			}, up, {
				multi: true
			})


		}
		return result
	}

	async static informacion(args, options, context, ubody){
		
		
		
		var body= ubody || args._body
  		
  		
  		
  		var table
		var q= body.query || body
		/*if(!body.query){
			body={
				query:body
			}
		}*/
		
		
		var project=null
		var relation= body.relation
		var limit=100, sort={
			created:-1
		}
		
		


		

		var rules= await args.site.getRules(options.tablename +".get"), ev
		var ruleLimit= await args.site.getRules("limit."+options.tablename +".get")
		ruleLimit= ruleLimit&& ruleLimit.length? ruleLimit[0]: null
		if(ruleLimit){
			ruleLimit= new Varavel.Project.App.RuleLimit(ruleLimit, null)
		}

		if(rules && rules.length){
			for(var k=0;k<rules.length;k++){
				let rule= rules[k]
				!context && (context=this.context(args))
				rule= new Varavel.Project.App.Rule(rule, context)
				ev= await rule.check(body, options)
				if(ev&&ev.defaultPrevented)
					return ev.data
			}
		}

		
		if(body.limit){
			limit= body.limit
			if(ruleLimit){
				limit= await ruleLimit.checkLimit(limit)
			}
		}
		if(body.query && body.sort){
			sort= body.sort
			if(ruleLimit){
				sort= await ruleLimit.checkSort(sort)
			}
		}
		
		table= await context.Database.table(options.tablename)
		
		if(body.aggregate || body.aggregate_pipeline){
			/*
			info= table.aggregate({
				$project: body.aggregate
			})*/
		}
		else if(body.project){
			project= body.project
			if(ruleLimit){
				project= await ruleLimit.checkProjection(project)
			}
			
		}
		else if(body.query){
			if(ruleLimit){
				project= await ruleLimit.checkProjection({})
			}
		}

		var f1,f2, tune, val, q2, d, keys
		var info,v1
		if(body.aggregate){
			
			
			body.aggregate_pipeline= [{
				$match: body.query
			},{
				$project: body.aggregate
			}]
			
		}
		
		
		if(body.aggregate_pipeline){
			//return body.aggregate_pipeline	
			// check aggregation 
			if(ruleLimit){
				for(var i=0;i<body.aggregate_pipeline.length;i++){
					if(body.aggregate_pipeline[i].$project){
						body.aggregate_pipeline[i].$project= await ruleLimit.checkProjection(body.aggregate_pipeline[i].$project,'aggregation')
						
						// se hace un break porque solo es necesario verificar la primera proyección
						break 
					}
				}
			}
			
			
			
			info= table.aggregate(body.aggregate_pipeline).limit(limit)
		}
		else if(body.count==1){
			info= table.count(q)
			return info
		}
		else{
			
			info= (project?table.find(q,{fields:project}):table.find(q)).limit(limit).sort(sort)
		}
	
	
		
		info= await info.toArray()
		if(info.length){
			
			//process.env.DEBUG==1&&vw.warning("RELATION: ", relation)
			
			
			if(relation && relation.length){
				for(var i=0;i<relation.length;i++){
					let relat= relation[i]
					tune={}
					process.env.DEBUG==1&&vw.warning("RELAT: ", relat)
			
					keys=[]
					f1= relat.field1


					for(var y=0;y<info.length;y++){
						let inf= info[y]
						val=inf[f1]
						if(val){
							if(val instanceof Array){
								for(var z=0;z<val.length;z++){
									let v= val[z]
									v1=v.value || v
									tune[v1]= tune[v1]||[]
									tune[v1].push(inf)
									keys.push(v1)
								}
							}else{
								v1=val.value || val
								tune[v1]= tune[v1]||[]
								tune[v1].push(inf)
								keys.push(v1)
							}
						}
					}

					f2= relat.field2
					q2= relat.query || {}
					q2[f2]= {
						$in:keys
					}

					//process.env.DEBUG==1&&vw.warning("QUERY: ", q2)
					
					d=await this.informacion(args,  relat.options, context, {
						query: q2,
						relation: relat.relation,
						limit: relat.limit,
						sort: relat.sort
					})

					
					if(d && d.length){
						for(var a=0;a<d.length;a++){
							let d1= d[a]
							val= d1[f2]
							if(tune[val]){
								for(var obj of tune[val]){
									obj[relat.name]=obj[relat.name]||[]
									obj[relat.name].push(d1)
								}
							}
						}
					}
				}
			}
		}

		if(ruleLimit){
			info= ruleLimit.transformData(info)
		}
		return info
	}

}
QueryBase.vars= {}
export default QueryBase
