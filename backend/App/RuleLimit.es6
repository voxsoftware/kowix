/* global vw, core, Varavel */
var VaravelNamespace= core.org.voxsoftware.Varavel
import Url from 'url'
var Fs= core.System.IO.Fs
import fs from 'fs'
import Path from 'path'
var times=0

class RuleLimit{
	constructor(row,context){
		this.row= row
		this.context= context
	}


	checkUpdate(options,query,update){
		var row= this.row
		if(row.maxLength !== undefined && options.data.length>row.maxLength){
			throw Varavel.Project.App.Exception.create("Allowed length of updated rows exceeded").putCode("INVALID_PARAMETERS").end()
		}
		return this.checkData(options, update)
	}

	get hardDelete(){
		return !!this.row.hardDelete
	}

	transformData(data){
		if(this.row.projection){
			var q=new global.Mingo.Query({}, this.row.projection)
			data= q.find(data).all()
		}
		return data
	}

	checkData(options, update){
		if(this.row.projection){
			var copia={}
			for(var id in update){
				if(id.startsWith("$"))
					copia[id]= update[id]
			}

			var q= new global.Mingo.Query({}, this.row.projection)
			update= q.find([update]).first()
			if(Object.keys(copia).length>0){
				for(var id in copia){
					update[id]= q.find([copia[id]]).first()
				}
			}
			copia=null
		}
		return update
	}





	async controlData(){

	}

	async checkLimit(limit){
		var row= this
		if(row.maxLimit !== undefined && limit>row.maxLimit){
			limit= row.maxLimit
		}
		else if(row.minLimit !== undefined && limit<row.minLimit){
			limit= row.minLimit
		}
		if(row.limitFunc){
			this.limitFunc= new Varavel.Project.App.Function(row.limitFunc, this.context)
			await this.limitFunc.load()
			limit= this.limitFunc.execute.bind(this)(limit)
		}
		return limit
	}
	
	
	static createEnableFunc(e){
		return function(body){
			var neg, newbody
			body=body||{}
			if(body){
				for(var id in body){
					if(body.id==0)
						neg= true 
				}
			}
			
			if(neg){
				newbody={}
				for(var i=0;i<e.length;i++){
					newbody[e[i]]= 1
				}
				for(var id in body){
					if(body[id]!==undefined && e.indexOf(id)>=0)
						delete newbody[id]
				}	
			}
			else{
				newbody= {}
				for(var id in body){
					if(body[id]!==undefined && e.indexOf(id)>=0){
						newbody[id]= body[id]
					}
				}
				if(Object.keys(newbody).length==0){
					for(var i=0;i<e.length;i++){
						newbody[e[i]]= 1
					}
				}
			}
			return newbody
		}
	}


	async checkProjection(project, mode ){

		if(mode=='aggregation'){
			return await this.checkAggregateProjection(project)
		}

		var row= this.row, vacio, pj
		if(row.disabledfields){
			vacio=project?Object.keys(project).length==0: true
			if(!project)
				project={}
			for(var field of row.disabledfields){
				if(vacio)
					project[field]=0
				else
					delete project[field]
			}
		}
		if(row.enabledfields){
			
			pj= RuleLimit.createEnableFunc(row.enabledfields)
			project= pj(project)
			//vw.info("ENABLED:",row.enabledfields,project)
		}
		if(row.projectionFunc){
			this.projectionFunc= new Varavel.Project.App.Function(row.projectionFunc, this.context)
			await this.projectionFunc.load()
			project= this.projectionFunc.execute.bind(this)(project)
		}
		
		return project
	}
	
	
	async checkAggregateProjection(project){
		var row= this.row, vacio, pj, g
		if(row.disabledfields){
			g= function(val){
				if(typeof val=="object"){
					
					if(val instanceof Array){
						for(var i=0;i<val.length;i++){
							if(typeof val[i] == "object"){
								g(val[i])
							}
							else{
								if(val[i].startsWith && val[i].startsWith("$")){
									if(row.disabledfields.indexOf(val[i].substring(1))>=0)
										throw Varavel.Project.App.Exception.create("You cannot project field: " + val[i]).putCode("ACCESS_DENIED").end()
								}
							}
						}
					}
					else{
						for(var id in val){
							if(typeof val[id] == "object"){
								g(val[id])
							}
							else{
								if(val[id].startsWith && val[id].startsWith("$")){
									if(row.disabledfields.indexOf(val[id].substring(1))>=0)
										throw Varavel.Project.App.Exception.create("You cannot project field: " + val[id]).putCode("ACCESS_DENIED").end()
								}
							}
						}
					}
				}
			}
			
		}
		
		
		else if(row.enabledfields){
			
			g= function(val){
				if(typeof val=="object"){
					
					if(val instanceof Array){
						for(var i=0;i<val.length;i++){
							if(typeof val[i] == "object"){
								g(val[i])
							}
							else{
								if(val[i].startsWith && val[i].startsWith("$")){
									if(row.enabledfields.indexOf(val[i].substring(1))<0)
										throw Varavel.Project.App.Exception.create("You cannot project field: " + val[i]).putCode("ACCESS_DENIED").end()
								}
							}
						}
					}
					else{
						for(var id in val){
							if(typeof val[id] == "object"){
								g(val[id])
							}
							else{
								if(val[id].startsWith && val[id].startsWith("$")){
									if(row.enabledfields.indexOf(val[id].substring(1))<0)
										throw Varavel.Project.App.Exception.create("You cannot project field: " + val[id]).putCode("ACCESS_DENIED").end()
								}
							}
						}
					}
				}
			}
			
		}
		
		if(g)
			g(project)
			
			
		
		if(row.projectionFunc){
			this.projectionFunc= new Varavel.Project.App.Function(row.projectionFunc, this.context)
			await this.projectionFunc.load()
			project= this.projectionFunc.execute.bind(this)(project, 'aggregation')
		}
		
		return project
	}

	async checkSort(sort){
		var row= this
		if(row.sortFunc){
			this.sortFunc= new Varavel.Project.App.Function(row.sortFunc, this.context)
			await this.sortFunc.load()
			sort= this.sortFunc.execute.bind(this)(sort)
		}
		return sort
	}



}

export default RuleLimit
