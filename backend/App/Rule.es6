

var VaravelNamespace= core.org.voxsoftware.Varavel
import Url from 'url'
var Fs= core.System.IO.Fs
import fs from 'fs'
import Path from 'path'
var times=0
class Rule{
	constructor(row,context){
		this.row= row
		this.context= context
	}

	async check(body,options){
		var row= this.row
		if(row.function){
			
			//process.env.DEBUG==1 &&
			//vw.warning("FUNC:",row.function)
			this.func= new Varavel.Project.App.Function(row.function, this.context)
			await this.func.load()
			return await this.func.execute(body,options)
		}
	}

}

export default Rule
