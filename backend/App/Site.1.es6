/* global core,vw,Varavel */
import Path from 'path'
var Fs= core.System.IO.Fs
import fs from 'fs'

var tempdir= Path.join(global.appDir,"cache","tmp")
class Site{
	constructor(args){
		if(args){
			//this.args= args
			args.request.params=args.request.params||{}
			if(args.request.params.site){
				this.site= args.request.params.site
			}
			else{
				this.site= args.request.headers.host
				this.site= this.site.split(":")[0]
				
			}
		}
	}
	
	static get appDir(){
		return process.env.BASE_DIR || global.baseDir || global.appDir
	}
	
	
	get siteName(){
		return this.site
	}
	getPublicPath(){
		var p=this.getSiteDir()
		if(!p)
			throw new core.System.Exception("Domain not found")
		return Path.join(p, "public")
	}
	getFunctionPath(){
		var p=this.getSiteDir()
		if(!p)
			throw new core.System.Exception("Domain not found")
		return Path.join(p, "functions")
	}
	
	get idSite(){
		return this.site
	}
	
	
	
	getSiteDir(gcloudMethod){
		Site.sitesc= Site.sitesc||{}
		var c= Site.sitesc[this.site], r, origSite= this.site
		
		
		if(c && Date.now()- c.updated<= 10*60*1000){
			if(c.realSite){
				this.site= c.realSite
			}
			return c.file
		}
		
		var siteDir, r
		if(this.site=="127.0.0.1" && process.env.DEFAULT_DIR){
			siteDir= process.env.DEFAULT_DIR
		}
		else{
			siteDir=Path.join(Site.appDir, "..",  this.site)
			if(!Fs.sync.exists(siteDir)){
				siteDir=Path.join(Site.appDir, "..", "domains", this.site)
				if(Fs.sync.exists(siteDir)){
					r= Fs.sync.readFile(siteDir, 'utf8')
					r= r.trim()
					this.site= r
					siteDir=Path.join(Site.appDir, "..", "remote-gs", "proyectos",r)
				}
				else{
					siteDir=null
				}
			}
		}
		
		if(siteDir){
			Site.sitesc[origSite]= {
				realSite: r,
				file:siteDir, 
				updated: Date.now()
			}
		}
		return siteDir
	}
	
	
	
	
	
	async getFunctionPathMode2(){
		var p= await this.getSiteDirMode2()
		if(!p)
			throw new core.System.Exception("Domain not found")
		return Path.join(p, "functions")
	}
	
	
	async getSiteDirMode2(){
		var c= null
		var r
		if(Site.sitesc_2){
			c= Site.sitesc_2[this.site]
			if(!c){
				await this.loadSite2Cache()
				c= Site.sitesc_2[this.site]
			}
		}
		else{
			await this.loadSite2Cache()
			c= Site.sitesc_2[this.site]
		}
		
		if(c && Date.now()- c.updated<= 1*60*1000){
			return c.file
		}
		else if(c && c.file){
			this._getSiteDirMode2(true)
			return c.file
		}
		
		var site= await this._getSiteDirMode2()
		return site
	}
	
	
	
	async _getSiteDirMode2(secure){
		var siteDir,sto,buk, req, response, url, content, newsite
		try{
			
			if(this.site=="127.0.0.1" && process.env.DEFAULT_DIR){
				siteDir= process.env.DEFAULT_DIR
			}
			else{
				siteDir= Path.join(Site.appDir, "..", this.site)
				
				var exists=Fs.sync.exists(siteDir)
				if(!exists){
					
					siteDir= Path.join(Site.appDir, "..", "domains", this.site)
					exists=Fs.sync.exists(siteDir)
					if(exists){
						content= await Fs.async.readFile(siteDir,'utf8')
						if(content){
							newsite= content
							siteDir= Path.join(Site.appDir, "..", content)
						}
						else{
							siteDir=null
						}
					}
				}
			}
			if(siteDir){
				
				
				Site.sitesc_2[this.site]= {
					file:siteDir, 
					updated: Date.now()
				}
				
				if(newsite)
					this.site=newsite 
				
				// DISABLE UPDATE CACHE ...
				//this.updateSiteCache()
				
			}
		}	catch(e){
			if(!secure)
				throw e
			else 
				console.error("Error al obtener directorio del sitio",e)
		}
		return siteDir
	}
	
	
	async loadSite2Cache(){
		Site.sitesc_2= Site.sitesc_2||{}
		return 
	
	}
	
	
	async updateSiteCache(){
		return
	}
	
	
	
	
	
	async readState(options){
		
		Site.siteVars= Site.siteVars||{}
		var cache= Site.siteVars[this.site]
		if(cache && Date.now()-cache.updated<= 120000)
			return cache.code
		
		
		var info= await this.getFunction("vars",".json",options)
		if(!info)
			throw new core.System.Exception("Site "+this.site+" it's not enabled")
		if(!info.compiledCode){
			info.compiledCode= JSON.parse(info.code)
		}
		cache=cache || {}
		Site.siteVars[this.site]= cache
		cache.updated= Date.now()
		return cache.code=info.compiledCode
	}
	
	static getFromArgs(args){
		if(args.site){
			return args.site
		}
		args.site= new Site(args)
		return args.site
	}
	
	static getFromId(site){
		
		var s= new Site()
		s.site= site
		return s
	}
	
	async getRules(rule){
		var rules= await this.getFunction("rules." + rule,".json")
		vw.warning("RULE HERE:",rule,rules)
		if(rules && !rules.compiledCode){
			rules.compiledCode= JSON.parse(rules.code)
		}
		
		var ruleAll
		var rs= rule.split(".")
		rs.splice(rs.length>3?1:0,1)
		ruleAll= rs.join(".")
		
		
		
		var rules2= await this.getFunction(ruleAll,".json")
		if(rules2 && !rules2.compiledCode){
			rules2.compiledCode= JSON.parse(rules2.code)
		}
		var ar=[]
		if(rules && rules.compiledCode){
			for(var i=0;i<rules.compiledCode.length;i++){
				ar.push(rules.compiledCode[i])
			}
		}
		if(rules2 && rules2.compiledCode){
			for(var i=0;i<rules2.compiledCode.length;i++){
				ar.push(rules2.compiledCode[i])
			}
		}
		return ar
	}
	
	
	async getFile({path, fullpath, error404, error, success, headers }){
		
		
		var site=this
		var pub
		if(!fullpath){
			pub= Path.join(await site.getSiteDirMode2(), "public")
			pub=Path.join(pub, path)
		}
		else{
			pub= fullpath
		}
		
		
		if(Varavel.Funcs.env("FILE_CACHE_SERVER")){
			var url= Varavel.Funcs.env("FILE_CACHE_SERVER")
			url+="/api/staticinfo?file=" + encodeURIComponent(pub)
			var req= new core.VW.Http.Request(url)
			var response= await req.getResponseAsync()
			if(typeof response.body=="string"){
				response.body=JSON.parse(response.body)
			}
			if(response.body.error){
				return error(response.body.error)
			}
			if(response.body.noexists){
				return error404()
			}
			if(response.body){
				return success(response.body)
			}
			return 
		}
		
		
		var cache= Site.fileCache[pub], exists
		//vw.log("cache: ", pub,  cache)
		var uname=pub.replace(/\//ig, "$")
		try{
			if(cache && cache.exists===false){
				
				if(cache && Date.now() - cache.updated<= parseInt(Varavel.Funcs.env("CACHE_STATIC_TIME", 1.5*60000))){
					if(error404)
						return await error404()
					throw new core.System.IO.FileNotFoundException("File not found")
				}
				
				try{
					exists= Fs.sync.exists(pub)
					//exists= exists[0]
				}catch(e){
				}
				if(!exists){
					
					Site.fileCache[pub]={
						file: pub, 
						exists: false,
						updated: Date.now()
					}
					
					if(error404)
						return await error404()
					throw new core.System.IO.FileNotFoundException("File not found")
				}
				
				//reload= true
			}
			else if(!cache){
				exists= Fs.sync.exists(pub)
				if(!exists){
					
					Site.fileCache[pub]={
						file: pub, 
						exists: false,
						updated: Date.now()
					}
					
					if(error404)
						return await error404()
					throw new core.System.IO.FileNotFoundException("File not found")
				}
			}
			
			success({
				tmpfile: Path.join(tempdir, uname),
				file: pub,
				name: Path.basename(pub)
			})
			
		}
		catch(e){
			error(e)
		}
		
		
		
	}
	
	
	
	async getFunction(cid, extension=".js", options){
		
		var time= Date.now()
		var path= await this.getFunctionPathMode2()
		
		
		var func= Path.join(path, cid + extension)
		var content, self= this
		Site.cache = Site.cache||{}
		var cache, stat, cachetime
		cachetime= Varavel.Funcs.env("CACHE_API_TIME", 300000)
		cache= Site.cache[this.site + "_" + cid]
		var task=new core.VW.Task(), result
		
		
		
		
		if(!cache || Date.now()-cache.updated> cachetime || (options && options.development_mode==1)){
			var upg= async ()=>{
		
				await self.getFile({
					fullpath:func, 
					error404: function(){
						task.result=task.result||{}
						task.result.noexists=true
						task.finish()
					}, 
					error: function(e){
						task.exception= e
						task.finish()
					}, 
					success: function(r){
						
						task.result= r
						task.finish()
					}
				})
				result= await task 
				result= result || {}
				if(result.noexists){
					
					task= new core.VW.Task()
					func= Path.join(path, cid + (extension!=".js"?".js":".es6"))
					self.getFile({
						fullpath:func, 
						error404: function(){
							task.result=task.result||{}
							task.result.noexists=true
							task.finish()
						}, 
						error: function(e){
							task.exception= e
							task.finish()
						}, 
						success: function(r){
							task.result= r
							task.finish()
						}
					})
					result= await task 
					result= result||{}
					
					
				}
				
				
				if(!result.noexists){
					stat= await Fs.async.stat(result.file)
					if(cache && cache.updated){
						if(stat.mtime.getTime()<= cache.mtime){
							cache.updated= Date.now()
							//cache.mtime= stat.mtime.getTime()
							return cache 
						}
					}
					content= await Fs.async.readFile(result.file,'utf8')
					if(cache){
						/*
						for(var id in cache){
							if(cache[id] && cache[id].destroy)
								cache[id].destroy()
							delete cache[id]
						}*/
					}
					
					Site.cache[self.site + "_" + cid]=null
					cache= Site.cache[self.site + "_" + cid]= {
						file: result.file,
						codefile: (result.tmpfile || result.file) + "$$-code",
						mtime: stat.mtime.getTime(),
						remotefile: func, 
						code: content, 
						ext: Path.extname(func),
						updated: Date.now()
					}
					
				}
			}
			
			await upg()
		}
		return cache
	}
	
}

Site.fileCache={}
export default Site