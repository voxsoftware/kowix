/*global Varavel,core*/
var controllerGroup = Varavel.Project.Modules.Api.Http.Controllers

class Project{
    constructor(sitename, args){
        this.sitename= sitename
        this.args= args
    }
    
    static async initialize(args){
    	
    	//args._body =args._body  ||{}
    	args._obody=args._obody ||{}
    	
    	
		var site = args.site 
		if(!site){
			site= Varavel.Project.App.Site.getFromArgs(args)
		}
		await site.getRealSite()
		
		args.site= site
		var state = await site.readState()
		args.prefix = state.prefix
		args.constants = state.constants
		var func, ctx
		
		
		
		
		
		if(Object.keys(args._obody).length==0){
			if(!args.constants || args.constants.bodyparser!==false){
				if(!Project.bodyParser)
					Project.bodyParser= core.VW.Http.Server.getBodyParser()
				
				if(Project.bodyParser&& args.request && args.request.headers){
					var task= new core.VW.Task()
					Project.bodyParser({
						request:args.request,
						response:args.response,
						continue:function(){
							task.finish()
						}
					})
					await task 
					
					args._obody= Project.unionBody(args)
				}
			}
		}
		
		
		
		if(args.constants && args.constants.bodyAnalyze && Object.keys(args._obody).length>0){
			ctx= Varavel.Project.App.QueryBase.context(args)
			func = new Varavel.Project.App.Function(args.constants.bodyAnalyze, ctx)
			args._body= await func.invoke(args._obody)
		}
		else{
			args._body= args._obody || args._body
		}
    }
    
    
    static unionBody(args) {
		var data = {}
		if(args.request){
			if (args.request.query.__json__mode == 1) {
				args.request.body = JSON.parse(args.request.query.__json__data)
			}
			if (args.request.query) {
				for (var id in args.request.query) {
					data[id] = args.request.query[id]
				}
			}
			if (args.request.body) {
				for (var id in args.request.body) {
					data[id] = args.request.body[id]
				}
			}
		}
		//data = this.perfect(data)
		return data
	}
	
	
    
    async context(site,args){
    	args=args|| this.args ||{}
	    //var f= controllerGroup.ApiController.bindMethod("checkPermission")	
		
		var site= Varavel.Project.App.Site.getFromId(site )
		var args1={
			request: args.request || {},
			response: args.response || {}, 
			continue: args.continue || function(){},
			continueAsync:  args.continueAsync || function(){}
		}
		site.args= args1
		//await site.getRealSite()
		args1.site= site
		//await f(args1, true)
		await Project.initialize(args1)
		
		var ctx= Varavel.Project.App.QueryBase.context(args1)
		return ctx
    }
    async methodAs(site, args, funcname){
	    var ctx = await this.context(site,args)
		var func = new Varavel.Project.App.Function(funcname, ctx)
		return func 
		
	}
	
	userFunction(e){
	    return this.methodAs(this.sitename, this.args, e)   
	}
	
	getContext(){
	    return this.context(this.sitename, this.args)   
	}
}
export default Project