/* global core,vw,Varavel */
import Path from 'path'
var Fs= core.System.IO.Fs
import fs from 'fs'
var tempdir= Path.join(global.appDir,"cache","tmp")

class Site{
	
	static getFromArgs(args){
		if(args.site)
			return args.site 
		args.site= new Site(args)
		return args.site
	}
	static get appDir(){
		return process.env.BASE_DIR || global.baseDir || global.appDir
	}
	
	static getFromId(site){
		var s= new Site()
		s.site= site
		return s
	}
	
	constructor(args){
		if(args){
			
			this.args= args
			args.request.params=args.request.params||{}
			if(args.request.params.site){
				this.site= args.request.params.site
			}
			else{
				this.site= args.request.headers.host
				this.site= this.site.split(":")[0]
			}
			//this.getRealSite()
			
			
		}
	}
	
	
	get idSite(){
		return this.site
	}
	
	
	async getRealSite(){
		
		if(this.gettedRealsite)
			return 
		
		
		try{
			var parts
			parts= this.site.split("::")
			var tsite= parts[0]
			this.site= tsite 
			
			
			Site.customDomains= Site.customDomains||{}
			var cached= Site.customDomains[this.site]
			if(cached && cached.updated>=Date.now() - Varavel.Funcs.env("CACHE_STATIC_TIME", 60000)){
				this.site= cached.site 
				this.usiteDir= cached.siteDir
				this.found= cached.found 
				return 
			}
			
			
			console.log("Cache not up to date ....", parts[0])
			
			var siteDir, r, newsite
			if(tsite=="127.0.0.1" && process.env.DEFAULT_DIR){
				siteDir= process.env.DEFAULT_DIR
				this.usiteDir= siteDir
				//console.log("Site ...........................", siteDir)
			}
			else{
				siteDir=Path.join(Site.appDir, "..",  tsite)
				if(!Fs.sync.exists(siteDir)){
					siteDir=Path.join(Site.appDir, "..", "domains", tsite)
					try{
						r= await Fs.async.readFile(siteDir, 'utf8')
						r= r.trim()
						newsite= r
					}catch(e){
						siteDir=null 
					}
					
					if(siteDir==null){
						siteDir=Path.join(Site.appDir, "..", "domains_v2", parts[0])
						try{
							r= await Fs.async.readFile(siteDir, 'utf8')
							r= r.trim()
							newsite= r
						}catch(e){
							siteDir=null
						}
					}
				}
			}
			
			
			if(!siteDir){
				
				if(parts.length>1){
					
					
					try{
						var p=  new Varavel.Project.App.Project("kowix", this.args)
						var ctx= await p.getContext()
						await ctx.userFunction("download.site").invoke({
							"site": parts[0],
							"key" : parts[1]
						})
						
						siteDir=Path.join(Site.appDir, "..", "domains_v2", parts[0])
						try{
							r= await Fs.async.readFile(siteDir, 'utf8')
							r= r.trim()
							newsite= r
						}catch(e){
							siteDir=null
						}
						
						
						
						
					}catch(e){
						
						
						Site.customDomains[tsite]={
							updated: Date.now(), 
							site: newsite || tsite,
							found: !!siteDir,
							siteDir: this.usiteDir,
							error: e
						}
						
					}
					
					
				}
				
			}
			
			
			Site.customDomains[tsite]= {
				updated: Date.now(), 
				site: newsite || tsite,
				found: !!siteDir,
				siteDir: this.usiteDir
			}
			if(this.site!=tsite){
				Site.customDomains[this.site]=Site.customDomains[tsite]
			}
			if(newsite){
				
				this.site= newsite
				Site.customDomains[tsite]= {
					updated: Date.now(), 
					site: newsite || tsite,
					found: !!siteDir
				}
				Site.customDomains[newsite]=Site.customDomains[tsite]
			}
			
			
			if(!siteDir && this.site=="kowix")
				siteDir= global.appDir
			
			this.found= !!siteDir
		}catch(e){
			throw e 
		}
		finally{
			this.gettedRealsite=true
		}
		
		
		
		
	}
	
	
	getSiteDir(){
		if(!this.found)
			throw new core.System.Exception("Site "+this.site+" not found")
		return this.usiteDir || Path.join(Site.appDir, "..",  this.site)
	}
	
	
	
	getPublicPath(){
		var p=this.getSiteDir()
		return Path.join(p, "public")
	}
	
	
	getFunctionPath(){
		var p=this.getSiteDir()
		return Path.join(p, "functions")
	}
	
	
	
	/// COMPATIBILITY 
	getFunctionPathMode2(){
		return this.getFunctionPath()
	}
	getSiteDirMode2(){
		return this.getSiteDir()
	}
	
	/// COMPATIBILITY 
	
	
	async readState(options){
		
		Site.siteVars= Site.siteVars||{}
		var cache= Site.siteVars[this.site]
		var cache2= Site.siteVars[this.site+".error"]
		if(cache && Date.now()-cache.updated<= 120000)
			return cache.code
		
		if(cache2 && Date.now()-cache2.updated<= Varavel.Funcs.env("CACHE_STATIC_TIME", 120000))
			throw cache2.error 
		
		
		var info= await this.getFunction("vars",".json",options)
		if(!info){
			cache2={
				error: new core.System.Exception("Site "+this.site+" it's not enabled"),
				updated: Date.now()
			}
			Site.siteVars[this.site+".error"]= cache2
			throw cache2.error
		}
		
		
		if(!info.compiledCode){
			info.compiledCode= JSON.parse(info.code)
		}
		cache=cache || {}
		Site.siteVars[this.site]= cache
		if(Site.siteVars[this.site+".error"])
			delete Site.siteVars[this.site+".error"]
			
		cache.updated= Date.now()
		return cache.code=info.compiledCode
	}
	
	
	
	
	async getRules(rule){
		var rules= await this.getFunction("rules." + rule,".json")
		vw.warning("RULE HERE:",rule,rules)
		if(rules && !rules.compiledCode){
			rules.compiledCode= JSON.parse(rules.code)
		}
		
		var ruleAll
		var rs= rule.split(".")
		rs.splice(rs.length>3?1:0,1)
		ruleAll= rs.join(".")
		
		
		
		var rules2= await this.getFunction(ruleAll,".json")
		if(rules2 && !rules2.compiledCode){
			rules2.compiledCode= JSON.parse(rules2.code)
		}
		var ar=[]
		if(rules && rules.compiledCode){
			for(var i=0;i<rules.compiledCode.length;i++){
				ar.push(rules.compiledCode[i])
			}
		}
		if(rules2 && rules2.compiledCode){
			for(var i=0;i<rules2.compiledCode.length;i++){
				ar.push(rules2.compiledCode[i])
			}
		}
		return ar
	}
	
	
	
	/// TODO
	/// Revisar si se debe modificar este método para optimizarlo 
	async getFile({path, fullpath, error404, error, success, headers }){
		
		
		var site=this
		var pub
		if(!fullpath){
			pub= Path.join(await site.getSiteDirMode2(), "public")
			pub=Path.join(pub, path)
		}
		else{
			pub= fullpath
		}
		
		
		if(Varavel.Funcs.env("FILE_CACHE_SERVER")){
			var url= Varavel.Funcs.env("FILE_CACHE_SERVER")
			url+="/api/staticinfo?file=" + encodeURIComponent(pub)
			var req= new core.VW.Http.Request(url)
			var response= await req.getResponseAsync()
			if(typeof response.body=="string"){
				response.body=JSON.parse(response.body)
			}
			if(response.body.error){
				return error(response.body.error)
			}
			if(response.body.noexists){
				return error404()
			}
			if(response.body){
				return success(response.body)
			}
			return 
		}
		
		
		var cache= Site.fileCache[pub], exists
		//vw.log("cache: ", pub,  cache)
		var uname=pub.replace(/\//ig, "$")
		try{
			if(cache && cache.exists===false){
				
				if(cache && Date.now() - cache.updated<= parseInt(Varavel.Funcs.env("CACHE_STATIC_TIME", 1.5*60000))){
					if(error404)
						return await error404()
					throw new core.System.IO.FileNotFoundException("File not found")
				}
				
				try{
					exists= Fs.sync.exists(pub)
					//exists= exists[0]
				}catch(e){
				}
				if(!exists){
					
					Site.fileCache[pub]={
						file: pub, 
						exists: false,
						updated: Date.now()
					}
					
					if(error404)
						return await error404()
					throw new core.System.IO.FileNotFoundException("File not found")
				}
				
				//reload= true
			}
			else if(!cache){
				exists= Fs.sync.exists(pub)
				if(!exists){
					
					Site.fileCache[pub]={
						file: pub, 
						exists: false,
						updated: Date.now()
					}
					
					if(error404)
						return await error404()
					throw new core.System.IO.FileNotFoundException("File not found")
				}
			}
			
			success({
				tmpfile: Path.join(tempdir, uname),
				file: pub,
				name: Path.basename(pub)
			})
			
		}
		catch(e){
			error(e)
		}
		
		
		
	}
	
	
	
	async getFunction(cid, extension=".js", options){
		
		//var time= Date.now()
		var path= this.getFunctionPath()
		
		
		var func= Path.join(path, cid + extension)
		var content, self= this
		Site.cache = Site.cache||{}
		var cache, stat, cachetime
		cachetime= Varavel.Funcs.env("CACHE_API_TIME", 300000)
		cache= Site.cache[this.site + "_" + cid]
		var task=new core.VW.Task(), result
		
		
		
		
		if(!cache || Date.now()-cache.updated> cachetime || (options && options.development_mode==1)){
			//var upg= async ()=>{
		
				await self.getFile({
					fullpath:func, 
					error404: function(){
						task.result=task.result||{}
						task.result.noexists=true
						task.finish()
					}, 
					error: function(e){
						task.exception= e
						task.finish()
					}, 
					success: function(r){
						
						task.result= r
						task.finish()
					}
				})
				result= await task 
				result= result || {}
				if(result.noexists){
					
					task= new core.VW.Task()
					func= Path.join(path, cid + (extension!=".js"?".js":".es6"))
					self.getFile({
						fullpath:func, 
						error404: function(){
							task.result=task.result||{}
							task.result.noexists=true
							task.finish()
						}, 
						error: function(e){
							task.exception= e
							task.finish()
						}, 
						success: function(r){
							task.result= r
							task.finish()
						}
					})
					result= await task 
					result= result||{}
					
					
				}
				
				
				if(!result.noexists){
					stat= await Fs.async.stat(result.file)
					if(cache && cache.updated){
						if(stat.mtime.getTime()<= cache.mtime){
							cache.updated= Date.now()
							//cache.mtime= stat.mtime.getTime()
							return cache 
						}
					}
					content= await Fs.async.readFile(result.file,'utf8')
					if(cache){
						/*
						for(var id in cache){
							if(cache[id] && cache[id].destroy)
								cache[id].destroy()
							delete cache[id]
						}*/
					}
					
					Site.cache[self.site + "_" + cid]=null
					cache= Site.cache[self.site + "_" + cid]= {
						file: result.file,
						codefile: (result.tmpfile || result.file) + "$$-code",
						mtime: stat.mtime.getTime(),
						remotefile: func, 
						code: content, 
						ext: Path.extname(func),
						updated: Date.now()
					}
					
				}
			
			
			//await upg()
		}
		return cache
	}
	
	
	
}



Site.fileCache={}
export default Site