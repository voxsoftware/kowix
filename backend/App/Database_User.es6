/* global core,Varavel */
import Path from 'path'



class Database_User{

	constructor(args){
		this.prefix= args.prefix
		this.vars= args.constants
		if(this.vars)
			this.dbconnection= this.vars.dbconnection
	}
	
	static create(args){
		return new Database_User(args)
	}

	async getDatabase(){
		return this
	}
	


	putUserFunction(uf){
		this.UserFunction= uf
		return this
	}

	static get(idSite, args){
		return new Database_User(args,idSite)
	}

	async table(name){
		var prefix= this.prefix
		if(!prefix && !this.dbconnection){
			throw new core.System.Exception("Please especify a `prefix` or a `dbconnection` in your vars.json file")
		}
		prefix=prefix||""
	
		
		// en kowix se debe crear la función para crear la base de datos 
		if(!this._db){
			var DatabaseF= await this.UserFunction("database.init")
			await DatabaseF.load()
			this._db= await DatabaseF.execute(this)
		}
		return await this._db.table(prefix+name)
	}
}

export default Database_User
