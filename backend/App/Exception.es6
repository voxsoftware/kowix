class Exception extends core.System.Exception{
	putCode(code, pars){
		this.code= code
		if(pars)
			this.putParameters(pars)
		return this
	}
	static create(message, innerException){
		/*var  args= Array.prototype.slice.call(arguments, 0,arguments.length)
 		var exception= Exception.$constructor.bind(Exception.$constructor, args)
		return new exception()*/
		return new Exception(message, innerException)
	}

	end(){
		return this
	}

	putParameters(pars){
		if(pars)
			this.pars= pars
		return this
	}


}
export default Exception
