if (!window.app) {
    var h = location.host;
    var p = location.protocol;
    if (location.host == 'admin.d0stream.com') {
        h = 'v.d0stream.com';
        p = 'https';
    }
    window.app = {
        'uiUrl': '',
        'apiUrl': '/api',
        'videoUrl': location.protocol + '//' + location.host,
        'livePutUrl': 'http://server01.codedream.ml:30010/api'
    };
}