/*global core,Varavel,vw*/
import fs from 'fs'
import Path from 'path'
import Url from 'url'
var Fs= core.System.IO.Fs
import Md5 from 'md5'

var tempdir= Path.join(global.appDir,"cache","tmp")


class FrontController extends core.org.voxsoftware.Varavel.Http.Controller{
	
	
	getSiteFrontEndDir(args){
		return Path.join(Varavel.Project.App.Site.getFromArgs(args).getSiteDir(), "frontend")
	}
	async getSiteFrontEndDirMode2(args){
		return Path.join(await Varavel.Project.App.Site.getFromArgs(args).getSiteDirMode2(), "frontend")
	}
	
	
	async staticContent(args){
		
		var fileServer=new core.VW.Http.StaticServe.Server()
		var path= await Varavel.Project.App.Site.getFromArgs(args).getSiteDirMode2()
		path= Path.join(path,"public")
	//	vw.info(path, ">", args.request.url)
		fileServer.addPath(path)
		return await fileServer.handle(args)
	}
	/*
	async staticContent_0(args){
		var site= args.request.params.site|| args.request.headers.host
		var pub = Path.join("proyectos", site, "public")
		var pub1= Path.join(global.appDir, "..", "remote-gs", pub)
		var storage= new Varavel.Project.Modules.Api.Libraries.Storage()
		var u=Url.parse(args.request.url)
		pub=Path.join(pub, u.path)
		var file= storage.bucket().file(pub)
		var exists, exists1
		try{
			exists1=exists= await Fs.async.exists(pub1+"/"+u.path)
			if(!exists)
				exists= await file.exists()
				
		}catch(e){
		}
		if(!exists){
			return await args.continueAsync()
		}
		var paths, p
		if(!exists1){
			paths= pub.split("/")
			p=Path.join(global.appDir, "..", "remote-gs", 'proyectos')+"/"
			for(var i=1;i<paths.length-1;i++){
				p+= paths[i]+"/"
				if(!Fs.sync.exists(p))
					Fs.sync.mkdir(p)
			}
		}
		var fileServer= new core.VW.Http.StaticServe.Server()
		fileServer.addPath(pub1)
		return await fileServer.handle(args)
	}
	*/
	
	async staticContent_2(args){
		var site= Varavel.Project.App.Site.getFromArgs(args)
		var u=Url.parse(args.request.url), continue_code, task
		var end= function(er){
			vw.error("Error en request",er)
			continue_code= false
			if(task)
				task.finish()
			args.response.statusCode=500
			args.response.end()
		}
		
		if(args.request.headers["range"]!= undefined){
			
			
			
			var pub
			if(args.request.query.project_root=="496712")
				pub= Path.join(await site.getSiteDirMode2())
			else 
				pub= Path.join(await site.getSiteDirMode2(), "public")
				
				
			var storage= new Varavel.Project.Modules.Api.Libraries.Storage()
			pub=Path.join(pub, u.pathname)
			var file= storage.bucket().file(pub)
			var exists
			try{
				exists= await file.exists()
				exists= exists[0]
			}catch(e){
			}
			if(!exists){
				return await args.continueAsync()
			}
			
			
			var url= await file.getSignedUrl({
				action: "read", 
				expires: Date.now() + parseInt(Varavel.Funcs.env("CACHE_SIGNEDURL", 4*3600*1000))
			})
			var req= new core.VW.Http.Request(url.toString())
			req.method="GET"
			for(var id in args.request.headers){
				if(id!="host"){
					req.headers[id]=  args.request.headers[id]
				}
			}
			req.beginGetResponse()
			req.innerRequest.pipe(args.response).on("error", end)
			args.request.on("aborted", req.innerRequest.abort.bind(req.innerRequest))
			args.request.on("close", req.innerRequest.abort.bind(req.innerRequest))
			return 
		}
		
		
		continue_code= true
		task= new core.VW.Task()
		
		var tt={
			path: u.pathname,
			error404: function(){
				continue_code= false
				args.response.writeHead(404, {})
				return args.response.end()
			}, 
			error: end,
			success: function(data){
				task.result= data
				task.finish()
			}, 
			headers: args.request.headers
		}
		if(args.request.query.project_root=="496712"){
			tt.fullpath= Path.join(await site.getSiteDirMode2(), u.pathname)
			tt.path= undefined
		}
		site.getFile(tt)
		var result= await task
		if(!continue_code)
			return 
			
		
		var fileServer= FrontController.fileCachedServer
		if(!fileServer){
			fileServer= FrontController.fileCachedServer= new core.VW.Http.StaticServe.Server()
			fileServer.addPath(Path.dirname(result.file))
		}
		args.request.url= "/"+ result.name
		await fileServer.handle(args)
		
		
	}
	
	
	
	
	
	
	
	
	
	/*
	
	async staticContent_(args){
		var site= args.request.params.site|| args.request.headers.host
		var pub = Path.join("proyectos", site, "public")
		var storage= new Varavel.Project.Modules.Api.Libraries.Storage()
		var u=Url.parse(args.request.url)
		pub=Path.join(pub, u.path)
		var file= storage.bucket().file(pub)
		var meta
		try{
			meta= await file.getMetadata()
		}catch(e){
		}
		if(!meta){
			return await args.continueAsync()
		}
		meta=meta[0]
		var options={
			action:"read"
		}, partial=false
		var updated=new Date(meta.updated)
		args.response.setHeader("last-modified", updated.toGMTString())
		args.response.setHeader("etag", Md5(pub))
		args.response.setHeader("accept-ranges", "bytes")
		
		if(args.request.headers["if-modified-since"]){
			// revisar si se puede devolver 304
			let date=new Date(args.request.headers["if-modified-since"])
		
			date= parseInt(date.getTime()/1000)
			updated= parseInt(updated.getTime()/1000)
			if(updated>= date){
				args.response.writeHead(304,{})
				args.response.end()
			}
		}
		if(args.request.headers["range"]){
			let range= args.request.headers["range"]
			range= range.split("=").slice(1).join("")
			let bytes= range.split("/")[0].split("-")
			let num= parseInt(bytes[0])
			if(isNaN(num))
				throw new core.System.Exception("Range Header invalid")
			
			options.start= num
			let num1= parseInt(bytes[1])
			if(isNaN(num1))
				throw new core.System.Exception("Range Header invalid")
			
			options.end= num1
			partial= true
			args.response.setHeader("content-range", "bytes "+ num + "/" + num1)
		}
		
		var stream= file.createReadStream(options)
		if(partial){
			args.response.statusCode= 206
		}
		else{
			args.response.statusCode= 200
		}
		stream.on("error", function(er){
			try{
				args.response.end()
			}catch(e){}
		})
		stream.pipe(args.response).on("error", function(){
			try{
				args.response.end()
			}catch(e){}
		})
		args.request.on("aborted", stream.end.bind(stream))
		args.request.on("close", stream.end.bind(stream))
		
	}
	
	*/
	
	async getSiteDescription(args, site){
		FrontController.sites=FrontController.sites||{}
		var s= FrontController.sites[site]
		if(s){
			if(Date.now()-s.updated<60000){
				return s.data
			}
		}
		
		
		var data={}
		var site2=Varavel.Project.App.Site.getFromArgs(args)
		var task= new core.VW.Task()
		
		site2.getFile({
			fullpath:Path.join(await site2.getSiteDirMode2(),"website.json"), 
			error404: function(){
				task.result=task.result||{}
				task.result.noexists=true
				task.finish()
			}, 
			error: function(e){
				task.exception= e
				task.finish()
			}, 
			success: function(r){
				task.result= r
				task.finish()
			}
		})
		var result= await task
		
		//var path= Path.join( .getSiteDir(), "website.json")
		if(!result.noexists){
			data= await Fs.async.readFile(result.file,'utf8')
			data= JSON.parse(data)
		}
		s= FrontController.sites[site]= FrontController.sites[site]||{}
		s.data= data
		s.updated= Date.now()
		return s.data
	}
	
	userfunction(cid){
		var f=new Varavel.Project.App.Function(cid, Varavel.Project.App.QueryBase.context(this))
		return f
	}

	async index(args){
		
		var date= parseInt(Date.now()/10000) * 10000
		var items= args.request.url.split("/")
		
		if(args.request.headers["if-modified-since"]){
			var d2= new Date(args.request.headers["if-modified-since"])
			d2= d2.getTime()
			if(!isNaN(d2)){
				if(d2>= date){
					args.response.writeHead(304,{})
					args.response.end()
					return 
				}
			}
		}
		
		
		
		args.response.statusCode= 200
		args.response.setHeader("content-type", "text/html;Charset=utf8")
		
		var site= args.request.params.site|| args.request.headers.host
		var data= await this.getSiteDescription(args, site)

		var file
		var args1= {
			image: {
				url: data.imageUrl||'',
				secure_url: data.imageUrl||'',
			},
			description: data.description || "KOWI",
			titulo: data.tittle || "KOWI",
			type: data.type || "article"
		}
		var ucontext= {
	      arguments:args1,
	      response: args.response,
	      out: args.response,
	      request: args.request,
	      stop: args.response.end.bind(args.response), 
	      site: args.site,
	      UserFunction: this.userfunction.bind(args)
	    }

		var context=new core.VW.E6Html.Context()
		for(var id in ucontext){
			context[id]=ucontext[id]
		}
		
		
		
		
		
		var u= Url.parse(args.request.url)
		var usecache= true
		var es6File= Path.join(await this.getSiteFrontEndDirMode2(args), "src", "views")
		vw.warning("FILE:",es6File)
		if(!file){
			file= u.pathname// args.request.url.substring(1)
			if(file.endsWith(".es6.html")){
				file= file.substring(0, file.length-9)
				
			}
			else if(file.endsWith(".html")){
				file= file.substring(0, file.length-5)
			}
			else{
				file= "index"
				usecache= false
			}
		}
		
		//vw.warning("ES6FILE", es6File, file)
		if(usecache){
			
			date= new Date(date)
			args.response.setHeader("last-modified", date.toGMTString())
			args.response.setHeader("cache-control","private,must-revalidate,max-age=60")
			args.response.setHeader("etag", Md5(args.request.url))
			
		}
		var eFile= new Varavel.Project.App.E6HtmlFolderWeb(es6File)
	    await eFile.invoke(file,context,args1)
		args.response.end()
	}

}
FrontController.fileCache={}
export default FrontController
