
/* global Varavel,core*/
import Path from 'path'

export default function(Router){
	
	
	Router.use("/site/:site", Varavel.Project.Modules.Api.Http.Controllers.ApiController.bindMethod("allowAndLoadToken"))
	Router.use("/site/:site", Varavel.Project.Modules.Api.Http.Controllers.ApiController.bindMethod("checkPermission"))
	Router.use("", Varavel.Project.Modules.Api.Http.Controllers.ApiController.bindMethod("allowAndLoadToken"))
	Router.use("", Varavel.Project.Modules.Api.Http.Controllers.ApiController.bindMethod("checkPermission"))
	
	
	Router.get("/", Varavel.Project.Modules.Front.Http.Controllers.FrontController.bindMethod("index"))
	Router.use("/dynamic/:id", Varavel.Project.Modules.Front.Http.Controllers.FrontController.bindMethod("index"))
	

	
	Router.use("/static/e/:id/", Varavel.Project.Modules.Front.Http.Controllers.FrontController.bindMethod("staticContent"))
	Router.use("/static", Varavel.Project.Modules.Front.Http.Controllers.FrontController.bindMethod("staticContent"))
	
	
	
	
	Router.use("/site/:site/static/e/:id", Varavel.Project.Modules.Front.Http.Controllers.FrontController.bindMethod("staticContent"))
	Router.use("/site/:site/static", Varavel.Project.Modules.Front.Http.Controllers.FrontController.bindMethod("staticContent"))
	
	
	
	
	
	
	
	Router.use("/ui", Varavel.Project.Modules.Front.Http.Controllers.FrontController.bindMethod("index"))
	
	
	
	
	Router.use("/site/:site/dynamic/:id", Varavel.Project.Modules.Front.Http.Controllers.FrontController.bindMethod("index"))
	
	
	
	Router.use("/site/:site/ui", Varavel.Project.Modules.Front.Http.Controllers.FrontController.bindMethod("index"))
	Router.get("/site/:site", Varavel.Project.Modules.Front.Http.Controllers.FrontController.bindMethod("index"))
	
	

	
	Router.use("/site/:site", Varavel.Project.Modules.Api.Http.Controllers.ApiController.bindMethod("defaultMethod"))
	Router.use("", Varavel.Project.Modules.Api.Http.Controllers.ApiController.bindMethod("defaultMethod"))
	
	
}
