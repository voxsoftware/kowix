/*global core,vw,Varavel*/
var VaravelNamespace = core.org.voxsoftware.Varavel
import Url from 'url'
var Fs = core.System.IO.Fs
import fs from 'fs'
import Path from 'path'
var times = 0


var Mongo, Bcrypt
try{
	Mongo= require("mongodb")
}catch(e){
	Mongo= {ObjectID:function(a){return a}, mock: true}
}

try{
	Bcrypt= require("bcryptjs")
}catch(e){
	Bcrypt= {}
}


//import LanguageParser from 'accept-language-parser'

class ApiController extends VaravelNamespace.Http.Controller {

	
	


	async checkPermission(args, nocontinue) {

		if (args.request.method == "OPTIONS")
			return args.continueAsync()

		if (!ApiController.empresas)
			ApiController.empresas = {}



		if(args.response){
			args.response.setHeader("x-service","kowix@"+ ApiController.version +"/" +Varavel.Funcs.env("KOWI_CODE") + "-" + process.pid.toString(16).toString())
		}

		await Varavel.Project.App.Project.initialize(args)
		
		
		/*
		ApiController.executed=ApiController.executed||{}
		if(!ApiController.executed[site.site]){
			if(!ctx)
				ctx= Varavel.Project.App.QueryBase.context(args)
			ApiController.executed[site.site]= true
			func = new Varavel.Project.App.Function("init.0", ctx)
			try{
				await func.load()
			}catch(e){
				console.error(e)
				func=null 
			}
			if(func){
				//console.info("Executing init")
				await func.execute()
			}
		}
		
		*/
		
		if(!nocontinue){
			var result = await args.continueAsync()
			return result
		}
	}
	
	
	async onUpgrade(req, socket, head, thisFunc){
		req.headers=head
		if(req._originalurl)
			return 
			
		req.params=req.params||{}
		var i= req.url.indexOf("/site/")
		var site 
		if(i>=0){
			
			site=req.params.site= req.url.split("/")[2]
			console.info("REQ URL:",req.url, site)
			req._originalurl= req.url
			req.url= "/"+req.url.split("/").slice(3).join("/")
		}
		else{
			site=req.headers.host 
			req.params.site=site
		}
		
		ApiController.executed=ApiController.executed||{}
		
		var args={
			request: req, 
			response: socket, 
			socket, 
			head,
			thisFunc,
			continue:function(){},
			continueAsync: function(){}
		}
		var site = Varavel.Project.App.Site.getFromArgs(args)
		if(site){
			if(!ApiController.executed[site.site]){
				console.info("Iniciando")
				await this.checkPermission(args, true)
				/*
				thisFunc.server.removeListener('upgrade',thisFunc)
				thisFunc.server.on('upgrade',thisFunc)
				thisFunc.server.emit('upgrade',req,socket,head)
				*/
				return //socket.end()
			}
			else{
				console.info("YET EXECUTED")
				if(!ApiController.executed[site.site+"--socket"]){
					ApiController.executed[site.site+"--socket"]= true 
					/*thisFunc.server.removeListener('upgrade',thisFunc)
					thisFunc.server.on('upgrade',thisFunc)
					thisFunc.server.emit('upgrade',req,socket,head)*/
				}
				return 
			}
		}
		else{
			return socket.end()
		}
		
		
	}
	
	
	onUpgradeAdd(func, ofunc, req,socket,head){
		ofunc.server.removeListener('upgrade', ofunc)
		ofunc.server.on('upgrade', func)
		ofunc.server.on('upgrade', ofunc)
		ofunc.server.emit('upgrade',req,socket,head)
	}
	
	
	async onUpgradeAsync(req, socket, head, thisFunc, site){
		console.info("URL:", req.url, "Params:", req.params)
		var args={
			request: req, 
			response: socket, 
			socket, 
			head,
			thisFunc,
			continue:function(){},
			continueAsync: function(){}
		}
		ApiController.executed[site]= true 
		try{
			
			
			await this.checkPermission(args, true)
			var func 
			try{
				func = new Varavel.Project.App.Function("socket.connection", Varavel.Project.App.QueryBase.context(args))
				await func.load(args._body)
			}catch(e){
				console.info("No attached on upgrade: ", e)
				return 
			}
			ApiController.executed[site]=  await func.execute(args._body)
			if(typeof ApiController.executed[site]=="function"){
				this.onUpgradeAdd(ApiController.executed[site], thisFunc,req,socket,head)
			}
		
			
		}catch(e){
			delete ApiController.executed[site]
			console.error("ERROR ON UPGRADE:", site, e)
		}
		
	}


	async q_informacion(args) {
		var json = new VaravelNamespace.Http.Response.JsonResponse(args)
		try {
			
			var ctx= Varavel.Project.App.QueryBase.context(args)
			if(args.constants && args.constants.queryPreload){
				args._body.type= 'query'
				//  preload a function 
				var func = new Varavel.Project.App.Function(args.constants.queryPreload, ctx)
				await func.invoke(args._body)
			}
			
			var info = await Varavel.Project.App.QueryBase.informacion(args, args._body.options || {
				tablename: args._body.tablename
			}, ctx)
			
			if(args.constants && args.constants.queryPostload){
				args._body.type= 'query'
				//  preload a function 
				var func = new Varavel.Project.App.Function(args.constants.queryPostload, ctx)
				info= await func.invoke(info)
			}
			
			
			json.write(info)
		}
		catch (e) {
			json.write(e)
		}
	}


	async q_edit(args) {
		var json = new VaravelNamespace.Http.Response.JsonResponse(args)
		try {
			
			var ctx= Varavel.Project.App.QueryBase.context(args)
			if(args.constants && args.constants.queryPreload){
				args._body.type= 'update'
				//  preload a function 
				var func = new Varavel.Project.App.Function(args.constants.queryPreload, ctx)
				await func.invoke(args._body)
			}
			
			
			var info = await Varavel.Project.App.QueryBase.edicion(args, args._body.options || {
				tablename: args._body.tablename
			}, ctx)
			
			if(args.constants && args.constants.queryPostload){
				args._body.type= 'update'
				//  preload a function 
				var func = new Varavel.Project.App.Function(args.constants.queryPostload, ctx)
				info= await func.invoke(info)
			}
			
			
			json.write(info)
		}
		catch (e) {
			json.write(e)
		}
	}


	async q_insert(args) {
		var json = new VaravelNamespace.Http.Response.JsonResponse(args)
		try {
			
			var ctx= Varavel.Project.App.QueryBase.context(args)
			if(args.constants && args.constants.queryPreload){
				args._body.type= 'insert'
				//  preload a function 
				var func = new Varavel.Project.App.Function(args.constants.queryPreload, ctx)
				await func.invoke(args._body)
			}
			
			
			var info = await Varavel.Project.App.QueryBase.insert(args, args._body.options || {
				tablename: args._body.tablename
			}, ctx)
			
			if(args.constants && args.constants.queryPostload){
				args._body.type= 'insert'
				//  preload a function 
				var func = new Varavel.Project.App.Function(args.constants.queryPostload, ctx)
				info= await func.invoke(info)
			}
			json.write(info)
		}
		catch (e) {
			json.write(e)
		}
	}


	async q_delete(args) {
		var json = new VaravelNamespace.Http.Response.JsonResponse(args)
		try {
			
			var ctx= Varavel.Project.App.QueryBase.context(args)
			if(args.constants && args.constants.queryPreload){
				args._body.type= 'delete'
				//  preload a function 
				var func = new Varavel.Project.App.Function(args.constants.queryPreload,ctx)
				await func.invoke(args._body)
			}
			
			
			var info = await Varavel.Project.App.QueryBase.borrar(args, args._body.options || {
				tablename: args._body.tablename
			}, ctx)
			
			if(args.constants && args.constants.queryPostload){
				args._body.type= 'delete'
				//  preload a function 
				var func = new Varavel.Project.App.Function(args.constants.queryPostload, ctx)
				info= await func.invoke(info)
			}
			json.write(info)
		}
		catch (e) {
			json.write(e)
		}
	}



	async getFileContent(args) {
		var json = new VaravelNamespace.Http.Response.JsonResponse(args)

		var file = args.request.query.file
		var site = new Varavel.Project.App.Site()
		var task = new core.VW.Task()

		site.getFile({
			fullpath: file,
			error404: function() {
				task.result = task.result || {}
				task.result.noexists = true
				task.finish()
			},
			error: function(e) {
				task.exception = e
				task.finish()
			},
			success: function(r) {
				task.result = r
				task.finish()
			}
		})

		try {
			var result = await task
			json.write(result)
		}
		catch (e) {
			json.write(e)
		}
	}



	async executeFunction(args) {
		var json = new VaravelNamespace.Http.Response.JsonResponse(args)
		var func
		try {
			var time = Date.now()
			if (args.request.params.id)
				throw new core.System.NotImplementedException()
			else
				func = new Varavel.Project.App.Function(args.request.params.cid, Varavel.Project.App.QueryBase.context(args))


			await func.load(args._body)
			//time = Date.now()
			var result = await func.execute(args._body)
			if(args._body && args._body.preventdefault)
				args.response.end()
			else 
				json.write(result)
			
			
			func.context=func.func=null
		}
		catch (e) {
			if(func)
				func.context=func.func=null
			vw.error(e)
			if(args._body && args._body.stopdefault)
				return 
			if(args._body && args._body.preventdefault)
				return args.response.end()
				
			json.write(e)
		}
	}
	
	
	async defaultMethod(args) {
		var json = new VaravelNamespace.Http.Response.JsonResponse(args)
		try {
			
			var func, er
			if(args.apidefault_noretry)
				return await args.continueAsync()
				
			try{
				func = new Varavel.Project.App.Function("api.default", Varavel.Project.App.QueryBase.context(args))
				await func.load(args._body)
			}catch(e){
				er= e	
				args.apidefault_noretry= true
			}
			
			if(er)
				return await args.continueAsync()

			args.apidefault_noretry= true
			var result = await func.execute(args._body)
			vw.info("RESULT:",args._body)
			if(args._body && args._body.stopdefault)
				return 
			if(args._body && args._body.preventdefault)
				args.response.end()
			else if(args.request)
				json.write(result)
			
			func.context=func.func=null 
		}
		catch (e) {
			
			if(func)
				func.context=func.func=null
				
			
			if(args._body && args._body.preventdefault)
				return args.response.end()
				
			json.write(e)
		}
	}


	


	unionBody(args) {
		var data = {}
		if (args.request.query.__json__mode == 1) {
			args.request.body = JSON.parse(args.request.query.__json__data)
		}
		if (args.request.query) {
			for (var id in args.request.query) {
				data[id] = args.request.query[id]
			}
		}
		if (args.request.body) {
			for (var id in args.request.body) {
				data[id] = args.request.body[id]
			}
		}
		//data = this.perfect(data)
		return data
	}


	perfect(data) {
		var o, e
		for (var id in data) {
			if (typeof data[id] == "object") {
				o = data[id]
				if (o) {
					if (o.$oid) {
						data[id] = Mongo.ObjectID(o.$oid)
					}
					else if (o.$bcrypt) {
						e = Bcrypt.genSaltSync(10)
						data[id] = Bcrypt.hashSync(o.$bcrypt, e)
					}
					else {
						this.perfect(o)
					}
				}
			}
		}
		return data
	}


	defaultInfo() {
		return null
	}
	
	

	allow(args) {

		if (Varavel.Funcs.env("DOMAIN_ID"))
			args.response.setHeader("cdn-id", Varavel.Funcs.env("DOMAIN_ID"))
		args.response.setHeader("access-control-allow-headers", `Accept, Accept-Language, Authorization, Range, content-type, content-range`)
		args.response.setHeader("access-control-allow-methods", "GET,POST,PUT,DELETE,OPTIONS")
		args.response.setHeader("access-control-allow-credentials", true)
		args.response.setHeader("connection", "keep-alive")
		args.response.setHeader("access-control-allow-origin", args.request.query["src-url"] || "*")
		args.srcUrl = args.request.query["src-url"]
		delete args.request.query["src-url"]
		return args.continue()
	}

	allowAndLoadToken(args) {
		if (Varavel.Funcs.env("DOMAIN_ID"))
			args.response.setHeader("cdn-id", Varavel.Funcs.env("DOMAIN_ID"))
		args.response.setHeader("access-control-allow-headers", `Accept, Accept-Language, Authorization, Range, content-type, content-range`)
		args.response.setHeader("access-control-allow-methods", "GET,POST,PUT,DELETE,OPTIONS")
		args.response.setHeader("access-control-allow-credentials", true)
		args.response.setHeader("connection", "keep-alive")
		args.response.setHeader("access-control-allow-origin", args.request.query["src-url"] || "*")
		args.srcUrl = args.request.query["src-url"]
		delete args.request.query["src-url"]
		//var body = this.unionBody(args)
		//args._body = body
		return args.continueAsync()
	}



	maintenance(args) {
		var time = ApiController.revisionTime || 0,
			error
		if (Date.now() - time > 5 * 60 * 1000) {
			ApiController.maintenance = false
			if (Fs.sync.exists(Path.join(global.appDir, "maintenance"))) {
				ApiController.revisionTime = Date.now() - (3 * 60 * 1000)
				ApiController.maintenance = error = true
			}
			else {
				ApiController.revisionTime = Date.now()
			}
		}
		else {
			error = ApiController.maintenance
		}
		if (error) {
			args.response.write("The server it's in maintenance mode. We will come back soon")
			args.response.end()
			return
		}

		return args.continueAsync()
	}


	loadtoken(args) {
		return args.continue()
	}

	



}

ApiController.version=require(global.appDir+ "/package.json").version
export default ApiController
