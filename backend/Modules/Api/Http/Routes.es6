/*global Varavel,core*/
var Fs = core.System.IO.Fs
import Path from 'path'
var controllerGroup = Varavel.Project.Modules.Api.Http.Controllers


Array.prototype.shuffle = function() {
	var i = this.length,
		j, temp;
	if (i == 0) return this;
	while (--i) {
		j = Math.floor(Math.random() * (i + 1));
		temp = this[i];
		this[i] = this[j];
		this[j] = temp;
	}
	return this;
}



var exec = async function(Router, Server) {

	//var sessionStorage= await Varavel.Project.App.Database.sessionStorage()

	Router.use("/", controllerGroup.ApiController.bindMethod("maintenance"))


	//Router.use("/dynamic", core.VW.Http.Server.getBodyParser())
	Router.use("/dynamic", Varavel.Project.Modules.Api.Http.Controllers.ApiController.bindMethod("allowAndLoadToken"))
	Router.use("/dynamic", Varavel.Project.Modules.Api.Http.Controllers.ApiController.bindMethod("checkPermission"))

	//Router.use("/api", core.VW.Http.Server.getBodyParser())
	Router.use("/api", Varavel.Project.Modules.Api.Http.Controllers.ApiController.bindMethod("allowAndLoadToken"))
	Router.use("/api", Varavel.Project.Modules.Api.Http.Controllers.ApiController.bindMethod("checkPermission"))

	//Router.use("/site/:site", core.VW.Http.Server.getBodyParser())
	Router.use("/site/:site", Varavel.Project.Modules.Api.Http.Controllers.ApiController.bindMethod("allowAndLoadToken"))
	Router.use("/site/:site", Varavel.Project.Modules.Api.Http.Controllers.ApiController.bindMethod("checkPermission"))



	Router.get("/api/staticinfo", Varavel.Project.Modules.Api.Http.Controllers.ApiController.bindMethod("getFileContent"))


	Router.get("/api/function/:id", controllerGroup.ApiController.bindMethod("executeFunction"))
	Router.get("/api/function/c/:cid", controllerGroup.ApiController.bindMethod("executeFunction"))
	Router.post("/api/function/:id", controllerGroup.ApiController.bindMethod("executeFunction"))
	Router.post("/api/function/c/:cid", controllerGroup.ApiController.bindMethod("executeFunction"))



	//Router.use("/api/db", sessionStorage.handle.bind(sessionStorage))
	Router.get("/api/db/query", controllerGroup.ApiController.bindMethod("q_informacion"))
	Router.get("/api/db/seed", controllerGroup.ApiController.bindMethod("seed"))
	Router.post("/api/db/query", controllerGroup.ApiController.bindMethod("q_informacion"))
	Router.put("/api/db/edit", controllerGroup.ApiController.bindMethod("q_edit"))
	Router.put("/api/db", controllerGroup.ApiController.bindMethod("q_insert"))
	Router.post("/api/db", controllerGroup.ApiController.bindMethod("q_insert"))
	Router.delete("/api/db", controllerGroup.ApiController.bindMethod("q_delete"))

	Router.get("/api/u_db/query", controllerGroup.ApiController.bindMethod("q_informacion"))
	Router.post("/api/u_db/query", controllerGroup.ApiController.bindMethod("q_informacion"))
	Router.put("/api/u_db/edit", controllerGroup.ApiController.bindMethod("q_edit"))
	Router.put("/api/u_db", controllerGroup.ApiController.bindMethod("q_insert"))
	Router.post("/api/u_db", controllerGroup.ApiController.bindMethod("q_insert"))
	Router.delete("/api/u_db", controllerGroup.ApiController.bindMethod("q_delete"))


	/*
	  Router.get("/api/config/lang", controllerGroup.ApiController.bindMethod("obtenerLenguaje"))
	  
	  Router.use("/api/session", sessionStorage.handle.bind(sessionStorage))
	  Router.get("/api/sesion", controllerGroup.ApiController.bindMethod("sesion"))
	  Router.put("/api/sesion", controllerGroup.ApiController.bindMethod("putSesion"))
	  */



	Router.get("/site/:site/api/function/:id", controllerGroup.ApiController.bindMethod("executeFunction"))
	Router.get("/site/:site/api/function/c/:cid", controllerGroup.ApiController.bindMethod("executeFunction"))
	Router.post("/site/:site/api/function/:id", controllerGroup.ApiController.bindMethod("executeFunction"))
	Router.post("/site/:site/api/function/c/:cid", controllerGroup.ApiController.bindMethod("executeFunction"))




	Router.get("/site/:site/api/db/query", controllerGroup.ApiController.bindMethod("q_informacion"))
	Router.get("/site/:site/api/db/seed", controllerGroup.ApiController.bindMethod("seed"))
	Router.post("/site/:site/api/db/query", controllerGroup.ApiController.bindMethod("q_informacion"))
	Router.put("/site/:site/api/db/edit", controllerGroup.ApiController.bindMethod("q_edit"))
	Router.put("/site/:site/api/db", controllerGroup.ApiController.bindMethod("q_insert"))
	Router.post("/site/:site/api/db", controllerGroup.ApiController.bindMethod("q_insert"))
	Router.delete("/site/:site/api/db", controllerGroup.ApiController.bindMethod("q_delete"))

	Router.get("/site/:site/api/u_db/query", controllerGroup.ApiController.bindMethod("q_informacion"))
	Router.post("/site/:site/api/u_db/query", controllerGroup.ApiController.bindMethod("q_informacion"))
	Router.put("/site/:site/api/u_db/edit", controllerGroup.ApiController.bindMethod("q_edit"))
	Router.put("/site/:site/api/u_db", controllerGroup.ApiController.bindMethod("q_insert"))
	Router.post("/site/:site/api/u_db", controllerGroup.ApiController.bindMethod("q_insert"))
	Router.delete("/site/:site/api/u_db", controllerGroup.ApiController.bindMethod("q_delete"))
	Router.get("/site/:site/api/config/lang", controllerGroup.ApiController.bindMethod("obtenerLenguaje"))
	/*
  Router.use("/site/:site/api/session", sessionStorage.handle.bind(sessionStorage))
  Router.get("/site/:site/api/sesion", controllerGroup.ApiController.bindMethod("sesion"))
  Router.put("/site/:site/api/sesion", controllerGroup.ApiController.bindMethod("putSesion"))
*/
	

	setTimeout(async function() {


		/// EXECUTE INIT.0 ON ALL PROJECTS 

		/*
		var f= controllerGroup.ApiController.bindMethod("onUpgrade")
		var y
		y= function(req,socket,head){
		  return f(req, socket, head, y)
		}
		y.server= Server.innerServer
		Server.innerServer.on("connect", y)
		*/


		/*
		var domains = Path.join(global.appDir, "..")
		var dirs = Fs.sync.readdir(domains)
		var file, ufile, stat
		
		var args={
			request: null, 
			response: null, 
			continue:function(){},
			continueAsync: function(){}
		}
		var site , file2
		
		
		for (var i = 0; i < dirs.length; i++) {

			file = dirs[i]
			ufile= Path.join(domains, file)
			stat= Fs.sync.stat(ufile)
			if(stat.isDirectory()){
				file2= Path.join(ufile, "functions","vars.json")
				if(Fs.sync.exists(file2)){
					
					// execute init.0
					console.info("Execugin init.0 on: ",file)
					site= Varavel.Project.App.Site.getFromId(file)
					
				}
			}

		}
		*/
		
		try{
			//var f= controllerGroup.ApiController.bindMethod("checkPermission")
			var args={
				request: {}, 
				response: {}, 
				ExecuteFunctionAsSite: async function(site, funcname, options){
					/*
					var site= Varavel.Project.App.Site.getFromId(site )
					var args={
						request: {},
						response:null, 
						continue:function(){},
						continueAsync: function(){},
						site: site 
					}
					await site.getRealSite()
					await f(args, true)
					
					
					var ctx= Varavel.Project.App.QueryBase.context(args)
					var func = new Varavel.Project.App.Function(funcname, ctx)
					return await func.invoke(options)
					*/
					var args={
						request: {on:function(){}},
						response:null, 
						continue:function(){},
						continueAsync: function(){},
						site: site 
					}
					var pj= new Varavel.Project.App.Project(site,args)
					return await (await pj.userFunction(funcname)).invoke(options)
				},
				continue:function(){},
				continueAsync: function(){}
			}
			
			var site= Varavel.Project.App.Site.getFromId("kowix")
			await site.getRealSite()
			
			
			args.site= site
			var ctx= Varavel.Project.App.QueryBase.context(args)
			var func = new Varavel.Project.App.Function("socket.io", ctx)
			
			// creating socket.io server
			func.invoke({
				internallcall: true
			}).catch(function(e){
				console.error("1.Error initing socket.io: ",e)
			})
		}catch(e){
			console.error("Error initing socket.io: ",e)
		}
		
		
	

	}, 600)



}
export default exec
