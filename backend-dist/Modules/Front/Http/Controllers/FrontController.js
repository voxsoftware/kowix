var $mod$4 = core.VW.Ecma2015.Utils.module(require('fs'));
var $mod$5 = core.VW.Ecma2015.Utils.module(require('path'));
var $mod$6 = core.VW.Ecma2015.Utils.module(require('url'));
var Fs = core.System.IO.Fs;
var $mod$7 = core.VW.Ecma2015.Utils.module(require('md5'));
var tempdir = $mod$5.default.join(global.appDir, 'cache', 'tmp');
{
    var FrontController = function FrontController() {
        FrontController.$constructor ? FrontController.$constructor.apply(this, arguments) : FrontController.$superClass && FrontController.$superClass.apply(this, arguments);
    };
    FrontController.prototype = Object.create(core.org.voxsoftware.Varavel.Http.Controller.prototype);
    Object.setPrototypeOf ? Object.setPrototypeOf(FrontController, core.org.voxsoftware.Varavel.Http.Controller) : FrontController.__proto__ = core.org.voxsoftware.Varavel.Http.Controller;
    FrontController.prototype.constructor = FrontController;
    FrontController.$super = core.org.voxsoftware.Varavel.Http.Controller.prototype;
    FrontController.$superClass = core.org.voxsoftware.Varavel.Http.Controller;
    Object.defineProperty(FrontController.prototype, 'getSiteFrontEndDir', {
        enumerable: false,
        value: function (args) {
            return $mod$5.default.join(Varavel.Project.App.Site.getFromArgs(args).getSiteDir(), 'frontend');
        }
    });
    Object.defineProperty(FrontController.prototype, 'getSiteFrontEndDirMode2', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0(args) {
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        context$1$0.t0 = $mod$5.default;
                        context$1$0.next = 3;
                        return regeneratorRuntime.awrap(Varavel.Project.App.Site.getFromArgs(args).getSiteDirMode2());
                    case 3:
                        context$1$0.t1 = context$1$0.sent;
                        return context$1$0.abrupt('return', context$1$0.t0.join.call(context$1$0.t0, context$1$0.t1, 'frontend'));
                    case 5:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this);
        })
    });
    Object.defineProperty(FrontController.prototype, 'staticContent', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0(args) {
            var fileServer, path;
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        fileServer = new core.VW.Http.StaticServe.Server();
                        context$1$0.next = 3;
                        return regeneratorRuntime.awrap(Varavel.Project.App.Site.getFromArgs(args).getSiteDirMode2());
                    case 3:
                        path = context$1$0.sent;
                        path = $mod$5.default.join(path, 'public');
                        fileServer.addPath(path);
                        context$1$0.next = 8;
                        return regeneratorRuntime.awrap(fileServer.handle(args));
                    case 8:
                        return context$1$0.abrupt('return', context$1$0.sent);
                    case 9:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this);
        })
    });
    Object.defineProperty(FrontController.prototype, 'staticContent_2', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0(args) {
            var site, u, continue_code, task, end, pub, storage, file, exists, url, req, id, tt, result, fileServer;
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        site = Varavel.Project.App.Site.getFromArgs(args);
                        u = $mod$6.default.parse(args.request.url);
                        end = function (er) {
                            vw.error('Error en request', er);
                            continue_code = false;
                            if (task)
                                task.finish();
                            args.response.statusCode = 500;
                            args.response.end();
                        };
                        if (!(args.request.headers['range'] != undefined)) {
                            context$1$0.next = 44;
                            break;
                        }
                        if (!(args.request.query.project_root == '496712')) {
                            context$1$0.next = 12;
                            break;
                        }
                        context$1$0.t0 = $mod$5.default;
                        context$1$0.next = 8;
                        return regeneratorRuntime.awrap(site.getSiteDirMode2());
                    case 8:
                        context$1$0.t1 = context$1$0.sent;
                        pub = context$1$0.t0.join.call(context$1$0.t0, context$1$0.t1);
                        context$1$0.next = 17;
                        break;
                    case 12:
                        context$1$0.t2 = $mod$5.default;
                        context$1$0.next = 15;
                        return regeneratorRuntime.awrap(site.getSiteDirMode2());
                    case 15:
                        context$1$0.t3 = context$1$0.sent;
                        pub = context$1$0.t2.join.call(context$1$0.t2, context$1$0.t3, 'public');
                    case 17:
                        storage = new Varavel.Project.Modules.Api.Libraries.Storage();
                        pub = $mod$5.default.join(pub, u.pathname);
                        file = storage.bucket().file(pub);
                        context$1$0.prev = 20;
                        context$1$0.next = 23;
                        return regeneratorRuntime.awrap(file.exists());
                    case 23:
                        exists = context$1$0.sent;
                        exists = exists[0];
                        context$1$0.next = 29;
                        break;
                    case 27:
                        context$1$0.prev = 27;
                        context$1$0.t4 = context$1$0['catch'](20);
                    case 29:
                        if (exists) {
                            context$1$0.next = 33;
                            break;
                        }
                        context$1$0.next = 32;
                        return regeneratorRuntime.awrap(args.continueAsync());
                    case 32:
                        return context$1$0.abrupt('return', context$1$0.sent);
                    case 33:
                        context$1$0.next = 35;
                        return regeneratorRuntime.awrap(file.getSignedUrl({
                            action: 'read',
                            expires: Date.now() + parseInt(Varavel.Funcs.env('CACHE_SIGNEDURL', 4 * 3600 * 1000))
                        }));
                    case 35:
                        url = context$1$0.sent;
                        req = new core.VW.Http.Request(url.toString());
                        req.method = 'GET';
                        for (id in args.request.headers) {
                            if (id != 'host') {
                                req.headers[id] = args.request.headers[id];
                            }
                        }
                        req.beginGetResponse();
                        req.innerRequest.pipe(args.response).on('error', end);
                        args.request.on('aborted', req.innerRequest.abort.bind(req.innerRequest));
                        args.request.on('close', req.innerRequest.abort.bind(req.innerRequest));
                        return context$1$0.abrupt('return');
                    case 44:
                        continue_code = true;
                        task = new core.VW.Task();
                        tt = {
                            path: u.pathname,
                            error404: function () {
                                continue_code = false;
                                args.response.writeHead(404, {});
                                return args.response.end();
                            },
                            error: end,
                            success: function (data) {
                                task.result = data;
                                task.finish();
                            },
                            headers: args.request.headers
                        };
                        if (!(args.request.query.project_root == '496712')) {
                            context$1$0.next = 55;
                            break;
                        }
                        context$1$0.t5 = $mod$5.default;
                        context$1$0.next = 51;
                        return regeneratorRuntime.awrap(site.getSiteDirMode2());
                    case 51:
                        context$1$0.t6 = context$1$0.sent;
                        context$1$0.t7 = u.pathname;
                        tt.fullpath = context$1$0.t5.join.call(context$1$0.t5, context$1$0.t6, context$1$0.t7);
                        tt.path = undefined;
                    case 55:
                        site.getFile(tt);
                        context$1$0.next = 58;
                        return regeneratorRuntime.awrap(task);
                    case 58:
                        result = context$1$0.sent;
                        if (continue_code) {
                            context$1$0.next = 61;
                            break;
                        }
                        return context$1$0.abrupt('return');
                    case 61:
                        fileServer = FrontController.fileCachedServer;
                        if (!fileServer) {
                            fileServer = FrontController.fileCachedServer = new core.VW.Http.StaticServe.Server();
                            fileServer.addPath($mod$5.default.dirname(result.file));
                        }
                        args.request.url = '/' + result.name;
                        context$1$0.next = 66;
                        return regeneratorRuntime.awrap(fileServer.handle(args));
                    case 66:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this, [[
                    20,
                    27
                ]]);
        })
    });
    Object.defineProperty(FrontController.prototype, 'getSiteDescription', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0(args, site) {
            var s, data, site2, task, result;
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        FrontController.sites = FrontController.sites || {};
                        s = FrontController.sites[site];
                        if (!s) {
                            context$1$0.next = 5;
                            break;
                        }
                        if (!(Date.now() - s.updated < 60000)) {
                            context$1$0.next = 5;
                            break;
                        }
                        return context$1$0.abrupt('return', s.data);
                    case 5:
                        data = {};
                        site2 = Varavel.Project.App.Site.getFromArgs(args);
                        task = new core.VW.Task();
                        context$1$0.t0 = site2;
                        context$1$0.t1 = $mod$5.default;
                        context$1$0.next = 12;
                        return regeneratorRuntime.awrap(site2.getSiteDirMode2());
                    case 12:
                        context$1$0.t2 = context$1$0.sent;
                        context$1$0.t3 = context$1$0.t1.join.call(context$1$0.t1, context$1$0.t2, 'website.json');
                        context$1$0.t4 = function () {
                            task.result = task.result || {};
                            task.result.noexists = true;
                            task.finish();
                        };
                        context$1$0.t5 = function (e) {
                            task.exception = e;
                            task.finish();
                        };
                        context$1$0.t6 = function (r) {
                            task.result = r;
                            task.finish();
                        };
                        context$1$0.t7 = {
                            fullpath: context$1$0.t3,
                            error404: context$1$0.t4,
                            error: context$1$0.t5,
                            success: context$1$0.t6
                        };
                        context$1$0.t0.getFile.call(context$1$0.t0, context$1$0.t7);
                        context$1$0.next = 21;
                        return regeneratorRuntime.awrap(task);
                    case 21:
                        result = context$1$0.sent;
                        if (result.noexists) {
                            context$1$0.next = 27;
                            break;
                        }
                        context$1$0.next = 25;
                        return regeneratorRuntime.awrap(Fs.async.readFile(result.file, 'utf8'));
                    case 25:
                        data = context$1$0.sent;
                        data = JSON.parse(data);
                    case 27:
                        s = FrontController.sites[site] = FrontController.sites[site] || {};
                        s.data = data;
                        s.updated = Date.now();
                        return context$1$0.abrupt('return', s.data);
                    case 31:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this);
        })
    });
    Object.defineProperty(FrontController.prototype, 'userfunction', {
        enumerable: false,
        value: function (cid) {
            var f = new Varavel.Project.App.Function(cid, Varavel.Project.App.QueryBase.context(this));
            return f;
        }
    });
    Object.defineProperty(FrontController.prototype, 'index', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0(args) {
            var date, items, d2, site, data, file, args1, ucontext, context, id, u, usecache, es6File, eFile;
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        date = parseInt(Date.now() / 10000) * 10000;
                        items = args.request.url.split('/');
                        if (!args.request.headers['if-modified-since']) {
                            context$1$0.next = 10;
                            break;
                        }
                        d2 = new Date(args.request.headers['if-modified-since']);
                        d2 = d2.getTime();
                        if (isNaN(d2)) {
                            context$1$0.next = 10;
                            break;
                        }
                        if (!(d2 >= date)) {
                            context$1$0.next = 10;
                            break;
                        }
                        args.response.writeHead(304, {});
                        args.response.end();
                        return context$1$0.abrupt('return');
                    case 10:
                        args.response.statusCode = 200;
                        args.response.setHeader('content-type', 'text/html;Charset=utf8');
                        site = args.request.params.site || args.request.headers.host;
                        context$1$0.next = 15;
                        return regeneratorRuntime.awrap(this.getSiteDescription(args, site));
                    case 15:
                        data = context$1$0.sent;
                        args1 = {
                            image: {
                                url: data.imageUrl || '',
                                secure_url: data.imageUrl || ''
                            },
                            description: data.description || 'KOWI',
                            titulo: data.tittle || 'KOWI',
                            type: data.type || 'article'
                        };
                        ucontext = {
                            arguments: args1,
                            response: args.response,
                            out: args.response,
                            request: args.request,
                            stop: args.response.end.bind(args.response),
                            site: args.site,
                            UserFunction: this.userfunction.bind(args)
                        };
                        context = new core.VW.E6Html.Context();
                        for (id in ucontext) {
                            context[id] = ucontext[id];
                        }
                        u = $mod$6.default.parse(args.request.url);
                        usecache = true;
                        context$1$0.t0 = $mod$5.default;
                        context$1$0.next = 25;
                        return regeneratorRuntime.awrap(this.getSiteFrontEndDirMode2(args));
                    case 25:
                        context$1$0.t1 = context$1$0.sent;
                        es6File = context$1$0.t0.join.call(context$1$0.t0, context$1$0.t1, 'src', 'views');
                        vw.warning('FILE:', es6File);
                        if (!file) {
                            file = u.pathname;
                            if (file.endsWith('.es6.html')) {
                                file = file.substring(0, file.length - 9);
                            } else if (file.endsWith('.html')) {
                                file = file.substring(0, file.length - 5);
                            } else {
                                file = 'index';
                                usecache = false;
                            }
                        }
                        if (usecache) {
                            date = new Date(date);
                            args.response.setHeader('last-modified', date.toGMTString());
                            args.response.setHeader('cache-control', 'private,must-revalidate,max-age=60');
                            args.response.setHeader('etag', $mod$7.default(args.request.url));
                        }
                        eFile = new Varavel.Project.App.E6HtmlFolderWeb(es6File);
                        context$1$0.next = 33;
                        return regeneratorRuntime.awrap(eFile.invoke(file, context, args1));
                    case 33:
                        args.response.end();
                    case 34:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this);
        })
    });
}
FrontController.fileCache = {};
exports.default = FrontController;