var Fs = core.System.IO.Fs;
var $mod$3 = core.VW.Ecma2015.Utils.module(require('path'));
var controllerGroup = Varavel.Project.Modules.Api.Http.Controllers;
Array.prototype.shuffle = function () {
    var i = this.length, j, temp;
    if (i == 0)
        return this;
    while (--i) {
        j = Math.floor(Math.random() * (i + 1));
        temp = this[i];
        this[i] = this[j];
        this[j] = temp;
    }
    return this;
};
var exec = (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0(Router, Server) {
    return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
        while (1)
            switch (context$1$0.prev = context$1$0.next) {
            case 0:
                Router.use('/', controllerGroup.ApiController.bindMethod('maintenance'));
                Router.use('/dynamic', Varavel.Project.Modules.Api.Http.Controllers.ApiController.bindMethod('allowAndLoadToken'));
                Router.use('/dynamic', Varavel.Project.Modules.Api.Http.Controllers.ApiController.bindMethod('checkPermission'));
                Router.use('/api', Varavel.Project.Modules.Api.Http.Controllers.ApiController.bindMethod('allowAndLoadToken'));
                Router.use('/api', Varavel.Project.Modules.Api.Http.Controllers.ApiController.bindMethod('checkPermission'));
                Router.use('/site/:site', Varavel.Project.Modules.Api.Http.Controllers.ApiController.bindMethod('allowAndLoadToken'));
                Router.use('/site/:site', Varavel.Project.Modules.Api.Http.Controllers.ApiController.bindMethod('checkPermission'));
                Router.get('/api/staticinfo', Varavel.Project.Modules.Api.Http.Controllers.ApiController.bindMethod('getFileContent'));
                Router.get('/api/function/:id', controllerGroup.ApiController.bindMethod('executeFunction'));
                Router.get('/api/function/c/:cid', controllerGroup.ApiController.bindMethod('executeFunction'));
                Router.post('/api/function/:id', controllerGroup.ApiController.bindMethod('executeFunction'));
                Router.post('/api/function/c/:cid', controllerGroup.ApiController.bindMethod('executeFunction'));
                Router.get('/api/db/query', controllerGroup.ApiController.bindMethod('q_informacion'));
                Router.get('/api/db/seed', controllerGroup.ApiController.bindMethod('seed'));
                Router.post('/api/db/query', controllerGroup.ApiController.bindMethod('q_informacion'));
                Router.put('/api/db/edit', controllerGroup.ApiController.bindMethod('q_edit'));
                Router.put('/api/db', controllerGroup.ApiController.bindMethod('q_insert'));
                Router.post('/api/db', controllerGroup.ApiController.bindMethod('q_insert'));
                Router.delete('/api/db', controllerGroup.ApiController.bindMethod('q_delete'));
                Router.get('/api/u_db/query', controllerGroup.ApiController.bindMethod('q_informacion'));
                Router.post('/api/u_db/query', controllerGroup.ApiController.bindMethod('q_informacion'));
                Router.put('/api/u_db/edit', controllerGroup.ApiController.bindMethod('q_edit'));
                Router.put('/api/u_db', controllerGroup.ApiController.bindMethod('q_insert'));
                Router.post('/api/u_db', controllerGroup.ApiController.bindMethod('q_insert'));
                Router.delete('/api/u_db', controllerGroup.ApiController.bindMethod('q_delete'));
                Router.get('/site/:site/api/function/:id', controllerGroup.ApiController.bindMethod('executeFunction'));
                Router.get('/site/:site/api/function/c/:cid', controllerGroup.ApiController.bindMethod('executeFunction'));
                Router.post('/site/:site/api/function/:id', controllerGroup.ApiController.bindMethod('executeFunction'));
                Router.post('/site/:site/api/function/c/:cid', controllerGroup.ApiController.bindMethod('executeFunction'));
                Router.get('/site/:site/api/db/query', controllerGroup.ApiController.bindMethod('q_informacion'));
                Router.get('/site/:site/api/db/seed', controllerGroup.ApiController.bindMethod('seed'));
                Router.post('/site/:site/api/db/query', controllerGroup.ApiController.bindMethod('q_informacion'));
                Router.put('/site/:site/api/db/edit', controllerGroup.ApiController.bindMethod('q_edit'));
                Router.put('/site/:site/api/db', controllerGroup.ApiController.bindMethod('q_insert'));
                Router.post('/site/:site/api/db', controllerGroup.ApiController.bindMethod('q_insert'));
                Router.delete('/site/:site/api/db', controllerGroup.ApiController.bindMethod('q_delete'));
                Router.get('/site/:site/api/u_db/query', controllerGroup.ApiController.bindMethod('q_informacion'));
                Router.post('/site/:site/api/u_db/query', controllerGroup.ApiController.bindMethod('q_informacion'));
                Router.put('/site/:site/api/u_db/edit', controllerGroup.ApiController.bindMethod('q_edit'));
                Router.put('/site/:site/api/u_db', controllerGroup.ApiController.bindMethod('q_insert'));
                Router.post('/site/:site/api/u_db', controllerGroup.ApiController.bindMethod('q_insert'));
                Router.delete('/site/:site/api/u_db', controllerGroup.ApiController.bindMethod('q_delete'));
                Router.get('/site/:site/api/config/lang', controllerGroup.ApiController.bindMethod('obtenerLenguaje'));
                setTimeout((typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0() {
                    var args, site, ctx, func;
                    return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                        while (1)
                            switch (context$1$0.prev = context$1$0.next) {
                            case 0:
                                context$1$0.prev = 0;
                                args = {
                                    request: {},
                                    response: {},
                                    ExecuteFunctionAsSite: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0(site, funcname, options) {
                                        var args, pj;
                                        return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                                            while (1)
                                                switch (context$1$0.prev = context$1$0.next) {
                                                case 0:
                                                    args = {
                                                        request: {
                                                            on: function () {
                                                            }
                                                        },
                                                        response: null,
                                                        continue: function () {
                                                        },
                                                        continueAsync: function () {
                                                        },
                                                        site: site
                                                    };
                                                    pj = new Varavel.Project.App.Project(site, args);
                                                    context$1$0.t0 = regeneratorRuntime;
                                                    context$1$0.next = 5;
                                                    return regeneratorRuntime.awrap(pj.userFunction(funcname));
                                                case 5:
                                                    context$1$0.t1 = options;
                                                    context$1$0.t2 = context$1$0.sent.invoke(context$1$0.t1);
                                                    context$1$0.next = 9;
                                                    return context$1$0.t0.awrap.call(context$1$0.t0, context$1$0.t2);
                                                case 9:
                                                    return context$1$0.abrupt('return', context$1$0.sent);
                                                case 10:
                                                case 'end':
                                                    return context$1$0.stop();
                                                }
                                        }, null, this);
                                    }),
                                    continue: function () {
                                    },
                                    continueAsync: function () {
                                    }
                                };
                                site = Varavel.Project.App.Site.getFromId('kowix');
                                context$1$0.next = 5;
                                return regeneratorRuntime.awrap(site.getRealSite());
                            case 5:
                                args.site = site;
                                ctx = Varavel.Project.App.QueryBase.context(args);
                                func = new Varavel.Project.App.Function('socket.io', ctx);
                                func.invoke({ internallcall: true }).catch(function (e) {
                                    console.error('1.Error initing socket.io: ', e);
                                });
                                context$1$0.next = 14;
                                break;
                            case 11:
                                context$1$0.prev = 11;
                                context$1$0.t0 = context$1$0['catch'](0);
                                console.error('Error initing socket.io: ', context$1$0.t0);
                            case 14:
                            case 'end':
                                return context$1$0.stop();
                            }
                    }, null, this, [[
                            0,
                            11
                        ]]);
                }), 600);
            case 44:
            case 'end':
                return context$1$0.stop();
            }
    }, null, this);
});
exports.default = exec;