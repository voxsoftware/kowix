var VaravelNamespace = core.org.voxsoftware.Varavel;
var $mod$0 = core.VW.Ecma2015.Utils.module(require('url'));
var Fs = core.System.IO.Fs;
var $mod$1 = core.VW.Ecma2015.Utils.module(require('fs'));
var $mod$2 = core.VW.Ecma2015.Utils.module(require('path'));
var times = 0;
var Mongo, Bcrypt;
try {
    Mongo = require('mongodb');
} catch (e) {
    Mongo = {
        ObjectID: function (a) {
            return a;
        },
        mock: true
    };
}
try {
    Bcrypt = require('bcryptjs');
} catch (e) {
    Bcrypt = {};
}
{
    var ApiController = function ApiController() {
        ApiController.$constructor ? ApiController.$constructor.apply(this, arguments) : ApiController.$superClass && ApiController.$superClass.apply(this, arguments);
    };
    ApiController.prototype = Object.create(VaravelNamespace.Http.Controller.prototype);
    Object.setPrototypeOf ? Object.setPrototypeOf(ApiController, VaravelNamespace.Http.Controller) : ApiController.__proto__ = VaravelNamespace.Http.Controller;
    ApiController.prototype.constructor = ApiController;
    ApiController.$super = VaravelNamespace.Http.Controller.prototype;
    ApiController.$superClass = VaravelNamespace.Http.Controller;
    Object.defineProperty(ApiController.prototype, 'checkPermission', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0(args, nocontinue) {
            var result;
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        if (!(args.request.method == 'OPTIONS')) {
                            context$1$0.next = 2;
                            break;
                        }
                        return context$1$0.abrupt('return', args.continueAsync());
                    case 2:
                        if (!ApiController.empresas)
                            ApiController.empresas = {};
                        if (args.response) {
                            args.response.setHeader('x-service', 'kowix@' + ApiController.version + '/' + Varavel.Funcs.env('KOWI_CODE') + '-' + process.pid.toString(16).toString());
                        }
                        context$1$0.next = 6;
                        return regeneratorRuntime.awrap(Varavel.Project.App.Project.initialize(args));
                    case 6:
                        if (nocontinue) {
                            context$1$0.next = 11;
                            break;
                        }
                        context$1$0.next = 9;
                        return regeneratorRuntime.awrap(args.continueAsync());
                    case 9:
                        result = context$1$0.sent;
                        return context$1$0.abrupt('return', result);
                    case 11:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this);
        })
    });
    Object.defineProperty(ApiController.prototype, 'onUpgrade', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0(req, socket, head, thisFunc) {
            var i, site, args;
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        req.headers = head;
                        if (!req._originalurl) {
                            context$1$0.next = 3;
                            break;
                        }
                        return context$1$0.abrupt('return');
                    case 3:
                        req.params = req.params || {};
                        i = req.url.indexOf('/site/');
                        if (i >= 0) {
                            site = req.params.site = req.url.split('/')[2];
                            console.info('REQ URL:', req.url, site);
                            req._originalurl = req.url;
                            req.url = '/' + req.url.split('/').slice(3).join('/');
                        } else {
                            site = req.headers.host;
                            req.params.site = site;
                        }
                        ApiController.executed = ApiController.executed || {};
                        args = {
                            request: req,
                            response: socket,
                            socket: socket,
                            head: head,
                            thisFunc: thisFunc,
                            continue: function () {
                            },
                            continueAsync: function () {
                            }
                        };
                        site = Varavel.Project.App.Site.getFromArgs(args);
                        if (!site) {
                            context$1$0.next = 22;
                            break;
                        }
                        if (ApiController.executed[site.site]) {
                            context$1$0.next = 17;
                            break;
                        }
                        console.info('Iniciando');
                        context$1$0.next = 14;
                        return regeneratorRuntime.awrap(this.checkPermission(args, true));
                    case 14:
                        return context$1$0.abrupt('return');
                    case 17:
                        console.info('YET EXECUTED');
                        if (!ApiController.executed[site.site + '--socket']) {
                            ApiController.executed[site.site + '--socket'] = true;
                        }
                        return context$1$0.abrupt('return');
                    case 20:
                        context$1$0.next = 23;
                        break;
                    case 22:
                        return context$1$0.abrupt('return', socket.end());
                    case 23:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this);
        })
    });
    Object.defineProperty(ApiController.prototype, 'onUpgradeAdd', {
        enumerable: false,
        value: function (func, ofunc, req, socket, head) {
            ofunc.server.removeListener('upgrade', ofunc);
            ofunc.server.on('upgrade', func);
            ofunc.server.on('upgrade', ofunc);
            ofunc.server.emit('upgrade', req, socket, head);
        }
    });
    Object.defineProperty(ApiController.prototype, 'onUpgradeAsync', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0(req, socket, head, thisFunc, site) {
            var args, func;
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        console.info('URL:', req.url, 'Params:', req.params);
                        args = {
                            request: req,
                            response: socket,
                            socket: socket,
                            head: head,
                            thisFunc: thisFunc,
                            continue: function () {
                            },
                            continueAsync: function () {
                            }
                        };
                        ApiController.executed[site] = true;
                        context$1$0.prev = 3;
                        context$1$0.next = 6;
                        return regeneratorRuntime.awrap(this.checkPermission(args, true));
                    case 6:
                        context$1$0.prev = 6;
                        func = new Varavel.Project.App.Function('socket.connection', Varavel.Project.App.QueryBase.context(args));
                        context$1$0.next = 10;
                        return regeneratorRuntime.awrap(func.load(args._body));
                    case 10:
                        context$1$0.next = 16;
                        break;
                    case 12:
                        context$1$0.prev = 12;
                        context$1$0.t0 = context$1$0['catch'](6);
                        console.info('No attached on upgrade: ', context$1$0.t0);
                        return context$1$0.abrupt('return');
                    case 16:
                        context$1$0.next = 18;
                        return regeneratorRuntime.awrap(func.execute(args._body));
                    case 18:
                        ApiController.executed[site] = context$1$0.sent;
                        if (typeof ApiController.executed[site] == 'function') {
                            this.onUpgradeAdd(ApiController.executed[site], thisFunc, req, socket, head);
                        }
                        context$1$0.next = 26;
                        break;
                    case 22:
                        context$1$0.prev = 22;
                        context$1$0.t1 = context$1$0['catch'](3);
                        delete ApiController.executed[site];
                        console.error('ERROR ON UPGRADE:', site, context$1$0.t1);
                    case 26:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this, [
                [
                    3,
                    22
                ],
                [
                    6,
                    12
                ]
            ]);
        })
    });
    Object.defineProperty(ApiController.prototype, 'q_informacion', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0(args) {
            var json, ctx, func, info;
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        json = new VaravelNamespace.Http.Response.JsonResponse(args);
                        context$1$0.prev = 1;
                        ctx = Varavel.Project.App.QueryBase.context(args);
                        if (!(args.constants && args.constants.queryPreload)) {
                            context$1$0.next = 8;
                            break;
                        }
                        args._body.type = 'query';
                        func = new Varavel.Project.App.Function(args.constants.queryPreload, ctx);
                        context$1$0.next = 8;
                        return regeneratorRuntime.awrap(func.invoke(args._body));
                    case 8:
                        context$1$0.next = 10;
                        return regeneratorRuntime.awrap(Varavel.Project.App.QueryBase.informacion(args, args._body.options || { tablename: args._body.tablename }, ctx));
                    case 10:
                        info = context$1$0.sent;
                        if (!(args.constants && args.constants.queryPostload)) {
                            context$1$0.next = 17;
                            break;
                        }
                        args._body.type = 'query';
                        func = new Varavel.Project.App.Function(args.constants.queryPostload, ctx);
                        context$1$0.next = 16;
                        return regeneratorRuntime.awrap(func.invoke(info));
                    case 16:
                        info = context$1$0.sent;
                    case 17:
                        json.write(info);
                        context$1$0.next = 23;
                        break;
                    case 20:
                        context$1$0.prev = 20;
                        context$1$0.t0 = context$1$0['catch'](1);
                        json.write(context$1$0.t0);
                    case 23:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this, [[
                    1,
                    20
                ]]);
        })
    });
    Object.defineProperty(ApiController.prototype, 'q_edit', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0(args) {
            var json, ctx, func, info;
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        json = new VaravelNamespace.Http.Response.JsonResponse(args);
                        context$1$0.prev = 1;
                        ctx = Varavel.Project.App.QueryBase.context(args);
                        if (!(args.constants && args.constants.queryPreload)) {
                            context$1$0.next = 8;
                            break;
                        }
                        args._body.type = 'update';
                        func = new Varavel.Project.App.Function(args.constants.queryPreload, ctx);
                        context$1$0.next = 8;
                        return regeneratorRuntime.awrap(func.invoke(args._body));
                    case 8:
                        context$1$0.next = 10;
                        return regeneratorRuntime.awrap(Varavel.Project.App.QueryBase.edicion(args, args._body.options || { tablename: args._body.tablename }, ctx));
                    case 10:
                        info = context$1$0.sent;
                        if (!(args.constants && args.constants.queryPostload)) {
                            context$1$0.next = 17;
                            break;
                        }
                        args._body.type = 'update';
                        func = new Varavel.Project.App.Function(args.constants.queryPostload, ctx);
                        context$1$0.next = 16;
                        return regeneratorRuntime.awrap(func.invoke(info));
                    case 16:
                        info = context$1$0.sent;
                    case 17:
                        json.write(info);
                        context$1$0.next = 23;
                        break;
                    case 20:
                        context$1$0.prev = 20;
                        context$1$0.t0 = context$1$0['catch'](1);
                        json.write(context$1$0.t0);
                    case 23:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this, [[
                    1,
                    20
                ]]);
        })
    });
    Object.defineProperty(ApiController.prototype, 'q_insert', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0(args) {
            var json, ctx, func, info;
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        json = new VaravelNamespace.Http.Response.JsonResponse(args);
                        context$1$0.prev = 1;
                        ctx = Varavel.Project.App.QueryBase.context(args);
                        if (!(args.constants && args.constants.queryPreload)) {
                            context$1$0.next = 8;
                            break;
                        }
                        args._body.type = 'insert';
                        func = new Varavel.Project.App.Function(args.constants.queryPreload, ctx);
                        context$1$0.next = 8;
                        return regeneratorRuntime.awrap(func.invoke(args._body));
                    case 8:
                        context$1$0.next = 10;
                        return regeneratorRuntime.awrap(Varavel.Project.App.QueryBase.insert(args, args._body.options || { tablename: args._body.tablename }, ctx));
                    case 10:
                        info = context$1$0.sent;
                        if (!(args.constants && args.constants.queryPostload)) {
                            context$1$0.next = 17;
                            break;
                        }
                        args._body.type = 'insert';
                        func = new Varavel.Project.App.Function(args.constants.queryPostload, ctx);
                        context$1$0.next = 16;
                        return regeneratorRuntime.awrap(func.invoke(info));
                    case 16:
                        info = context$1$0.sent;
                    case 17:
                        json.write(info);
                        context$1$0.next = 23;
                        break;
                    case 20:
                        context$1$0.prev = 20;
                        context$1$0.t0 = context$1$0['catch'](1);
                        json.write(context$1$0.t0);
                    case 23:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this, [[
                    1,
                    20
                ]]);
        })
    });
    Object.defineProperty(ApiController.prototype, 'q_delete', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0(args) {
            var json, ctx, func, info;
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        json = new VaravelNamespace.Http.Response.JsonResponse(args);
                        context$1$0.prev = 1;
                        ctx = Varavel.Project.App.QueryBase.context(args);
                        if (!(args.constants && args.constants.queryPreload)) {
                            context$1$0.next = 8;
                            break;
                        }
                        args._body.type = 'delete';
                        func = new Varavel.Project.App.Function(args.constants.queryPreload, ctx);
                        context$1$0.next = 8;
                        return regeneratorRuntime.awrap(func.invoke(args._body));
                    case 8:
                        context$1$0.next = 10;
                        return regeneratorRuntime.awrap(Varavel.Project.App.QueryBase.borrar(args, args._body.options || { tablename: args._body.tablename }, ctx));
                    case 10:
                        info = context$1$0.sent;
                        if (!(args.constants && args.constants.queryPostload)) {
                            context$1$0.next = 17;
                            break;
                        }
                        args._body.type = 'delete';
                        func = new Varavel.Project.App.Function(args.constants.queryPostload, ctx);
                        context$1$0.next = 16;
                        return regeneratorRuntime.awrap(func.invoke(info));
                    case 16:
                        info = context$1$0.sent;
                    case 17:
                        json.write(info);
                        context$1$0.next = 23;
                        break;
                    case 20:
                        context$1$0.prev = 20;
                        context$1$0.t0 = context$1$0['catch'](1);
                        json.write(context$1$0.t0);
                    case 23:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this, [[
                    1,
                    20
                ]]);
        })
    });
    Object.defineProperty(ApiController.prototype, 'getFileContent', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0(args) {
            var json, file, site, task, result;
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        json = new VaravelNamespace.Http.Response.JsonResponse(args);
                        file = args.request.query.file;
                        site = new Varavel.Project.App.Site();
                        task = new core.VW.Task();
                        site.getFile({
                            fullpath: file,
                            error404: function () {
                                task.result = task.result || {};
                                task.result.noexists = true;
                                task.finish();
                            },
                            error: function (e) {
                                task.exception = e;
                                task.finish();
                            },
                            success: function (r) {
                                task.result = r;
                                task.finish();
                            }
                        });
                        context$1$0.prev = 5;
                        context$1$0.next = 8;
                        return regeneratorRuntime.awrap(task);
                    case 8:
                        result = context$1$0.sent;
                        json.write(result);
                        context$1$0.next = 15;
                        break;
                    case 12:
                        context$1$0.prev = 12;
                        context$1$0.t0 = context$1$0['catch'](5);
                        json.write(context$1$0.t0);
                    case 15:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this, [[
                    5,
                    12
                ]]);
        })
    });
    Object.defineProperty(ApiController.prototype, 'executeFunction', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0(args) {
            var json, func, time, result;
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        json = new VaravelNamespace.Http.Response.JsonResponse(args);
                        context$1$0.prev = 1;
                        time = Date.now();
                        if (!args.request.params.id) {
                            context$1$0.next = 7;
                            break;
                        }
                        throw new core.System.NotImplementedException();
                    case 7:
                        func = new Varavel.Project.App.Function(args.request.params.cid, Varavel.Project.App.QueryBase.context(args));
                    case 8:
                        context$1$0.next = 10;
                        return regeneratorRuntime.awrap(func.load(args._body));
                    case 10:
                        context$1$0.next = 12;
                        return regeneratorRuntime.awrap(func.execute(args._body));
                    case 12:
                        result = context$1$0.sent;
                        if (args._body && args._body.preventdefault)
                            args.response.end();
                        else
                            json.write(result);
                        func.context = func.func = null;
                        context$1$0.next = 26;
                        break;
                    case 17:
                        context$1$0.prev = 17;
                        context$1$0.t0 = context$1$0['catch'](1);
                        if (func)
                            func.context = func.func = null;
                        vw.error(context$1$0.t0);
                        if (!(args._body && args._body.stopdefault)) {
                            context$1$0.next = 23;
                            break;
                        }
                        return context$1$0.abrupt('return');
                    case 23:
                        if (!(args._body && args._body.preventdefault)) {
                            context$1$0.next = 25;
                            break;
                        }
                        return context$1$0.abrupt('return', args.response.end());
                    case 25:
                        json.write(context$1$0.t0);
                    case 26:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this, [[
                    1,
                    17
                ]]);
        })
    });
    Object.defineProperty(ApiController.prototype, 'defaultMethod', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0(args) {
            var json, func, er, result;
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        json = new VaravelNamespace.Http.Response.JsonResponse(args);
                        context$1$0.prev = 1;
                        if (!args.apidefault_noretry) {
                            context$1$0.next = 6;
                            break;
                        }
                        context$1$0.next = 5;
                        return regeneratorRuntime.awrap(args.continueAsync());
                    case 5:
                        return context$1$0.abrupt('return', context$1$0.sent);
                    case 6:
                        context$1$0.prev = 6;
                        func = new Varavel.Project.App.Function('api.default', Varavel.Project.App.QueryBase.context(args));
                        context$1$0.next = 10;
                        return regeneratorRuntime.awrap(func.load(args._body));
                    case 10:
                        context$1$0.next = 16;
                        break;
                    case 12:
                        context$1$0.prev = 12;
                        context$1$0.t0 = context$1$0['catch'](6);
                        er = context$1$0.t0;
                        args.apidefault_noretry = true;
                    case 16:
                        if (!er) {
                            context$1$0.next = 20;
                            break;
                        }
                        context$1$0.next = 19;
                        return regeneratorRuntime.awrap(args.continueAsync());
                    case 19:
                        return context$1$0.abrupt('return', context$1$0.sent);
                    case 20:
                        args.apidefault_noretry = true;
                        context$1$0.next = 23;
                        return regeneratorRuntime.awrap(func.execute(args._body));
                    case 23:
                        result = context$1$0.sent;
                        vw.info('RESULT:', args._body);
                        if (!(args._body && args._body.stopdefault)) {
                            context$1$0.next = 27;
                            break;
                        }
                        return context$1$0.abrupt('return');
                    case 27:
                        if (args._body && args._body.preventdefault)
                            args.response.end();
                        else if (args.request)
                            json.write(result);
                        func.context = func.func = null;
                        context$1$0.next = 37;
                        break;
                    case 31:
                        context$1$0.prev = 31;
                        context$1$0.t1 = context$1$0['catch'](1);
                        if (func)
                            func.context = func.func = null;
                        if (!(args._body && args._body.preventdefault)) {
                            context$1$0.next = 36;
                            break;
                        }
                        return context$1$0.abrupt('return', args.response.end());
                    case 36:
                        json.write(context$1$0.t1);
                    case 37:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this, [
                [
                    1,
                    31
                ],
                [
                    6,
                    12
                ]
            ]);
        })
    });
    Object.defineProperty(ApiController.prototype, 'unionBody', {
        enumerable: false,
        value: function (args) {
            var data = {};
            if (args.request.query.__json__mode == 1) {
                args.request.body = JSON.parse(args.request.query.__json__data);
            }
            if (args.request.query) {
                for (var id in args.request.query) {
                    data[id] = args.request.query[id];
                }
            }
            if (args.request.body) {
                for (var id in args.request.body) {
                    data[id] = args.request.body[id];
                }
            }
            return data;
        }
    });
    Object.defineProperty(ApiController.prototype, 'perfect', {
        enumerable: false,
        value: function (data) {
            var o, e;
            for (var id in data) {
                if (typeof data[id] == 'object') {
                    o = data[id];
                    if (o) {
                        if (o.$oid) {
                            data[id] = Mongo.ObjectID(o.$oid);
                        } else if (o.$bcrypt) {
                            e = Bcrypt.genSaltSync(10);
                            data[id] = Bcrypt.hashSync(o.$bcrypt, e);
                        } else {
                            this.perfect(o);
                        }
                    }
                }
            }
            return data;
        }
    });
    Object.defineProperty(ApiController.prototype, 'defaultInfo', {
        enumerable: false,
        value: function () {
            return null;
        }
    });
    Object.defineProperty(ApiController.prototype, 'allow', {
        enumerable: false,
        value: function (args) {
            if (Varavel.Funcs.env('DOMAIN_ID'))
                args.response.setHeader('cdn-id', Varavel.Funcs.env('DOMAIN_ID'));
            args.response.setHeader('access-control-allow-headers', 'Accept, Accept-Language, Authorization, Range, content-type, content-range');
            args.response.setHeader('access-control-allow-methods', 'GET,POST,PUT,DELETE,OPTIONS');
            args.response.setHeader('access-control-allow-credentials', true);
            args.response.setHeader('connection', 'keep-alive');
            args.response.setHeader('access-control-allow-origin', args.request.query['src-url'] || '*');
            args.srcUrl = args.request.query['src-url'];
            delete args.request.query['src-url'];
            return args.continue();
        }
    });
    Object.defineProperty(ApiController.prototype, 'allowAndLoadToken', {
        enumerable: false,
        value: function (args) {
            if (Varavel.Funcs.env('DOMAIN_ID'))
                args.response.setHeader('cdn-id', Varavel.Funcs.env('DOMAIN_ID'));
            args.response.setHeader('access-control-allow-headers', 'Accept, Accept-Language, Authorization, Range, content-type, content-range');
            args.response.setHeader('access-control-allow-methods', 'GET,POST,PUT,DELETE,OPTIONS');
            args.response.setHeader('access-control-allow-credentials', true);
            args.response.setHeader('connection', 'keep-alive');
            args.response.setHeader('access-control-allow-origin', args.request.query['src-url'] || '*');
            args.srcUrl = args.request.query['src-url'];
            delete args.request.query['src-url'];
            return args.continueAsync();
        }
    });
    Object.defineProperty(ApiController.prototype, 'maintenance', {
        enumerable: false,
        value: function (args) {
            var time = ApiController.revisionTime || 0, error;
            if (Date.now() - time > 5 * 60 * 1000) {
                ApiController.maintenance = false;
                if (Fs.sync.exists($mod$2.default.join(global.appDir, 'maintenance'))) {
                    ApiController.revisionTime = Date.now() - 3 * 60 * 1000;
                    ApiController.maintenance = error = true;
                } else {
                    ApiController.revisionTime = Date.now();
                }
            } else {
                error = ApiController.maintenance;
            }
            if (error) {
                args.response.write('The server it\'s in maintenance mode. We will come back soon');
                args.response.end();
                return;
            }
            return args.continueAsync();
        }
    });
    Object.defineProperty(ApiController.prototype, 'loadtoken', {
        enumerable: false,
        value: function (args) {
            return args.continue();
        }
    });
}
ApiController.version = require(global.appDir + '/package.json').version;
exports.default = ApiController;