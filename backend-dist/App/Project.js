var controllerGroup = Varavel.Project.Modules.Api.Http.Controllers;
{
    var Project = function Project() {
        Project.$constructor ? Project.$constructor.apply(this, arguments) : Project.$superClass && Project.$superClass.apply(this, arguments);
    };
    Object.defineProperty(Project, '$constructor', {
        enumerable: false,
        value: function (sitename, args) {
            this.sitename = sitename;
            this.args = args;
        }
    });
    Object.defineProperty(Project, 'initialize', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0(args) {
            var site, state, func, ctx, task;
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        args._obody = args._obody || {};
                        site = args.site;
                        if (!site) {
                            site = Varavel.Project.App.Site.getFromArgs(args);
                        }
                        context$1$0.next = 5;
                        return regeneratorRuntime.awrap(site.getRealSite());
                    case 5:
                        args.site = site;
                        context$1$0.next = 8;
                        return regeneratorRuntime.awrap(site.readState());
                    case 8:
                        state = context$1$0.sent;
                        args.prefix = state.prefix;
                        args.constants = state.constants;
                        if (!(Object.keys(args._obody).length == 0)) {
                            context$1$0.next = 20;
                            break;
                        }
                        if (!(!args.constants || args.constants.bodyparser !== false)) {
                            context$1$0.next = 20;
                            break;
                        }
                        if (!Project.bodyParser)
                            Project.bodyParser = core.VW.Http.Server.getBodyParser();
                        if (!(Project.bodyParser && args.request && args.request.headers)) {
                            context$1$0.next = 20;
                            break;
                        }
                        task = new core.VW.Task();
                        Project.bodyParser({
                            request: args.request,
                            response: args.response,
                            continue: function () {
                                task.finish();
                            }
                        });
                        context$1$0.next = 19;
                        return regeneratorRuntime.awrap(task);
                    case 19:
                        args._obody = Project.unionBody(args);
                    case 20:
                        if (!(args.constants && args.constants.bodyAnalyze && Object.keys(args._obody).length > 0)) {
                            context$1$0.next = 28;
                            break;
                        }
                        ctx = Varavel.Project.App.QueryBase.context(args);
                        func = new Varavel.Project.App.Function(args.constants.bodyAnalyze, ctx);
                        context$1$0.next = 25;
                        return regeneratorRuntime.awrap(func.invoke(args._obody));
                    case 25:
                        args._body = context$1$0.sent;
                        context$1$0.next = 29;
                        break;
                    case 28:
                        args._body = args._obody || args._body;
                    case 29:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this);
        })
    });
    Object.defineProperty(Project, 'unionBody', {
        enumerable: false,
        value: function (args) {
            var data = {};
            if (args.request) {
                if (args.request.query.__json__mode == 1) {
                    args.request.body = JSON.parse(args.request.query.__json__data);
                }
                if (args.request.query) {
                    for (var id in args.request.query) {
                        data[id] = args.request.query[id];
                    }
                }
                if (args.request.body) {
                    for (var id in args.request.body) {
                        data[id] = args.request.body[id];
                    }
                }
            }
            return data;
        }
    });
    Object.defineProperty(Project.prototype, 'context', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0(site, args) {
            var args1, ctx;
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        args = args || this.args || {};
                        site = Varavel.Project.App.Site.getFromId(site);
                        args1 = {
                            request: args.request || {},
                            response: args.response || {},
                            continue: args.continue || function () {
                            },
                            continueAsync: args.continueAsync || function () {
                            }
                        };
                        site.args = args1;
                        args1.site = site;
                        context$1$0.next = 7;
                        return regeneratorRuntime.awrap(Project.initialize(args1));
                    case 7:
                        ctx = Varavel.Project.App.QueryBase.context(args1);
                        return context$1$0.abrupt('return', ctx);
                    case 9:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this);
        })
    });
    Object.defineProperty(Project.prototype, 'methodAs', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0(site, args, funcname) {
            var ctx, func;
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        context$1$0.next = 2;
                        return regeneratorRuntime.awrap(this.context(site, args));
                    case 2:
                        ctx = context$1$0.sent;
                        func = new Varavel.Project.App.Function(funcname, ctx);
                        return context$1$0.abrupt('return', func);
                    case 5:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this);
        })
    });
    Object.defineProperty(Project.prototype, 'userFunction', {
        enumerable: false,
        value: function (e) {
            return this.methodAs(this.sitename, this.args, e);
        }
    });
    Object.defineProperty(Project.prototype, 'getContext', {
        enumerable: false,
        value: function () {
            return this.context(this.sitename, this.args);
        }
    });
}
exports.default = Project;