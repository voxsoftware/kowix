var $mod$17 = core.VW.Ecma2015.Utils.module(require('path'));
var Fs = core.System.IO.Fs;
var $mod$18 = core.VW.Ecma2015.Utils.module(require('fs'));
var tempdir = $mod$17.default.join(global.appDir, 'cache', 'tmp');
{
    var Site = function Site() {
        Site.$constructor ? Site.$constructor.apply(this, arguments) : Site.$superClass && Site.$superClass.apply(this, arguments);
    };
    Object.defineProperty(Site, '$constructor', {
        enumerable: false,
        value: function (args) {
            if (args) {
                args.request.params = args.request.params || {};
                if (args.request.params.site) {
                    this.site = args.request.params.site;
                } else {
                    this.site = args.request.headers.host;
                    this.site = this.site.split(':')[0];
                }
            }
        }
    });
    Site.__defineGetter__('appDir', function () {
        return process.env.BASE_DIR || global.baseDir || global.appDir;
    });
    Site.prototype.__defineGetter__('siteName', function () {
        return this.site;
    });
    Object.defineProperty(Site.prototype, 'getPublicPath', {
        enumerable: false,
        value: function () {
            var p = this.getSiteDir();
            if (!p)
                throw new core.System.Exception('Domain not found');
            return $mod$17.default.join(p, 'public');
        }
    });
    Object.defineProperty(Site.prototype, 'getFunctionPath', {
        enumerable: false,
        value: function () {
            var p = this.getSiteDir();
            if (!p)
                throw new core.System.Exception('Domain not found');
            return $mod$17.default.join(p, 'functions');
        }
    });
    Site.prototype.__defineGetter__('idSite', function () {
        return this.site;
    });
    Object.defineProperty(Site.prototype, 'getSiteDir', {
        enumerable: false,
        value: function (gcloudMethod) {
            Site.sitesc = Site.sitesc || {};
            var c = Site.sitesc[this.site], r, origSite = this.site;
            if (c && Date.now() - c.updated <= 10 * 60 * 1000) {
                if (c.realSite) {
                    this.site = c.realSite;
                }
                return c.file;
            }
            var siteDir, r;
            if (this.site == '127.0.0.1' && process.env.DEFAULT_DIR) {
                siteDir = process.env.DEFAULT_DIR;
            } else {
                siteDir = $mod$17.default.join(Site.appDir, '..', this.site);
                if (!Fs.sync.exists(siteDir)) {
                    siteDir = $mod$17.default.join(Site.appDir, '..', 'domains', this.site);
                    if (Fs.sync.exists(siteDir)) {
                        r = Fs.sync.readFile(siteDir, 'utf8');
                        r = r.trim();
                        this.site = r;
                        siteDir = $mod$17.default.join(Site.appDir, '..', 'remote-gs', 'proyectos', r);
                    } else {
                        siteDir = null;
                    }
                }
            }
            if (siteDir) {
                Site.sitesc[origSite] = {
                    realSite: r,
                    file: siteDir,
                    updated: Date.now()
                };
            }
            return siteDir;
        }
    });
    Object.defineProperty(Site.prototype, 'getFunctionPathMode2', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0() {
            var p;
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        context$1$0.next = 2;
                        return regeneratorRuntime.awrap(this.getSiteDirMode2());
                    case 2:
                        p = context$1$0.sent;
                        if (p) {
                            context$1$0.next = 5;
                            break;
                        }
                        throw new core.System.Exception('Domain not found');
                    case 5:
                        return context$1$0.abrupt('return', $mod$17.default.join(p, 'functions'));
                    case 6:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this);
        })
    });
    Object.defineProperty(Site.prototype, 'getSiteDirMode2', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0() {
            var c, r, site;
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        c = null;
                        if (!Site.sitesc_2) {
                            context$1$0.next = 9;
                            break;
                        }
                        c = Site.sitesc_2[this.site];
                        if (c) {
                            context$1$0.next = 7;
                            break;
                        }
                        context$1$0.next = 6;
                        return regeneratorRuntime.awrap(this.loadSite2Cache());
                    case 6:
                        c = Site.sitesc_2[this.site];
                    case 7:
                        context$1$0.next = 12;
                        break;
                    case 9:
                        context$1$0.next = 11;
                        return regeneratorRuntime.awrap(this.loadSite2Cache());
                    case 11:
                        c = Site.sitesc_2[this.site];
                    case 12:
                        if (!(c && Date.now() - c.updated <= 1 * 60 * 1000)) {
                            context$1$0.next = 16;
                            break;
                        }
                        return context$1$0.abrupt('return', c.file);
                    case 16:
                        if (!(c && c.file)) {
                            context$1$0.next = 19;
                            break;
                        }
                        this._getSiteDirMode2(true);
                        return context$1$0.abrupt('return', c.file);
                    case 19:
                        context$1$0.next = 21;
                        return regeneratorRuntime.awrap(this._getSiteDirMode2());
                    case 21:
                        site = context$1$0.sent;
                        return context$1$0.abrupt('return', site);
                    case 23:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this);
        })
    });
    Object.defineProperty(Site.prototype, '_getSiteDirMode2', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0(secure) {
            var siteDir, sto, buk, req, response, url, content, newsite, exists;
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        context$1$0.prev = 0;
                        if (!(this.site == '127.0.0.1' && process.env.DEFAULT_DIR)) {
                            context$1$0.next = 5;
                            break;
                        }
                        siteDir = process.env.DEFAULT_DIR;
                        context$1$0.next = 15;
                        break;
                    case 5:
                        siteDir = $mod$17.default.join(Site.appDir, '..', this.site);
                        exists = Fs.sync.exists(siteDir);
                        if (exists) {
                            context$1$0.next = 15;
                            break;
                        }
                        siteDir = $mod$17.default.join(Site.appDir, '..', 'domains', this.site);
                        exists = Fs.sync.exists(siteDir);
                        if (!exists) {
                            context$1$0.next = 15;
                            break;
                        }
                        context$1$0.next = 13;
                        return regeneratorRuntime.awrap(Fs.async.readFile(siteDir, 'utf8'));
                    case 13:
                        content = context$1$0.sent;
                        if (content) {
                            newsite = content;
                            siteDir = $mod$17.default.join(Site.appDir, '..', content);
                        } else {
                            siteDir = null;
                        }
                    case 15:
                        if (siteDir) {
                            Site.sitesc_2[this.site] = {
                                file: siteDir,
                                updated: Date.now()
                            };
                            if (newsite)
                                this.site = newsite;
                        }
                        context$1$0.next = 25;
                        break;
                    case 18:
                        context$1$0.prev = 18;
                        context$1$0.t0 = context$1$0['catch'](0);
                        if (secure) {
                            context$1$0.next = 24;
                            break;
                        }
                        throw context$1$0.t0;
                    case 24:
                        console.error('Error al obtener directorio del sitio', context$1$0.t0);
                    case 25:
                        return context$1$0.abrupt('return', siteDir);
                    case 26:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this, [[
                    0,
                    18
                ]]);
        })
    });
    Object.defineProperty(Site.prototype, 'loadSite2Cache', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0() {
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        Site.sitesc_2 = Site.sitesc_2 || {};
                        return context$1$0.abrupt('return');
                    case 2:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this);
        })
    });
    Object.defineProperty(Site.prototype, 'updateSiteCache', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0() {
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        return context$1$0.abrupt('return');
                    case 1:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this);
        })
    });
    Object.defineProperty(Site.prototype, 'readState', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0(options) {
            var cache, info;
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        Site.siteVars = Site.siteVars || {};
                        cache = Site.siteVars[this.site];
                        if (!(cache && Date.now() - cache.updated <= 120000)) {
                            context$1$0.next = 4;
                            break;
                        }
                        return context$1$0.abrupt('return', cache.code);
                    case 4:
                        context$1$0.next = 6;
                        return regeneratorRuntime.awrap(this.getFunction('vars', '.json', options));
                    case 6:
                        info = context$1$0.sent;
                        if (info) {
                            context$1$0.next = 9;
                            break;
                        }
                        throw new core.System.Exception('Site ' + this.site + ' it\'s not enabled');
                    case 9:
                        if (!info.compiledCode) {
                            info.compiledCode = JSON.parse(info.code);
                        }
                        cache = cache || {};
                        Site.siteVars[this.site] = cache;
                        cache.updated = Date.now();
                        return context$1$0.abrupt('return', cache.code = info.compiledCode);
                    case 14:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this);
        })
    });
    Object.defineProperty(Site, 'getFromArgs', {
        enumerable: false,
        value: function (args) {
            if (args.site) {
                return args.site;
            }
            args.site = new Site(args);
            return args.site;
        }
    });
    Object.defineProperty(Site, 'getFromId', {
        enumerable: false,
        value: function (site) {
            var s = new Site();
            s.site = site;
            return s;
        }
    });
    Object.defineProperty(Site.prototype, 'getRules', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0(rule) {
            var rules, ruleAll, rs, rules2, ar, i;
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        context$1$0.next = 2;
                        return regeneratorRuntime.awrap(this.getFunction('rules.' + rule, '.json'));
                    case 2:
                        rules = context$1$0.sent;
                        vw.warning('RULE HERE:', rule, rules);
                        if (rules && !rules.compiledCode) {
                            rules.compiledCode = JSON.parse(rules.code);
                        }
                        rs = rule.split('.');
                        rs.splice(rs.length > 3 ? 1 : 0, 1);
                        ruleAll = rs.join('.');
                        context$1$0.next = 10;
                        return regeneratorRuntime.awrap(this.getFunction(ruleAll, '.json'));
                    case 10:
                        rules2 = context$1$0.sent;
                        if (rules2 && !rules2.compiledCode) {
                            rules2.compiledCode = JSON.parse(rules2.code);
                        }
                        ar = [];
                        if (rules && rules.compiledCode) {
                            for (i = 0; i < rules.compiledCode.length; i++) {
                                ar.push(rules.compiledCode[i]);
                            }
                        }
                        if (rules2 && rules2.compiledCode) {
                            for (i = 0; i < rules2.compiledCode.length; i++) {
                                ar.push(rules2.compiledCode[i]);
                            }
                        }
                        return context$1$0.abrupt('return', ar);
                    case 16:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this);
        })
    });
    Object.defineProperty(Site.prototype, 'getFile', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0($_ref0) {
            var path, fullpath, error404, error, success, headers, site, pub, url, req, response, cache, exists, uname;
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        path = $_ref0 ? $_ref0.path : undefined, fullpath = $_ref0 ? $_ref0.fullpath : undefined, error404 = $_ref0 ? $_ref0.error404 : undefined, error = $_ref0 ? $_ref0.error : undefined, success = $_ref0 ? $_ref0.success : undefined, headers = $_ref0 ? $_ref0.headers : undefined;
                        site = this;
                        if (fullpath) {
                            context$1$0.next = 11;
                            break;
                        }
                        context$1$0.t0 = $mod$17.default;
                        context$1$0.next = 6;
                        return regeneratorRuntime.awrap(site.getSiteDirMode2());
                    case 6:
                        context$1$0.t1 = context$1$0.sent;
                        pub = context$1$0.t0.join.call(context$1$0.t0, context$1$0.t1, 'public');
                        pub = $mod$17.default.join(pub, path);
                        context$1$0.next = 12;
                        break;
                    case 11:
                        pub = fullpath;
                    case 12:
                        if (!Varavel.Funcs.env('FILE_CACHE_SERVER')) {
                            context$1$0.next = 27;
                            break;
                        }
                        url = Varavel.Funcs.env('FILE_CACHE_SERVER');
                        url += '/api/staticinfo?file=' + encodeURIComponent(pub);
                        req = new core.VW.Http.Request(url);
                        context$1$0.next = 18;
                        return regeneratorRuntime.awrap(req.getResponseAsync());
                    case 18:
                        response = context$1$0.sent;
                        if (typeof response.body == 'string') {
                            response.body = JSON.parse(response.body);
                        }
                        if (!response.body.error) {
                            context$1$0.next = 22;
                            break;
                        }
                        return context$1$0.abrupt('return', error(response.body.error));
                    case 22:
                        if (!response.body.noexists) {
                            context$1$0.next = 24;
                            break;
                        }
                        return context$1$0.abrupt('return', error404());
                    case 24:
                        if (!response.body) {
                            context$1$0.next = 26;
                            break;
                        }
                        return context$1$0.abrupt('return', success(response.body));
                    case 26:
                        return context$1$0.abrupt('return');
                    case 27:
                        cache = Site.fileCache[pub];
                        uname = pub.replace(/\//ig, '$');
                        context$1$0.prev = 29;
                        if (!(cache && cache.exists === false)) {
                            context$1$0.next = 47;
                            break;
                        }
                        if (!(cache && Date.now() - cache.updated <= parseInt(Varavel.Funcs.env('CACHE_STATIC_TIME', 1.5 * 60000)))) {
                            context$1$0.next = 37;
                            break;
                        }
                        if (!error404) {
                            context$1$0.next = 36;
                            break;
                        }
                        context$1$0.next = 35;
                        return regeneratorRuntime.awrap(error404());
                    case 35:
                        return context$1$0.abrupt('return', context$1$0.sent);
                    case 36:
                        throw new core.System.IO.FileNotFoundException('File not found');
                    case 37:
                        try {
                            exists = Fs.sync.exists(pub);
                        } catch (e) {
                        }
                        if (exists) {
                            context$1$0.next = 45;
                            break;
                        }
                        Site.fileCache[pub] = {
                            file: pub,
                            exists: false,
                            updated: Date.now()
                        };
                        if (!error404) {
                            context$1$0.next = 44;
                            break;
                        }
                        context$1$0.next = 43;
                        return regeneratorRuntime.awrap(error404());
                    case 43:
                        return context$1$0.abrupt('return', context$1$0.sent);
                    case 44:
                        throw new core.System.IO.FileNotFoundException('File not found');
                    case 45:
                        context$1$0.next = 56;
                        break;
                    case 47:
                        if (cache) {
                            context$1$0.next = 56;
                            break;
                        }
                        exists = Fs.sync.exists(pub);
                        if (exists) {
                            context$1$0.next = 56;
                            break;
                        }
                        Site.fileCache[pub] = {
                            file: pub,
                            exists: false,
                            updated: Date.now()
                        };
                        if (!error404) {
                            context$1$0.next = 55;
                            break;
                        }
                        context$1$0.next = 54;
                        return regeneratorRuntime.awrap(error404());
                    case 54:
                        return context$1$0.abrupt('return', context$1$0.sent);
                    case 55:
                        throw new core.System.IO.FileNotFoundException('File not found');
                    case 56:
                        success({
                            tmpfile: $mod$17.default.join(tempdir, uname),
                            file: pub,
                            name: $mod$17.default.basename(pub)
                        });
                        context$1$0.next = 62;
                        break;
                    case 59:
                        context$1$0.prev = 59;
                        context$1$0.t2 = context$1$0['catch'](29);
                        error(context$1$0.t2);
                    case 62:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this, [[
                    29,
                    59
                ]]);
        })
    });
    Object.defineProperty(Site.prototype, 'getFunction', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0(cid, extension, options) {
            var time, path, func, content, self, cache, stat, cachetime, task, result, upg;
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        if (extension === undefined) {
                            extension = '.js';
                        }
                        time = Date.now();
                        context$1$0.next = 4;
                        return regeneratorRuntime.awrap(this.getFunctionPathMode2());
                    case 4:
                        path = context$1$0.sent;
                        func = $mod$17.default.join(path, cid + extension);
                        self = this;
                        Site.cache = Site.cache || {};
                        cachetime = Varavel.Funcs.env('CACHE_API_TIME', 300000);
                        cache = Site.cache[this.site + '_' + cid];
                        task = new core.VW.Task();
                        if (!(!cache || Date.now() - cache.updated > cachetime || options && options.development_mode == 1)) {
                            context$1$0.next = 15;
                            break;
                        }
                        upg = (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0() {
                            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                                while (1)
                                    switch (context$1$0.prev = context$1$0.next) {
                                    case 0:
                                        context$1$0.next = 2;
                                        return regeneratorRuntime.awrap(self.getFile({
                                            fullpath: func,
                                            error404: function () {
                                                task.result = task.result || {};
                                                task.result.noexists = true;
                                                task.finish();
                                            },
                                            error: function (e) {
                                                task.exception = e;
                                                task.finish();
                                            },
                                            success: function (r) {
                                                task.result = r;
                                                task.finish();
                                            }
                                        }));
                                    case 2:
                                        context$1$0.next = 4;
                                        return regeneratorRuntime.awrap(task);
                                    case 4:
                                        result = context$1$0.sent;
                                        result = result || {};
                                        if (!result.noexists) {
                                            context$1$0.next = 14;
                                            break;
                                        }
                                        task = new core.VW.Task();
                                        func = $mod$17.default.join(path, cid + (extension != '.js' ? '.js' : '.es6'));
                                        self.getFile({
                                            fullpath: func,
                                            error404: function () {
                                                task.result = task.result || {};
                                                task.result.noexists = true;
                                                task.finish();
                                            },
                                            error: function (e) {
                                                task.exception = e;
                                                task.finish();
                                            },
                                            success: function (r) {
                                                task.result = r;
                                                task.finish();
                                            }
                                        });
                                        context$1$0.next = 12;
                                        return regeneratorRuntime.awrap(task);
                                    case 12:
                                        result = context$1$0.sent;
                                        result = result || {};
                                    case 14:
                                        if (result.noexists) {
                                            context$1$0.next = 28;
                                            break;
                                        }
                                        context$1$0.next = 17;
                                        return regeneratorRuntime.awrap(Fs.async.stat(result.file));
                                    case 17:
                                        stat = context$1$0.sent;
                                        if (!(cache && cache.updated)) {
                                            context$1$0.next = 22;
                                            break;
                                        }
                                        if (!(stat.mtime.getTime() <= cache.mtime)) {
                                            context$1$0.next = 22;
                                            break;
                                        }
                                        cache.updated = Date.now();
                                        return context$1$0.abrupt('return', cache);
                                    case 22:
                                        context$1$0.next = 24;
                                        return regeneratorRuntime.awrap(Fs.async.readFile(result.file, 'utf8'));
                                    case 24:
                                        content = context$1$0.sent;
                                        if (cache) {
                                        }
                                        Site.cache[self.site + '_' + cid] = null;
                                        cache = Site.cache[self.site + '_' + cid] = {
                                            file: result.file,
                                            codefile: (result.tmpfile || result.file) + '$$-code',
                                            mtime: stat.mtime.getTime(),
                                            remotefile: func,
                                            code: content,
                                            ext: $mod$17.default.extname(func),
                                            updated: Date.now()
                                        };
                                    case 28:
                                    case 'end':
                                        return context$1$0.stop();
                                    }
                            }, null, this);
                        });
                        context$1$0.next = 15;
                        return regeneratorRuntime.awrap(upg());
                    case 15:
                        return context$1$0.abrupt('return', cache);
                    case 16:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this);
        })
    });
}
Site.fileCache = {};
exports.default = Site;