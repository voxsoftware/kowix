var he = core.VW.E6Html.HtmlEncode;
var Fs = core.System.IO.Fs;
{
    var E6HtmlFile = function E6HtmlFile() {
        E6HtmlFile.$constructor ? E6HtmlFile.$constructor.apply(this, arguments) : E6HtmlFile.$superClass && E6HtmlFile.$superClass.apply(this, arguments);
    };
    Object.defineProperty(E6HtmlFile, '$constructor', {
        enumerable: false,
        value: function (file) {
            this.file = file;
            E6HtmlFile.cache[file] = this;
        }
    });
    Object.defineProperty(E6HtmlFile, 'get', {
        enumerable: false,
        value: function (file) {
            if (!E6HtmlFile.cache[file]) {
                new E6HtmlFile(file);
            }
            return E6HtmlFile.cache[file];
        }
    });
    Object.defineProperty(E6HtmlFile.prototype, 'base64decode', {
        enumerable: false,
        value: function (text, context) {
            if (context && context.transform) {
                text = context.transform(text);
            }
            return new Buffer(text, 'base64');
        }
    });
    Object.defineProperty(E6HtmlFile.prototype, 'encode', {
        enumerable: false,
        value: function (text, context) {
            if (context && context.transform) {
                text = context.transform(text);
            }
            return he.encode(text);
        }
    });
    Object.defineProperty(E6HtmlFile.prototype, 'invoke', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0(context) {
            var func;
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        if (this.compiledTime) {
                            context$1$0.next = 5;
                            break;
                        }
                        context$1$0.next = 3;
                        return regeneratorRuntime.awrap(this.compile(null, true));
                    case 3:
                        context$1$0.next = 8;
                        break;
                    case 5:
                        if (!(this.compiledTime && Date.now() - this.compiledTime > Varavel.Funcs.env('CACHE_STATIC_TIME', 60000))) {
                            context$1$0.next = 8;
                            break;
                        }
                        context$1$0.next = 8;
                        return regeneratorRuntime.awrap(this.compile(true));
                    case 8:
                        if (!context) {
                            context = core.VW.E6Html.E6Html.createConsoleContext();
                        }
                        context.arguments = context.arguments || {};
                        context.e6html = this;
                        func = this.$func(context);
                        context$1$0.next = 14;
                        return regeneratorRuntime.awrap(func());
                    case 14:
                        return context$1$0.abrupt('return', context$1$0.sent);
                    case 15:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this);
        })
    });
    Object.defineProperty(E6HtmlFile.prototype, 'compile_2', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0(secure, newmode) {
            var sto, url, req, response, code, parser, conversion, stat, utime;
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        context$1$0.prev = 0;
                        sto = new Varavel.Project.Modules.Api.Libraries.Storage();
                        vw.warning('FILE', this.file);
                        context$1$0.next = 5;
                        return regeneratorRuntime.awrap(Fs.async.stat(this.file));
                    case 5:
                        stat = context$1$0.sent;
                        stat.updated = stat.mtime.getTime();
                        if (!this.$func) {
                            context$1$0.next = 10;
                            break;
                        }
                        if (!(this.lastMetatime && this.lastMetatime >= stat.updated)) {
                            context$1$0.next = 10;
                            break;
                        }
                        return context$1$0.abrupt('return');
                    case 10:
                        if (!(newmode && this.compiledTime)) {
                            context$1$0.next = 13;
                            break;
                        }
                        this.compiledTime = Date.now();
                        return context$1$0.abrupt('return');
                    case 13:
                        utime = this.lastMetatime;
                        this.lastMetatime = meta.updated;
                        context$1$0.next = 17;
                        return regeneratorRuntime.awrap(Fs.async.readFile(this.file, 'utf8'));
                    case 17:
                        code = context$1$0.sent;
                        this.compiledTime = Date.now();
                        parser = new core.VW.E6Html.Parser();
                        conversion = parser.parse(code);
                        this.conversion = conversion;
                        this.$func = eval(conversion.code);
                        conversion = null;
                        code = null;
                        context$1$0.next = 35;
                        break;
                    case 27:
                        context$1$0.prev = 27;
                        context$1$0.t0 = context$1$0['catch'](0);
                        if (utime !== null)
                            this.lastMetatime = utime;
                        if (secure) {
                            context$1$0.next = 34;
                            break;
                        }
                        throw context$1$0.t0;
                    case 34:
                        vw.error('Error en archivo: ', context$1$0.t0);
                    case 35:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this, [[
                    0,
                    27
                ]]);
        })
    });
    Object.defineProperty(E6HtmlFile.prototype, 'compile', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0(secure, newmode) {
            var parser, code, conversion, utime, metaUpdated, stat;
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        context$1$0.prev = 0;
                        if (!this.compiling) {
                            context$1$0.next = 3;
                            break;
                        }
                        return context$1$0.abrupt('return');
                    case 3:
                        this.compiling = true;
                        utime = null;
                        context$1$0.next = 7;
                        return regeneratorRuntime.awrap(Fs.async.stat(this.file));
                    case 7:
                        stat = context$1$0.sent;
                        metaUpdated = stat.mtime.getTime();
                        if (!this.$func) {
                            context$1$0.next = 12;
                            break;
                        }
                        if (!(this.lastMetatime && this.lastMetatime >= metaUpdated)) {
                            context$1$0.next = 12;
                            break;
                        }
                        return context$1$0.abrupt('return');
                    case 12:
                        if (!(newmode && this.compiledTime)) {
                            context$1$0.next = 15;
                            break;
                        }
                        this.compiledTime = Date.now();
                        return context$1$0.abrupt('return');
                    case 15:
                        utime = this.lastMetatime;
                        this.lastMetatime = metaUpdated;
                        if (!(newmode && this.compiledTime)) {
                            context$1$0.next = 19;
                            break;
                        }
                        return context$1$0.abrupt('return');
                    case 19:
                        context$1$0.next = 21;
                        return regeneratorRuntime.awrap(Fs.async.readFile(this.file, 'utf8'));
                    case 21:
                        code = context$1$0.sent;
                        this.compiledTime = Date.now();
                        parser = new core.VW.E6Html.Parser();
                        conversion = parser.parse(code);
                        this.conversion = conversion;
                        this.$func = eval(conversion.code);
                        conversion = null;
                        code = null;
                        context$1$0.next = 39;
                        break;
                    case 31:
                        context$1$0.prev = 31;
                        context$1$0.t0 = context$1$0['catch'](0);
                        if (utime !== null)
                            this.lastMetatime = utime;
                        if (secure) {
                            context$1$0.next = 38;
                            break;
                        }
                        throw context$1$0.t0;
                    case 38:
                        vw.error('Error en archivo: ', context$1$0.t0);
                    case 39:
                        context$1$0.prev = 39;
                        this.compiling = false;
                        return context$1$0.finish(39);
                    case 42:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this, [[
                    0,
                    31,
                    39,
                    42
                ]]);
        })
    });
}
E6HtmlFile.cache = {};
exports.default = E6HtmlFile;