var VaravelNamespace = core.org.voxsoftware.Varavel;
var $mod$14 = core.VW.Ecma2015.Utils.module(require('url'));
var Fs = core.System.IO.Fs;
var $mod$15 = core.VW.Ecma2015.Utils.module(require('fs'));
var $mod$16 = core.VW.Ecma2015.Utils.module(require('path'));
var times = 0;
{
    var RuleLimit = function RuleLimit() {
        RuleLimit.$constructor ? RuleLimit.$constructor.apply(this, arguments) : RuleLimit.$superClass && RuleLimit.$superClass.apply(this, arguments);
    };
    Object.defineProperty(RuleLimit, '$constructor', {
        enumerable: false,
        value: function (row, context) {
            this.row = row;
            this.context = context;
        }
    });
    Object.defineProperty(RuleLimit.prototype, 'checkUpdate', {
        enumerable: false,
        value: function (options, query, update) {
            var row = this.row;
            if (row.maxLength !== undefined && options.data.length > row.maxLength) {
                throw Varavel.Project.App.Exception.create('Allowed length of updated rows exceeded').putCode('INVALID_PARAMETERS').end();
            }
            return this.checkData(options, update);
        }
    });
    RuleLimit.prototype.__defineGetter__('hardDelete', function () {
        return !!this.row.hardDelete;
    });
    Object.defineProperty(RuleLimit.prototype, 'transformData', {
        enumerable: false,
        value: function (data) {
            if (this.row.projection) {
                var q = new global.Mingo.Query({}, this.row.projection);
                data = q.find(data).all();
            }
            return data;
        }
    });
    Object.defineProperty(RuleLimit.prototype, 'checkData', {
        enumerable: false,
        value: function (options, update) {
            if (this.row.projection) {
                var copia = {};
                for (var id in update) {
                    if (id.startsWith('$'))
                        copia[id] = update[id];
                }
                var q = new global.Mingo.Query({}, this.row.projection);
                update = q.find([update]).first();
                if (Object.keys(copia).length > 0) {
                    for (var id in copia) {
                        update[id] = q.find([copia[id]]).first();
                    }
                }
                copia = null;
            }
            return update;
        }
    });
    Object.defineProperty(RuleLimit.prototype, 'controlData', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0() {
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this);
        })
    });
    Object.defineProperty(RuleLimit.prototype, 'checkLimit', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0(limit) {
            var row;
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        row = this;
                        if (row.maxLimit !== undefined && limit > row.maxLimit) {
                            limit = row.maxLimit;
                        } else if (row.minLimit !== undefined && limit < row.minLimit) {
                            limit = row.minLimit;
                        }
                        if (!row.limitFunc) {
                            context$1$0.next = 7;
                            break;
                        }
                        this.limitFunc = new Varavel.Project.App.Function(row.limitFunc, this.context);
                        context$1$0.next = 6;
                        return regeneratorRuntime.awrap(this.limitFunc.load());
                    case 6:
                        limit = this.limitFunc.execute.bind(this)(limit);
                    case 7:
                        return context$1$0.abrupt('return', limit);
                    case 8:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this);
        })
    });
    Object.defineProperty(RuleLimit, 'createEnableFunc', {
        enumerable: false,
        value: function (e) {
            return function (body) {
                var neg, newbody;
                body = body || {};
                if (body) {
                    for (var id in body) {
                        if (body.id == 0)
                            neg = true;
                    }
                }
                if (neg) {
                    newbody = {};
                    for (var i = 0; i < e.length; i++) {
                        newbody[e[i]] = 1;
                    }
                    for (var id in body) {
                        if (body[id] !== undefined && e.indexOf(id) >= 0)
                            delete newbody[id];
                    }
                } else {
                    newbody = {};
                    for (var id in body) {
                        if (body[id] !== undefined && e.indexOf(id) >= 0) {
                            newbody[id] = body[id];
                        }
                    }
                    if (Object.keys(newbody).length == 0) {
                        for (var i = 0; i < e.length; i++) {
                            newbody[e[i]] = 1;
                        }
                    }
                }
                return newbody;
            };
        }
    });
    Object.defineProperty(RuleLimit.prototype, 'checkProjection', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0(project, mode) {
            var row, vacio, pj, field, $_iterator2, $_normal2, $_err2, $_step2;
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        if (!(mode == 'aggregation')) {
                            context$1$0.next = 4;
                            break;
                        }
                        context$1$0.next = 3;
                        return regeneratorRuntime.awrap(this.checkAggregateProjection(project));
                    case 3:
                        return context$1$0.abrupt('return', context$1$0.sent);
                    case 4:
                        row = this.row;
                        if (!row.disabledfields) {
                            context$1$0.next = 29;
                            break;
                        }
                        vacio = project ? Object.keys(project).length == 0 : true;
                        if (!project)
                            project = {};
                        typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined;
                        $_iterator2 = regeneratorRuntime.values(row.disabledfields), $_normal2 = false;
                        context$1$0.prev = 10;
                    case 11:
                        if (!true) {
                            context$1$0.next = 20;
                            break;
                        }
                        $_step2 = $_iterator2.next();
                        if (!$_step2.done) {
                            context$1$0.next = 16;
                            break;
                        }
                        $_normal2 = true;
                        return context$1$0.abrupt('break', 20);
                    case 16:
                        field = $_step2.value;
                        if (vacio)
                            project[field] = 0;
                        else
                            delete project[field];
                        context$1$0.next = 11;
                        break;
                    case 20:
                        context$1$0.next = 26;
                        break;
                    case 22:
                        context$1$0.prev = 22;
                        context$1$0.t0 = context$1$0['catch'](10);
                        $_normal2 = false;
                        $_err2 = context$1$0.t0;
                    case 26:
                        try {
                            if (!$_normal2 && $_iterator2['return']) {
                                $_iterator2['return']();
                            }
                        } catch (e) {
                        }
                        if (!$_err2) {
                            context$1$0.next = 29;
                            break;
                        }
                        throw $_err2;
                    case 29:
                        if (row.enabledfields) {
                            pj = RuleLimit.createEnableFunc(row.enabledfields);
                            project = pj(project);
                        }
                        if (!row.projectionFunc) {
                            context$1$0.next = 35;
                            break;
                        }
                        this.projectionFunc = new Varavel.Project.App.Function(row.projectionFunc, this.context);
                        context$1$0.next = 34;
                        return regeneratorRuntime.awrap(this.projectionFunc.load());
                    case 34:
                        project = this.projectionFunc.execute.bind(this)(project);
                    case 35:
                        return context$1$0.abrupt('return', project);
                    case 36:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this, [[
                    10,
                    22
                ]]);
        })
    });
    Object.defineProperty(RuleLimit.prototype, 'checkAggregateProjection', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0(project) {
            var row, vacio, pj, g;
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        row = this.row;
                        if (row.disabledfields) {
                            g = function (val) {
                                if (typeof val == 'object') {
                                    if (val instanceof Array) {
                                        for (var i = 0; i < val.length; i++) {
                                            if (typeof val[i] == 'object') {
                                                g(val[i]);
                                            } else {
                                                if (val[i].startsWith && val[i].startsWith('$')) {
                                                    if (row.disabledfields.indexOf(val[i].substring(1)) >= 0)
                                                        throw Varavel.Project.App.Exception.create('You cannot project field: ' + val[i]).putCode('ACCESS_DENIED').end();
                                                }
                                            }
                                        }
                                    } else {
                                        for (var id in val) {
                                            if (typeof val[id] == 'object') {
                                                g(val[id]);
                                            } else {
                                                if (val[id].startsWith && val[id].startsWith('$')) {
                                                    if (row.disabledfields.indexOf(val[id].substring(1)) >= 0)
                                                        throw Varavel.Project.App.Exception.create('You cannot project field: ' + val[id]).putCode('ACCESS_DENIED').end();
                                                }
                                            }
                                        }
                                    }
                                }
                            };
                        } else if (row.enabledfields) {
                            g = function (val) {
                                if (typeof val == 'object') {
                                    if (val instanceof Array) {
                                        for (var i = 0; i < val.length; i++) {
                                            if (typeof val[i] == 'object') {
                                                g(val[i]);
                                            } else {
                                                if (val[i].startsWith && val[i].startsWith('$')) {
                                                    if (row.enabledfields.indexOf(val[i].substring(1)) < 0)
                                                        throw Varavel.Project.App.Exception.create('You cannot project field: ' + val[i]).putCode('ACCESS_DENIED').end();
                                                }
                                            }
                                        }
                                    } else {
                                        for (var id in val) {
                                            if (typeof val[id] == 'object') {
                                                g(val[id]);
                                            } else {
                                                if (val[id].startsWith && val[id].startsWith('$')) {
                                                    if (row.enabledfields.indexOf(val[id].substring(1)) < 0)
                                                        throw Varavel.Project.App.Exception.create('You cannot project field: ' + val[id]).putCode('ACCESS_DENIED').end();
                                                }
                                            }
                                        }
                                    }
                                }
                            };
                        }
                        if (g)
                            g(project);
                        if (!row.projectionFunc) {
                            context$1$0.next = 8;
                            break;
                        }
                        this.projectionFunc = new Varavel.Project.App.Function(row.projectionFunc, this.context);
                        context$1$0.next = 7;
                        return regeneratorRuntime.awrap(this.projectionFunc.load());
                    case 7:
                        project = this.projectionFunc.execute.bind(this)(project, 'aggregation');
                    case 8:
                        return context$1$0.abrupt('return', project);
                    case 9:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this);
        })
    });
    Object.defineProperty(RuleLimit.prototype, 'checkSort', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0(sort) {
            var row;
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        row = this;
                        if (!row.sortFunc) {
                            context$1$0.next = 6;
                            break;
                        }
                        this.sortFunc = new Varavel.Project.App.Function(row.sortFunc, this.context);
                        context$1$0.next = 5;
                        return regeneratorRuntime.awrap(this.sortFunc.load());
                    case 5:
                        sort = this.sortFunc.execute.bind(this)(sort);
                    case 6:
                        return context$1$0.abrupt('return', sort);
                    case 7:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this);
        })
    });
}
exports.default = RuleLimit;