var $mod$1 = core.VW.Ecma2015.Utils.module(require('vm'));
var runInContext = function (script, options) {
    return script.runInThisContext(options);
};
var runCodeInContext = function (code, options) {
    return eval(code);
    return $mod$1.default.runInThisContext(code, options);
};
var VaravelNamespace = core.org.voxsoftware.Varavel;
var $mod$2 = core.VW.Ecma2015.Utils.module(require('url'));
var Fs = core.System.IO.Fs;
var $mod$3 = core.VW.Ecma2015.Utils.module(require('fs'));
var $mod$4 = core.VW.Ecma2015.Utils.module(require('path'));
var times = 0;
var $mod$5 = core.VW.Ecma2015.Utils.module(require('module'));
{
    var Function = function Function() {
        Function.$constructor ? Function.$constructor.apply(this, arguments) : Function.$superClass && Function.$superClass.apply(this, arguments);
    };
    Object.defineProperty(Function, '$constructor', {
        enumerable: false,
        value: function (idFunction, context, token) {
            this.idFunction = idFunction;
            this.context = context;
            this.token = null;
            this.idSite = context.IdSite;
        }
    });
    Object.defineProperty(Function.prototype, 'load', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0(options) {
            var func, compiledFunc, f1, rid, site;
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        this.idFunction = this.idFunction.replace(/\@/g, '/');
                        rid = this.idSite + '.' + this.idFunction;
                        if (!(typeof this.idFunction == 'string')) {
                            context$1$0.next = 17;
                            break;
                        }
                        if (!this.context.context) {
                            context$1$0.next = 11;
                            break;
                        }
                        site = this.context.context.site;
                        if (site) {
                            context$1$0.next = 9;
                            break;
                        }
                        site = Varavel.Project.App.Site.getFromArgs(this.context.context);
                        context$1$0.next = 9;
                        return regeneratorRuntime.awrap(site.getRealSite());
                    case 9:
                        context$1$0.next = 14;
                        break;
                    case 11:
                        site = Varavel.Project.App.Site.getFromId(this.idSite);
                        context$1$0.next = 14;
                        return regeneratorRuntime.awrap(site.getRealSite());
                    case 14:
                        context$1$0.next = 16;
                        return regeneratorRuntime.awrap(site.getFunction(this.idFunction, undefined, options));
                    case 16:
                        func = context$1$0.sent;
                    case 17:
                        if (func) {
                            context$1$0.next = 19;
                            break;
                        }
                        throw Varavel.Project.App.Exception.create('The function ' + this.idFunction + ' doesn\'t exists').putCode('FUNCTION_NOT_EXISTS').end();
                    case 19:
                        this.loading = Date.now();
                        context$1$0.prev = 20;
                        if (!(f1 = func.compiled0)) {
                            delete require.cache[require.resolve(func.file)];
                            f1 = require(func.file);
                            func.compiled0 = f1;
                        }
                        compiledFunc = func.compiled0(this.context);
                        context$1$0.next = 28;
                        break;
                    case 25:
                        context$1$0.prev = 25;
                        context$1$0.t0 = context$1$0['catch'](20);
                        throw context$1$0.t0;
                    case 28:
                        func.compiledFunc = compiledFunc;
                        this.func = func;
                    case 30:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this, [[
                    20,
                    25
                ]]);
        })
    });
    Object.defineProperty(Function, 'availablePaths', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0() {
            var tname, adir, stat, ufile, projects, dirs, i;
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        if (!Function.avpath) {
                            context$1$0.next = 2;
                            break;
                        }
                        return context$1$0.abrupt('return', Function.avpath);
                    case 2:
                        tname = $mod$4.default.basename(global.appDir);
                        adir = [];
                        projects = $mod$4.default.normalize($mod$4.default.join(global.appDir, '..'));
                        context$1$0.next = 7;
                        return regeneratorRuntime.awrap(Fs.async.readdir(projects));
                    case 7:
                        dirs = context$1$0.sent;
                        i = 0;
                    case 9:
                        if (!(i < dirs.length)) {
                            context$1$0.next = 19;
                            break;
                        }
                        ufile = $mod$4.default.join(projects, dirs[i]);
                        if (!(dirs[i] != tname)) {
                            context$1$0.next = 16;
                            break;
                        }
                        context$1$0.next = 14;
                        return regeneratorRuntime.awrap(Fs.async.stat(ufile));
                    case 14:
                        stat = context$1$0.sent;
                        if (stat.isDirectory())
                            adir.push(ufile);
                    case 16:
                        i++;
                        context$1$0.next = 9;
                        break;
                    case 19:
                        adir.push('/usr');
                        adir.push('/bin');
                        adir.push('/var/log');
                        return context$1$0.abrupt('return', Function.avpath = adir);
                    case 23:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this);
        })
    });
    Object.defineProperty(Function.prototype, 'execute', {
        enumerable: false,
        value: function () {
            try {
                return this.func.compiledFunc.apply(this.compiledFunc, arguments);
            } catch (e) {
                throw new core.System.Exception('Fail executing: ' + this.idFunction + ', ' + e.message);
            }
        }
    });
    Object.defineProperty(Function.prototype, 'invoke', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0(body) {
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        context$1$0.next = 2;
                        return regeneratorRuntime.awrap(this.load(body));
                    case 2:
                        context$1$0.next = 4;
                        return regeneratorRuntime.awrap(this.execute(body));
                    case 4:
                        return context$1$0.abrupt('return', context$1$0.sent);
                    case 5:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this);
        })
    });
}
Function.loadTimes = {};
Function.script = {};
Function.cacheFile = {};
exports.default = Function;