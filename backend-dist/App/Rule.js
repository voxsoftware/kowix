var VaravelNamespace = core.org.voxsoftware.Varavel;
var $mod$11 = core.VW.Ecma2015.Utils.module(require('url'));
var Fs = core.System.IO.Fs;
var $mod$12 = core.VW.Ecma2015.Utils.module(require('fs'));
var $mod$13 = core.VW.Ecma2015.Utils.module(require('path'));
var times = 0;
{
    var Rule = function Rule() {
        Rule.$constructor ? Rule.$constructor.apply(this, arguments) : Rule.$superClass && Rule.$superClass.apply(this, arguments);
    };
    Object.defineProperty(Rule, '$constructor', {
        enumerable: false,
        value: function (row, context) {
            this.row = row;
            this.context = context;
        }
    });
    Object.defineProperty(Rule.prototype, 'check', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0(body, options) {
            var row;
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        row = this.row;
                        if (!row.function) {
                            context$1$0.next = 8;
                            break;
                        }
                        this.func = new Varavel.Project.App.Function(row.function, this.context);
                        context$1$0.next = 5;
                        return regeneratorRuntime.awrap(this.func.load());
                    case 5:
                        context$1$0.next = 7;
                        return regeneratorRuntime.awrap(this.func.execute(body, options));
                    case 7:
                        return context$1$0.abrupt('return', context$1$0.sent);
                    case 8:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this);
        })
    });
}
exports.default = Rule;