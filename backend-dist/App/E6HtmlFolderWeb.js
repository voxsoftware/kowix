var fs = require('fs');
var Path = require('path');
var Fs = core.System.IO.Fs;
{
    var E6HtmlFolder = function E6HtmlFolder() {
        E6HtmlFolder.$constructor ? E6HtmlFolder.$constructor.apply(this, arguments) : E6HtmlFolder.$superClass && E6HtmlFolder.$superClass.apply(this, arguments);
    };
    Object.defineProperty(E6HtmlFolder, '$constructor', {
        enumerable: false,
        value: function (folder) {
            this.$folder = folder;
        }
    });
    Object.defineProperty(E6HtmlFolder.prototype, 'getFileName', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0(file) {
            var folder, exists, nf, file2, file3, i, ext;
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        folder = this.$folder;
                        file2 = Path.join(folder, file);
                        nf = file2;
                        E6HtmlFolder.caching = E6HtmlFolder.caching || {};
                        if (!E6HtmlFolder.caching[file2]) {
                            context$1$0.next = 6;
                            break;
                        }
                        return context$1$0.abrupt('return', E6HtmlFolder.caching[file2]);
                    case 6:
                        file3 = file2;
                        i = 0;
                    case 8:
                        if (!(i < E6HtmlFolder.extensions.length)) {
                            context$1$0.next = 19;
                            break;
                        }
                        ext = E6HtmlFolder.extensions[i];
                        file2 = Path.join(folder, file + ext);
                        context$1$0.next = 13;
                        return regeneratorRuntime.awrap(Fs.async.exists(file2));
                    case 13:
                        exists = context$1$0.sent;
                        if (!exists) {
                            context$1$0.next = 16;
                            break;
                        }
                        return context$1$0.abrupt('break', 19);
                    case 16:
                        i++;
                        context$1$0.next = 8;
                        break;
                    case 19:
                        if (exists) {
                            context$1$0.next = 23;
                            break;
                        }
                        context$1$0.next = 22;
                        return regeneratorRuntime.awrap(Fs.async.exists(file3));
                    case 22:
                        exists = context$1$0.sent;
                    case 23:
                        if (exists) {
                            context$1$0.next = 25;
                            break;
                        }
                        throw new core.System.IO.FileNotFoundException(file);
                    case 25:
                        vw.warning('FILE2:', file2);
                        E6HtmlFolder.caching[nf] = file2;
                        return context$1$0.abrupt('return', file2);
                    case 28:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this);
        })
    });
    Object.defineProperty(E6HtmlFolder.prototype, 'get', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0(file) {
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        context$1$0.next = 2;
                        return regeneratorRuntime.awrap(this.getFileName(file));
                    case 2:
                        file = context$1$0.sent;
                        return context$1$0.abrupt('return', Varavel.Project.App.E6HtmlFile.get(file));
                    case 4:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this);
        })
    });
    Object.defineProperty(E6HtmlFolder.prototype, 'invoke', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0(file, context, args) {
            var f;
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        context = context || core.VW.E6Html.E6Html.createConsoleContext();
                        context.arguments = args;
                        context.e6htmlFolder = this;
                        context$1$0.next = 5;
                        return regeneratorRuntime.awrap(this.get(file));
                    case 5:
                        f = context$1$0.sent;
                        context$1$0.next = 8;
                        return regeneratorRuntime.awrap(f.invoke(context));
                    case 8:
                        return context$1$0.abrupt('return', context$1$0.sent);
                    case 9:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this);
        })
    });
}
E6HtmlFolder.extensions = ['.es6.html'];
exports.default = E6HtmlFolder;