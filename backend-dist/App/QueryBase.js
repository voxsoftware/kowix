var VaravelNamespace = core.org.voxsoftware.Varavel;
var $mod$6 = core.VW.Ecma2015.Utils.module(require('url'));
var Fs = core.System.IO.Fs;
var $mod$7 = core.VW.Ecma2015.Utils.module(require('fs'));
var $mod$8 = core.VW.Ecma2015.Utils.module(require('path'));
var times = 0;
var $mod$9 = core.VW.Ecma2015.Utils.module(require('events'));
var $mod$10 = core.VW.Ecma2015.Utils.module(require('os'));
var uniqid = require('uniqid');
$mod$9.default.defaultMaxListeners = 100;
var Exception = Varavel.Project.App.Exception;
{
    var QueryBase = function QueryBase() {
        QueryBase.$constructor ? QueryBase.$constructor.apply(this, arguments) : QueryBase.$superClass && QueryBase.$superClass.apply(this, arguments);
    };
    Object.defineProperty(QueryBase, 'innerUserFunction', {
        enumerable: false,
        value: function (args, c, a) {
            return new Varavel.Project.App.Function(a, c, null, args.prefix);
        }
    });
    Object.defineProperty(QueryBase, 'context', {
        enumerable: false,
        value: function (args) {
            if (args.ctx)
                return args.ctx;
            var nargs = {};
            for (var id in args) {
                nargs[id] = args[id];
            }
            args = nargs;
            var c, site, osite, d;
            osite = args.site || Varavel.Project.App.Site.getFromArgs(args);
            var d = osite.getSiteDir();
            site = osite.idSite;
            var uf;
            uf = function (a) {
                return new Varavel.Project.App.Function(a, c, null, args.prefix);
            };
            var local1 = QueryBase.vars[site] = QueryBase.vars[site] || {};
            var global1 = QueryBase.vars[site + '.global'] = QueryBase.vars[site + '.global'] || {};
            c = {
                homeDir: 'deprecated',
                UserFunction: uf,
                userFunction: uf,
                getSiteContext: function (project) {
                    var p = new Varavel.Project.App.Project(project, args);
                    return p.getContext();
                },
                impersonate: function (project) {
                    return new Varavel.Project.App.Project(project, args);
                },
                kowiCode: Varavel.Funcs.env('KOWI_CODE'),
                setTimeout: setTimeout,
                setInterval: setInterval,
                clearTimeout: clearTimeout,
                clearInterval: clearInterval,
                IdSite: site,
                Buffer: Buffer,
                addGlobal: function (name, vari) {
                    c[name] = vari;
                    global1[name] = vari;
                    return vari;
                },
                context: args,
                Database: Varavel.Project.App.Database_User.get(site, args).putUserFunction(uf),
                Exception: Exception,
                regeneratorRuntime: regeneratorRuntime,
                publicContext: local1,
                uniqid: uniqid,
                core: core,
                constants: args.constants,
                UserContext: Varavel.Project.App.UserContext.get(args, site)
            };
            for (var id in global1) {
                c[id] = global1[id];
            }
            return args.ctx = c;
        }
    });
    Object.defineProperty(QueryBase, 'insert', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0(args, options, context) {
            var body, table, ins, response, r, i, result, rules, ruleLimit, rule, $_iterator0, $_normal0, $_err0, $_step0;
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        body = args._body;
                        ins = body.insert || body;
                        if (!(ins instanceof Array)) {
                            context$1$0.next = 15;
                            break;
                        }
                        response = [];
                        i = 0;
                    case 5:
                        if (!(i < ins.length)) {
                            context$1$0.next = 14;
                            break;
                        }
                        args._body.insert = ins[i];
                        context$1$0.next = 9;
                        return regeneratorRuntime.awrap(QueryBase.insert(args, options, context));
                    case 9:
                        r = context$1$0.sent;
                        response.push(r);
                    case 11:
                        i++;
                        context$1$0.next = 5;
                        break;
                    case 14:
                        return context$1$0.abrupt('return', response);
                    case 15:
                        context$1$0.next = 17;
                        return regeneratorRuntime.awrap(args.site.getRules(options.tablename + '.insert'));
                    case 17:
                        rules = context$1$0.sent;
                        context$1$0.next = 20;
                        return regeneratorRuntime.awrap(args.site.getRules('limit.' + options.tablename + '.insert'));
                    case 20:
                        ruleLimit = context$1$0.sent;
                        ruleLimit = ruleLimit && ruleLimit.length ? ruleLimit[0] : null;
                        if (ruleLimit) {
                            ruleLimit = new Varavel.Project.App.RuleLimit(ruleLimit, null);
                        }
                        if (!(rules && rules.length)) {
                            context$1$0.next = 48;
                            break;
                        }
                        typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined;
                        $_iterator0 = regeneratorRuntime.values(rules), $_normal0 = false;
                        context$1$0.prev = 26;
                    case 27:
                        if (!true) {
                            context$1$0.next = 39;
                            break;
                        }
                        $_step0 = $_iterator0.next();
                        if (!$_step0.done) {
                            context$1$0.next = 32;
                            break;
                        }
                        $_normal0 = true;
                        return context$1$0.abrupt('break', 39);
                    case 32:
                        rule = $_step0.value;
                        !context && (context = this.context(args));
                        rule = new Varavel.Project.App.Rule(rule, context);
                        context$1$0.next = 37;
                        return regeneratorRuntime.awrap(rule.check(ins, options));
                    case 37:
                        context$1$0.next = 27;
                        break;
                    case 39:
                        context$1$0.next = 45;
                        break;
                    case 41:
                        context$1$0.prev = 41;
                        context$1$0.t0 = context$1$0['catch'](26);
                        $_normal0 = false;
                        $_err0 = context$1$0.t0;
                    case 45:
                        try {
                            if (!$_normal0 && $_iterator0['return']) {
                                $_iterator0['return']();
                            }
                        } catch (e) {
                        }
                        if (!$_err0) {
                            context$1$0.next = 48;
                            break;
                        }
                        throw $_err0;
                    case 48:
                        if (!ruleLimit) {
                            context$1$0.next = 52;
                            break;
                        }
                        context$1$0.next = 51;
                        return regeneratorRuntime.awrap(ruleLimit.checkData(options, ins));
                    case 51:
                        ins = context$1$0.sent;
                    case 52:
                        context$1$0.next = 54;
                        return regeneratorRuntime.awrap(context.Database.table(options.tablename));
                    case 54:
                        table = context$1$0.sent;
                        if (!ins._id)
                            ins._id = Mongo.ObjectID();
                        ins.created = Date.now();
                        context$1$0.next = 59;
                        return regeneratorRuntime.awrap(table.insert(ins));
                    case 59:
                        return context$1$0.abrupt('return', ins);
                    case 60:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this, [[
                    26,
                    41
                ]]);
        })
    });
    Object.defineProperty(QueryBase, 'borrar', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0(args, options, context) {
            var body, table, q, data, result, rules, ruleLimit, getData, k, rule$1;
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        body = args._body;
                        q = body.query || { _id: body._id };
                        context$1$0.next = 4;
                        return regeneratorRuntime.awrap(args.site.getRules(options.tablename + '.delete'));
                    case 4:
                        rules = context$1$0.sent;
                        context$1$0.next = 7;
                        return regeneratorRuntime.awrap(args.site.getRules('limit.' + options.tablename + '.delete'));
                    case 7:
                        ruleLimit = context$1$0.sent;
                        ruleLimit = ruleLimit && ruleLimit.length ? ruleLimit[0] : null;
                        if (ruleLimit) {
                            ruleLimit = new Varavel.Project.App.RuleLimit(ruleLimit, null);
                        }
                        getData = (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0() {
                            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                                while (1)
                                    switch (context$1$0.prev = context$1$0.next) {
                                    case 0:
                                        if (!data) {
                                            context$1$0.next = 2;
                                            break;
                                        }
                                        return context$1$0.abrupt('return', data);
                                    case 2:
                                        if (table) {
                                            context$1$0.next = 6;
                                            break;
                                        }
                                        context$1$0.next = 5;
                                        return regeneratorRuntime.awrap(context.Database.table(options.tablename));
                                    case 5:
                                        table = context$1$0.sent;
                                    case 6:
                                        context$1$0.next = 8;
                                        return regeneratorRuntime.awrap(table.find(q, { _id: 1 }).toArray());
                                    case 8:
                                        data = context$1$0.sent;
                                        return context$1$0.abrupt('return', data);
                                    case 10:
                                    case 'end':
                                        return context$1$0.stop();
                                    }
                            }, null, this);
                        });
                        if (!true) {
                            context$1$0.next = 44;
                            break;
                        }
                        options.getData = getData;
                        if (!ruleLimit) {
                            context$1$0.next = 16;
                            break;
                        }
                        context$1$0.next = 16;
                        return regeneratorRuntime.awrap(ruleLimit.checkUpdate(options, args._body));
                    case 16:
                        if (!(rules && rules.length)) {
                            context$1$0.next = 27;
                            break;
                        }
                        k = 0;
                    case 18:
                        if (!(k < rules.length)) {
                            context$1$0.next = 27;
                            break;
                        }
                        rule$1 = rules[k];
                        !context && (context = this.context(args));
                        rule$1 = new Varavel.Project.App.Rule(rule$1, context);
                        context$1$0.next = 24;
                        return regeneratorRuntime.awrap(rule$1.check(q, options));
                    case 24:
                        k++;
                        context$1$0.next = 18;
                        break;
                    case 27:
                        if (data) {
                            context$1$0.next = 31;
                            break;
                        }
                        context$1$0.next = 30;
                        return regeneratorRuntime.awrap(getData());
                    case 30:
                        data = context$1$0.sent;
                    case 31:
                        if (table) {
                            context$1$0.next = 35;
                            break;
                        }
                        context$1$0.next = 34;
                        return regeneratorRuntime.awrap(context.Database.table(options.tablename));
                    case 34:
                        table = context$1$0.sent;
                    case 35:
                        if (!(ruleLimit && ruleLimit.hardDelete)) {
                            context$1$0.next = 41;
                            break;
                        }
                        context$1$0.next = 38;
                        return regeneratorRuntime.awrap(table.remove({
                            _id: {
                                $in: data.map(function (a) {
                                    return a._id;
                                })
                            }
                        }));
                    case 38:
                        result = context$1$0.sent;
                        context$1$0.next = 44;
                        break;
                    case 41:
                        context$1$0.next = 43;
                        return regeneratorRuntime.awrap(table.update({
                            _id: {
                                $in: data.map(function (a) {
                                    return a._id;
                                })
                            }
                        }, { $set: { deleted: true } }, { multi: true }));
                    case 43:
                        result = context$1$0.sent;
                    case 44:
                        return context$1$0.abrupt('return', result);
                    case 45:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this);
        })
    });
    Object.defineProperty(QueryBase, 'edicion', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0(args, options, context) {
            var body, table, q, up, data, result, rules, ruleLimit, getData, k, rule$2, id;
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        body = args._body;
                        q = body.query;
                        up = body.update;
                        if (!args.site) {
                            args.site = new Varavel.Project.App.Site(args);
                        }
                        context$1$0.next = 6;
                        return regeneratorRuntime.awrap(args.site.getRules(options.tablename + '.edit'));
                    case 6:
                        rules = context$1$0.sent;
                        context$1$0.next = 9;
                        return regeneratorRuntime.awrap(args.site.getRules('limit.' + options.tablename + '.edit'));
                    case 9:
                        ruleLimit = context$1$0.sent;
                        ruleLimit = ruleLimit && ruleLimit.length ? ruleLimit[0] : null;
                        if (ruleLimit) {
                            ruleLimit = new Varavel.Project.App.RuleLimit(ruleLimit, null);
                        }
                        getData = (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0() {
                            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                                while (1)
                                    switch (context$1$0.prev = context$1$0.next) {
                                    case 0:
                                        if (!data) {
                                            context$1$0.next = 2;
                                            break;
                                        }
                                        return context$1$0.abrupt('return', data);
                                    case 2:
                                        if (table) {
                                            context$1$0.next = 6;
                                            break;
                                        }
                                        context$1$0.next = 5;
                                        return regeneratorRuntime.awrap(context.Database.table(options.tablename));
                                    case 5:
                                        table = context$1$0.sent;
                                    case 6:
                                        context$1$0.next = 8;
                                        return regeneratorRuntime.awrap(table.find(q, { _id: 1 }).toArray());
                                    case 8:
                                        data = context$1$0.sent;
                                        return context$1$0.abrupt('return', data);
                                    case 10:
                                    case 'end':
                                        return context$1$0.stop();
                                    }
                            }, null, this);
                        });
                        if (!true) {
                            context$1$0.next = 43;
                            break;
                        }
                        options.getData = getData;
                        if (!ruleLimit) {
                            context$1$0.next = 19;
                            break;
                        }
                        context$1$0.next = 18;
                        return regeneratorRuntime.awrap(ruleLimit.checkUpdate(options, q, up));
                    case 18:
                        up = context$1$0.sent;
                    case 19:
                        if (!(rules && rules.length)) {
                            context$1$0.next = 30;
                            break;
                        }
                        k = 0;
                    case 21:
                        if (!(k < rules.length)) {
                            context$1$0.next = 30;
                            break;
                        }
                        rule$2 = rules[k];
                        !context && (context = this.context(args));
                        rule$2 = new Varavel.Project.App.Rule(rule$2, context);
                        context$1$0.next = 27;
                        return regeneratorRuntime.awrap(rule$2.check(body, options));
                    case 27:
                        k++;
                        context$1$0.next = 21;
                        break;
                    case 30:
                        if (data) {
                            context$1$0.next = 34;
                            break;
                        }
                        context$1$0.next = 33;
                        return regeneratorRuntime.awrap(getData());
                    case 33:
                        data = context$1$0.sent;
                    case 34:
                        if (table) {
                            context$1$0.next = 38;
                            break;
                        }
                        context$1$0.next = 37;
                        return regeneratorRuntime.awrap(context.Database.table(options.tablename));
                    case 37:
                        table = context$1$0.sent;
                    case 38:
                        for (id in up) {
                            if (!id.startsWith('$')) {
                                up.$set = up.$set || {};
                                up.$set[id] = up[id];
                                delete up[id];
                            }
                        }
                        up.$set.updated = Date.now();
                        context$1$0.next = 42;
                        return regeneratorRuntime.awrap(table.update({
                            _id: {
                                $in: data.map(function (a) {
                                    return a._id;
                                })
                            }
                        }, up, { multi: true }));
                    case 42:
                        result = context$1$0.sent;
                    case 43:
                        return context$1$0.abrupt('return', result);
                    case 44:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this);
        })
    });
    Object.defineProperty(QueryBase, 'informacion', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0(args, options, context, ubody) {
            var body, table, q, project, relation, limit, sort, rules, ev, ruleLimit, k, rule$3, f1, f2, tune, val, q2, d, keys, info, v1, i, relat$4, y, inf$5, z, v$7, a, d1$6, obj, $_iterator1, $_normal1, $_err1, $_step1;
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        body = ubody || args._body;
                        q = body.query || body;
                        project = null;
                        relation = body.relation;
                        limit = 100, sort = { created: -1 };
                        context$1$0.next = 7;
                        return regeneratorRuntime.awrap(args.site.getRules(options.tablename + '.get'));
                    case 7:
                        rules = context$1$0.sent;
                        context$1$0.next = 10;
                        return regeneratorRuntime.awrap(args.site.getRules('limit.' + options.tablename + '.get'));
                    case 10:
                        ruleLimit = context$1$0.sent;
                        ruleLimit = ruleLimit && ruleLimit.length ? ruleLimit[0] : null;
                        if (ruleLimit) {
                            ruleLimit = new Varavel.Project.App.RuleLimit(ruleLimit, null);
                        }
                        if (!(rules && rules.length)) {
                            context$1$0.next = 27;
                            break;
                        }
                        k = 0;
                    case 15:
                        if (!(k < rules.length)) {
                            context$1$0.next = 27;
                            break;
                        }
                        rule$3 = rules[k];
                        !context && (context = this.context(args));
                        rule$3 = new Varavel.Project.App.Rule(rule$3, context);
                        context$1$0.next = 21;
                        return regeneratorRuntime.awrap(rule$3.check(body, options));
                    case 21:
                        ev = context$1$0.sent;
                        if (!(ev && ev.defaultPrevented)) {
                            context$1$0.next = 24;
                            break;
                        }
                        return context$1$0.abrupt('return', ev.data);
                    case 24:
                        k++;
                        context$1$0.next = 15;
                        break;
                    case 27:
                        if (!body.limit) {
                            context$1$0.next = 33;
                            break;
                        }
                        limit = body.limit;
                        if (!ruleLimit) {
                            context$1$0.next = 33;
                            break;
                        }
                        context$1$0.next = 32;
                        return regeneratorRuntime.awrap(ruleLimit.checkLimit(limit));
                    case 32:
                        limit = context$1$0.sent;
                    case 33:
                        if (!(body.query && body.sort)) {
                            context$1$0.next = 39;
                            break;
                        }
                        sort = body.sort;
                        if (!ruleLimit) {
                            context$1$0.next = 39;
                            break;
                        }
                        context$1$0.next = 38;
                        return regeneratorRuntime.awrap(ruleLimit.checkSort(sort));
                    case 38:
                        sort = context$1$0.sent;
                    case 39:
                        context$1$0.next = 41;
                        return regeneratorRuntime.awrap(context.Database.table(options.tablename));
                    case 41:
                        table = context$1$0.sent;
                        if (!(body.aggregate || body.aggregate_pipeline)) {
                            context$1$0.next = 45;
                            break;
                        }
                        context$1$0.next = 58;
                        break;
                    case 45:
                        if (!body.project) {
                            context$1$0.next = 53;
                            break;
                        }
                        project = body.project;
                        if (!ruleLimit) {
                            context$1$0.next = 51;
                            break;
                        }
                        context$1$0.next = 50;
                        return regeneratorRuntime.awrap(ruleLimit.checkProjection(project));
                    case 50:
                        project = context$1$0.sent;
                    case 51:
                        context$1$0.next = 58;
                        break;
                    case 53:
                        if (!body.query) {
                            context$1$0.next = 58;
                            break;
                        }
                        if (!ruleLimit) {
                            context$1$0.next = 58;
                            break;
                        }
                        context$1$0.next = 57;
                        return regeneratorRuntime.awrap(ruleLimit.checkProjection({}));
                    case 57:
                        project = context$1$0.sent;
                    case 58:
                        if (body.aggregate) {
                            body.aggregate_pipeline = [
                                { $match: body.query },
                                { $project: body.aggregate }
                            ];
                        }
                        if (!body.aggregate_pipeline) {
                            context$1$0.next = 74;
                            break;
                        }
                        if (!ruleLimit) {
                            context$1$0.next = 71;
                            break;
                        }
                        i = 0;
                    case 62:
                        if (!(i < body.aggregate_pipeline.length)) {
                            context$1$0.next = 71;
                            break;
                        }
                        if (!body.aggregate_pipeline[i].$project) {
                            context$1$0.next = 68;
                            break;
                        }
                        context$1$0.next = 66;
                        return regeneratorRuntime.awrap(ruleLimit.checkProjection(body.aggregate_pipeline[i].$project, 'aggregation'));
                    case 66:
                        body.aggregate_pipeline[i].$project = context$1$0.sent;
                        return context$1$0.abrupt('break', 71);
                    case 68:
                        i++;
                        context$1$0.next = 62;
                        break;
                    case 71:
                        info = table.aggregate(body.aggregate_pipeline).limit(limit);
                        context$1$0.next = 80;
                        break;
                    case 74:
                        if (!(body.count == 1)) {
                            context$1$0.next = 79;
                            break;
                        }
                        info = table.count(q);
                        return context$1$0.abrupt('return', info);
                    case 79:
                        info = (project ? table.find(q, { fields: project }) : table.find(q)).limit(limit).sort(sort);
                    case 80:
                        context$1$0.next = 82;
                        return regeneratorRuntime.awrap(info.toArray());
                    case 82:
                        info = context$1$0.sent;
                        if (!info.length) {
                            context$1$0.next = 133;
                            break;
                        }
                        if (!(relation && relation.length)) {
                            context$1$0.next = 133;
                            break;
                        }
                        i = 0;
                    case 86:
                        if (!(i < relation.length)) {
                            context$1$0.next = 133;
                            break;
                        }
                        relat$4 = relation[i];
                        tune = {};
                        process.env.DEBUG == 1 && vw.warning('RELAT: ', relat$4);
                        keys = [];
                        f1 = relat$4.field1;
                        for (y = 0; y < info.length; y++) {
                            inf$5 = info[y];
                            val = inf$5[f1];
                            if (val) {
                                if (val instanceof Array) {
                                    for (z = 0; z < val.length; z++) {
                                        v$7 = val[z];
                                        v1 = v$7.value || v$7;
                                        tune[v1] = tune[v1] || [];
                                        tune[v1].push(inf$5);
                                        keys.push(v1);
                                    }
                                } else {
                                    v1 = val.value || val;
                                    tune[v1] = tune[v1] || [];
                                    tune[v1].push(inf$5);
                                    keys.push(v1);
                                }
                            }
                        }
                        f2 = relat$4.field2;
                        q2 = relat$4.query || {};
                        q2[f2] = { $in: keys };
                        context$1$0.next = 98;
                        return regeneratorRuntime.awrap(this.informacion(args, relat$4.options, context, {
                            query: q2,
                            relation: relat$4.relation,
                            limit: relat$4.limit,
                            sort: relat$4.sort
                        }));
                    case 98:
                        d = context$1$0.sent;
                        if (!(d && d.length)) {
                            context$1$0.next = 130;
                            break;
                        }
                        a = 0;
                    case 101:
                        if (!(a < d.length)) {
                            context$1$0.next = 130;
                            break;
                        }
                        d1$6 = d[a];
                        val = d1$6[f2];
                        if (!tune[val]) {
                            context$1$0.next = 127;
                            break;
                        }
                        typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined;
                        $_iterator1 = regeneratorRuntime.values(tune[val]), $_normal1 = false;
                        context$1$0.prev = 107;
                    case 108:
                        if (!true) {
                            context$1$0.next = 118;
                            break;
                        }
                        $_step1 = $_iterator1.next();
                        if (!$_step1.done) {
                            context$1$0.next = 113;
                            break;
                        }
                        $_normal1 = true;
                        return context$1$0.abrupt('break', 118);
                    case 113:
                        obj = $_step1.value;
                        obj[relat$4.name] = obj[relat$4.name] || [];
                        obj[relat$4.name].push(d1$6);
                        context$1$0.next = 108;
                        break;
                    case 118:
                        context$1$0.next = 124;
                        break;
                    case 120:
                        context$1$0.prev = 120;
                        context$1$0.t0 = context$1$0['catch'](107);
                        $_normal1 = false;
                        $_err1 = context$1$0.t0;
                    case 124:
                        try {
                            if (!$_normal1 && $_iterator1['return']) {
                                $_iterator1['return']();
                            }
                        } catch (e) {
                        }
                        if (!$_err1) {
                            context$1$0.next = 127;
                            break;
                        }
                        throw $_err1;
                    case 127:
                        a++;
                        context$1$0.next = 101;
                        break;
                    case 130:
                        i++;
                        context$1$0.next = 86;
                        break;
                    case 133:
                        if (ruleLimit) {
                            info = ruleLimit.transformData(info);
                        }
                        return context$1$0.abrupt('return', info);
                    case 135:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this, [[
                    107,
                    120
                ]]);
        })
    });
}
QueryBase.vars = {};
exports.default = QueryBase;