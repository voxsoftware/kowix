var $mod$19 = core.VW.Ecma2015.Utils.module(require('path'));
var Fs = core.System.IO.Fs;
var $mod$20 = core.VW.Ecma2015.Utils.module(require('fs'));
var tempdir = $mod$19.default.join(global.appDir, 'cache', 'tmp');
{
    var Site = function Site() {
        Site.$constructor ? Site.$constructor.apply(this, arguments) : Site.$superClass && Site.$superClass.apply(this, arguments);
    };
    Object.defineProperty(Site, 'getFromArgs', {
        enumerable: false,
        value: function (args) {
            if (args.site)
                return args.site;
            args.site = new Site(args);
            return args.site;
        }
    });
    Site.__defineGetter__('appDir', function () {
        return process.env.BASE_DIR || global.baseDir || global.appDir;
    });
    Object.defineProperty(Site, 'getFromId', {
        enumerable: false,
        value: function (site) {
            var s = new Site();
            s.site = site;
            return s;
        }
    });
    Object.defineProperty(Site, '$constructor', {
        enumerable: false,
        value: function (args) {
            if (args) {
                this.args = args;
                args.request.params = args.request.params || {};
                if (args.request.params.site) {
                    this.site = args.request.params.site;
                } else {
                    this.site = args.request.headers.host;
                    this.site = this.site.split(':')[0];
                }
            }
        }
    });
    Site.prototype.__defineGetter__('idSite', function () {
        return this.site;
    });
    Object.defineProperty(Site.prototype, 'getRealSite', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0() {
            var parts, tsite, cached, siteDir, r, newsite, p, ctx;
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        if (!this.gettedRealsite) {
                            context$1$0.next = 2;
                            break;
                        }
                        return context$1$0.abrupt('return');
                    case 2:
                        context$1$0.prev = 2;
                        parts = this.site.split('::');
                        tsite = parts[0];
                        this.site = tsite;
                        Site.customDomains = Site.customDomains || {};
                        cached = Site.customDomains[this.site];
                        if (!(cached && cached.updated >= Date.now() - Varavel.Funcs.env('CACHE_STATIC_TIME', 60000))) {
                            context$1$0.next = 13;
                            break;
                        }
                        this.site = cached.site;
                        this.usiteDir = cached.siteDir;
                        this.found = cached.found;
                        return context$1$0.abrupt('return');
                    case 13:
                        console.log('Cache not up to date ....', parts[0]);
                        if (!(tsite == '127.0.0.1' && process.env.DEFAULT_DIR)) {
                            context$1$0.next = 19;
                            break;
                        }
                        siteDir = process.env.DEFAULT_DIR;
                        this.usiteDir = siteDir;
                        context$1$0.next = 46;
                        break;
                    case 19:
                        siteDir = $mod$19.default.join(Site.appDir, '..', tsite);
                        if (Fs.sync.exists(siteDir)) {
                            context$1$0.next = 46;
                            break;
                        }
                        siteDir = $mod$19.default.join(Site.appDir, '..', 'domains', tsite);
                        context$1$0.prev = 22;
                        context$1$0.next = 25;
                        return regeneratorRuntime.awrap(Fs.async.readFile(siteDir, 'utf8'));
                    case 25:
                        r = context$1$0.sent;
                        r = r.trim();
                        newsite = r;
                        context$1$0.next = 33;
                        break;
                    case 30:
                        context$1$0.prev = 30;
                        context$1$0.t0 = context$1$0['catch'](22);
                        siteDir = null;
                    case 33:
                        if (!(siteDir == null)) {
                            context$1$0.next = 46;
                            break;
                        }
                        siteDir = $mod$19.default.join(Site.appDir, '..', 'domains_v2', parts[0]);
                        context$1$0.prev = 35;
                        context$1$0.next = 38;
                        return regeneratorRuntime.awrap(Fs.async.readFile(siteDir, 'utf8'));
                    case 38:
                        r = context$1$0.sent;
                        r = r.trim();
                        newsite = r;
                        context$1$0.next = 46;
                        break;
                    case 43:
                        context$1$0.prev = 43;
                        context$1$0.t1 = context$1$0['catch'](35);
                        siteDir = null;
                    case 46:
                        if (siteDir) {
                            context$1$0.next = 72;
                            break;
                        }
                        if (!(parts.length > 1)) {
                            context$1$0.next = 72;
                            break;
                        }
                        context$1$0.prev = 48;
                        p = new Varavel.Project.App.Project('kowix', this.args);
                        context$1$0.next = 52;
                        return regeneratorRuntime.awrap(p.getContext());
                    case 52:
                        ctx = context$1$0.sent;
                        context$1$0.next = 55;
                        return regeneratorRuntime.awrap(ctx.userFunction('download.site').invoke({
                            'site': parts[0],
                            'key': parts[1]
                        }));
                    case 55:
                        siteDir = $mod$19.default.join(Site.appDir, '..', 'domains_v2', parts[0]);
                        context$1$0.prev = 56;
                        context$1$0.next = 59;
                        return regeneratorRuntime.awrap(Fs.async.readFile(siteDir, 'utf8'));
                    case 59:
                        r = context$1$0.sent;
                        r = r.trim();
                        newsite = r;
                        context$1$0.next = 67;
                        break;
                    case 64:
                        context$1$0.prev = 64;
                        context$1$0.t2 = context$1$0['catch'](56);
                        siteDir = null;
                    case 67:
                        context$1$0.next = 72;
                        break;
                    case 69:
                        context$1$0.prev = 69;
                        context$1$0.t3 = context$1$0['catch'](48);
                        Site.customDomains[tsite] = {
                            updated: Date.now(),
                            site: newsite || tsite,
                            found: !!siteDir,
                            siteDir: this.usiteDir,
                            error: context$1$0.t3
                        };
                    case 72:
                        Site.customDomains[tsite] = {
                            updated: Date.now(),
                            site: newsite || tsite,
                            found: !!siteDir,
                            siteDir: this.usiteDir
                        };
                        if (this.site != tsite) {
                            Site.customDomains[this.site] = Site.customDomains[tsite];
                        }
                        if (newsite) {
                            this.site = newsite;
                            Site.customDomains[tsite] = {
                                updated: Date.now(),
                                site: newsite || tsite,
                                found: !!siteDir
                            };
                            Site.customDomains[newsite] = Site.customDomains[tsite];
                        }
                        if (!siteDir && this.site == 'kowix')
                            siteDir = global.appDir;
                        this.found = !!siteDir;
                        context$1$0.next = 82;
                        break;
                    case 79:
                        context$1$0.prev = 79;
                        context$1$0.t4 = context$1$0['catch'](2);
                        throw context$1$0.t4;
                    case 82:
                        context$1$0.prev = 82;
                        this.gettedRealsite = true;
                        return context$1$0.finish(82);
                    case 85:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this, [
                [
                    2,
                    79,
                    82,
                    85
                ],
                [
                    22,
                    30
                ],
                [
                    35,
                    43
                ],
                [
                    48,
                    69
                ],
                [
                    56,
                    64
                ]
            ]);
        })
    });
    Object.defineProperty(Site.prototype, 'getSiteDir', {
        enumerable: false,
        value: function () {
            if (!this.found)
                throw new core.System.Exception('Site ' + this.site + ' not found');
            return this.usiteDir || $mod$19.default.join(Site.appDir, '..', this.site);
        }
    });
    Object.defineProperty(Site.prototype, 'getPublicPath', {
        enumerable: false,
        value: function () {
            var p = this.getSiteDir();
            return $mod$19.default.join(p, 'public');
        }
    });
    Object.defineProperty(Site.prototype, 'getFunctionPath', {
        enumerable: false,
        value: function () {
            var p = this.getSiteDir();
            return $mod$19.default.join(p, 'functions');
        }
    });
    Object.defineProperty(Site.prototype, 'getFunctionPathMode2', {
        enumerable: false,
        value: function () {
            return this.getFunctionPath();
        }
    });
    Object.defineProperty(Site.prototype, 'getSiteDirMode2', {
        enumerable: false,
        value: function () {
            return this.getSiteDir();
        }
    });
    Object.defineProperty(Site.prototype, 'readState', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0(options) {
            var cache, cache2, info;
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        Site.siteVars = Site.siteVars || {};
                        cache = Site.siteVars[this.site];
                        cache2 = Site.siteVars[this.site + '.error'];
                        if (!(cache && Date.now() - cache.updated <= 120000)) {
                            context$1$0.next = 5;
                            break;
                        }
                        return context$1$0.abrupt('return', cache.code);
                    case 5:
                        if (!(cache2 && Date.now() - cache2.updated <= Varavel.Funcs.env('CACHE_STATIC_TIME', 120000))) {
                            context$1$0.next = 7;
                            break;
                        }
                        throw cache2.error;
                    case 7:
                        context$1$0.next = 9;
                        return regeneratorRuntime.awrap(this.getFunction('vars', '.json', options));
                    case 9:
                        info = context$1$0.sent;
                        if (info) {
                            context$1$0.next = 14;
                            break;
                        }
                        cache2 = {
                            error: new core.System.Exception('Site ' + this.site + ' it\'s not enabled'),
                            updated: Date.now()
                        };
                        Site.siteVars[this.site + '.error'] = cache2;
                        throw cache2.error;
                    case 14:
                        if (!info.compiledCode) {
                            info.compiledCode = JSON.parse(info.code);
                        }
                        cache = cache || {};
                        Site.siteVars[this.site] = cache;
                        if (Site.siteVars[this.site + '.error'])
                            delete Site.siteVars[this.site + '.error'];
                        cache.updated = Date.now();
                        return context$1$0.abrupt('return', cache.code = info.compiledCode);
                    case 20:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this);
        })
    });
    Object.defineProperty(Site.prototype, 'getRules', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0(rule) {
            var rules, ruleAll, rs, rules2, ar, i;
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        context$1$0.next = 2;
                        return regeneratorRuntime.awrap(this.getFunction('rules.' + rule, '.json'));
                    case 2:
                        rules = context$1$0.sent;
                        vw.warning('RULE HERE:', rule, rules);
                        if (rules && !rules.compiledCode) {
                            rules.compiledCode = JSON.parse(rules.code);
                        }
                        rs = rule.split('.');
                        rs.splice(rs.length > 3 ? 1 : 0, 1);
                        ruleAll = rs.join('.');
                        context$1$0.next = 10;
                        return regeneratorRuntime.awrap(this.getFunction(ruleAll, '.json'));
                    case 10:
                        rules2 = context$1$0.sent;
                        if (rules2 && !rules2.compiledCode) {
                            rules2.compiledCode = JSON.parse(rules2.code);
                        }
                        ar = [];
                        if (rules && rules.compiledCode) {
                            for (i = 0; i < rules.compiledCode.length; i++) {
                                ar.push(rules.compiledCode[i]);
                            }
                        }
                        if (rules2 && rules2.compiledCode) {
                            for (i = 0; i < rules2.compiledCode.length; i++) {
                                ar.push(rules2.compiledCode[i]);
                            }
                        }
                        return context$1$0.abrupt('return', ar);
                    case 16:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this);
        })
    });
    Object.defineProperty(Site.prototype, 'getFile', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0($_ref1) {
            var path, fullpath, error404, error, success, headers, site, pub, url, req, response, cache, exists, uname;
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        path = $_ref1 ? $_ref1.path : undefined, fullpath = $_ref1 ? $_ref1.fullpath : undefined, error404 = $_ref1 ? $_ref1.error404 : undefined, error = $_ref1 ? $_ref1.error : undefined, success = $_ref1 ? $_ref1.success : undefined, headers = $_ref1 ? $_ref1.headers : undefined;
                        site = this;
                        if (fullpath) {
                            context$1$0.next = 11;
                            break;
                        }
                        context$1$0.t0 = $mod$19.default;
                        context$1$0.next = 6;
                        return regeneratorRuntime.awrap(site.getSiteDirMode2());
                    case 6:
                        context$1$0.t1 = context$1$0.sent;
                        pub = context$1$0.t0.join.call(context$1$0.t0, context$1$0.t1, 'public');
                        pub = $mod$19.default.join(pub, path);
                        context$1$0.next = 12;
                        break;
                    case 11:
                        pub = fullpath;
                    case 12:
                        if (!Varavel.Funcs.env('FILE_CACHE_SERVER')) {
                            context$1$0.next = 27;
                            break;
                        }
                        url = Varavel.Funcs.env('FILE_CACHE_SERVER');
                        url += '/api/staticinfo?file=' + encodeURIComponent(pub);
                        req = new core.VW.Http.Request(url);
                        context$1$0.next = 18;
                        return regeneratorRuntime.awrap(req.getResponseAsync());
                    case 18:
                        response = context$1$0.sent;
                        if (typeof response.body == 'string') {
                            response.body = JSON.parse(response.body);
                        }
                        if (!response.body.error) {
                            context$1$0.next = 22;
                            break;
                        }
                        return context$1$0.abrupt('return', error(response.body.error));
                    case 22:
                        if (!response.body.noexists) {
                            context$1$0.next = 24;
                            break;
                        }
                        return context$1$0.abrupt('return', error404());
                    case 24:
                        if (!response.body) {
                            context$1$0.next = 26;
                            break;
                        }
                        return context$1$0.abrupt('return', success(response.body));
                    case 26:
                        return context$1$0.abrupt('return');
                    case 27:
                        cache = Site.fileCache[pub];
                        uname = pub.replace(/\//ig, '$');
                        context$1$0.prev = 29;
                        if (!(cache && cache.exists === false)) {
                            context$1$0.next = 47;
                            break;
                        }
                        if (!(cache && Date.now() - cache.updated <= parseInt(Varavel.Funcs.env('CACHE_STATIC_TIME', 1.5 * 60000)))) {
                            context$1$0.next = 37;
                            break;
                        }
                        if (!error404) {
                            context$1$0.next = 36;
                            break;
                        }
                        context$1$0.next = 35;
                        return regeneratorRuntime.awrap(error404());
                    case 35:
                        return context$1$0.abrupt('return', context$1$0.sent);
                    case 36:
                        throw new core.System.IO.FileNotFoundException('File not found');
                    case 37:
                        try {
                            exists = Fs.sync.exists(pub);
                        } catch (e) {
                        }
                        if (exists) {
                            context$1$0.next = 45;
                            break;
                        }
                        Site.fileCache[pub] = {
                            file: pub,
                            exists: false,
                            updated: Date.now()
                        };
                        if (!error404) {
                            context$1$0.next = 44;
                            break;
                        }
                        context$1$0.next = 43;
                        return regeneratorRuntime.awrap(error404());
                    case 43:
                        return context$1$0.abrupt('return', context$1$0.sent);
                    case 44:
                        throw new core.System.IO.FileNotFoundException('File not found');
                    case 45:
                        context$1$0.next = 56;
                        break;
                    case 47:
                        if (cache) {
                            context$1$0.next = 56;
                            break;
                        }
                        exists = Fs.sync.exists(pub);
                        if (exists) {
                            context$1$0.next = 56;
                            break;
                        }
                        Site.fileCache[pub] = {
                            file: pub,
                            exists: false,
                            updated: Date.now()
                        };
                        if (!error404) {
                            context$1$0.next = 55;
                            break;
                        }
                        context$1$0.next = 54;
                        return regeneratorRuntime.awrap(error404());
                    case 54:
                        return context$1$0.abrupt('return', context$1$0.sent);
                    case 55:
                        throw new core.System.IO.FileNotFoundException('File not found');
                    case 56:
                        success({
                            tmpfile: $mod$19.default.join(tempdir, uname),
                            file: pub,
                            name: $mod$19.default.basename(pub)
                        });
                        context$1$0.next = 62;
                        break;
                    case 59:
                        context$1$0.prev = 59;
                        context$1$0.t2 = context$1$0['catch'](29);
                        error(context$1$0.t2);
                    case 62:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this, [[
                    29,
                    59
                ]]);
        })
    });
    Object.defineProperty(Site.prototype, 'getFunction', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0(cid, extension, options) {
            var path, func, content, self, cache, stat, cachetime, task, result;
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        if (extension === undefined) {
                            extension = '.js';
                        }
                        path = this.getFunctionPath();
                        func = $mod$19.default.join(path, cid + extension);
                        self = this;
                        Site.cache = Site.cache || {};
                        cachetime = Varavel.Funcs.env('CACHE_API_TIME', 300000);
                        cache = Site.cache[this.site + '_' + cid];
                        task = new core.VW.Task();
                        if (!(!cache || Date.now() - cache.updated > cachetime || options && options.development_mode == 1)) {
                            context$1$0.next = 37;
                            break;
                        }
                        context$1$0.next = 11;
                        return regeneratorRuntime.awrap(self.getFile({
                            fullpath: func,
                            error404: function () {
                                task.result = task.result || {};
                                task.result.noexists = true;
                                task.finish();
                            },
                            error: function (e) {
                                task.exception = e;
                                task.finish();
                            },
                            success: function (r) {
                                task.result = r;
                                task.finish();
                            }
                        }));
                    case 11:
                        context$1$0.next = 13;
                        return regeneratorRuntime.awrap(task);
                    case 13:
                        result = context$1$0.sent;
                        result = result || {};
                        if (!result.noexists) {
                            context$1$0.next = 23;
                            break;
                        }
                        task = new core.VW.Task();
                        func = $mod$19.default.join(path, cid + (extension != '.js' ? '.js' : '.es6'));
                        self.getFile({
                            fullpath: func,
                            error404: function () {
                                task.result = task.result || {};
                                task.result.noexists = true;
                                task.finish();
                            },
                            error: function (e) {
                                task.exception = e;
                                task.finish();
                            },
                            success: function (r) {
                                task.result = r;
                                task.finish();
                            }
                        });
                        context$1$0.next = 21;
                        return regeneratorRuntime.awrap(task);
                    case 21:
                        result = context$1$0.sent;
                        result = result || {};
                    case 23:
                        if (result.noexists) {
                            context$1$0.next = 37;
                            break;
                        }
                        context$1$0.next = 26;
                        return regeneratorRuntime.awrap(Fs.async.stat(result.file));
                    case 26:
                        stat = context$1$0.sent;
                        if (!(cache && cache.updated)) {
                            context$1$0.next = 31;
                            break;
                        }
                        if (!(stat.mtime.getTime() <= cache.mtime)) {
                            context$1$0.next = 31;
                            break;
                        }
                        cache.updated = Date.now();
                        return context$1$0.abrupt('return', cache);
                    case 31:
                        context$1$0.next = 33;
                        return regeneratorRuntime.awrap(Fs.async.readFile(result.file, 'utf8'));
                    case 33:
                        content = context$1$0.sent;
                        if (cache) {
                        }
                        Site.cache[self.site + '_' + cid] = null;
                        cache = Site.cache[self.site + '_' + cid] = {
                            file: result.file,
                            codefile: (result.tmpfile || result.file) + '$$-code',
                            mtime: stat.mtime.getTime(),
                            remotefile: func,
                            code: content,
                            ext: $mod$19.default.extname(func),
                            updated: Date.now()
                        };
                    case 37:
                        return context$1$0.abrupt('return', cache);
                    case 38:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this);
        })
    });
}
Site.fileCache = {};
exports.default = Site;