{
    var Exception = function Exception() {
        Exception.$constructor ? Exception.$constructor.apply(this, arguments) : Exception.$superClass && Exception.$superClass.apply(this, arguments);
    };
    Exception.prototype = Object.create(core.System.Exception.prototype);
    Object.setPrototypeOf ? Object.setPrototypeOf(Exception, core.System.Exception) : Exception.__proto__ = core.System.Exception;
    Exception.prototype.constructor = Exception;
    Exception.$super = core.System.Exception.prototype;
    Exception.$superClass = core.System.Exception;
    Object.defineProperty(Exception.prototype, 'putCode', {
        enumerable: false,
        value: function (code, pars) {
            this.code = code;
            if (pars)
                this.putParameters(pars);
            return this;
        }
    });
    Object.defineProperty(Exception, 'create', {
        enumerable: false,
        value: function (message, innerException) {
            return new Exception(message, innerException);
        }
    });
    Object.defineProperty(Exception.prototype, 'end', {
        enumerable: false,
        value: function () {
            return this;
        }
    });
    Object.defineProperty(Exception.prototype, 'putParameters', {
        enumerable: false,
        value: function (pars) {
            if (pars)
                this.pars = pars;
            return this;
        }
    });
}
exports.default = Exception;