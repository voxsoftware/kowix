var $mod$0 = core.VW.Ecma2015.Utils.module(require('path'));
{
    var Database_User = function Database_User() {
        Database_User.$constructor ? Database_User.$constructor.apply(this, arguments) : Database_User.$superClass && Database_User.$superClass.apply(this, arguments);
    };
    Object.defineProperty(Database_User, '$constructor', {
        enumerable: false,
        value: function (args) {
            this.prefix = args.prefix;
            this.vars = args.constants;
            if (this.vars)
                this.dbconnection = this.vars.dbconnection;
        }
    });
    Object.defineProperty(Database_User, 'create', {
        enumerable: false,
        value: function (args) {
            return new Database_User(args);
        }
    });
    Object.defineProperty(Database_User.prototype, 'getDatabase', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0() {
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        return context$1$0.abrupt('return', this);
                    case 1:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this);
        })
    });
    Object.defineProperty(Database_User.prototype, 'putUserFunction', {
        enumerable: false,
        value: function (uf) {
            this.UserFunction = uf;
            return this;
        }
    });
    Object.defineProperty(Database_User, 'get', {
        enumerable: false,
        value: function (idSite, args) {
            return new Database_User(args, idSite);
        }
    });
    Object.defineProperty(Database_User.prototype, 'table', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0(name) {
            var prefix, DatabaseF;
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        prefix = this.prefix;
                        if (!(!prefix && !this.dbconnection)) {
                            context$1$0.next = 3;
                            break;
                        }
                        throw new core.System.Exception('Please especify a `prefix` or a `dbconnection` in your vars.json file');
                    case 3:
                        prefix = prefix || '';
                        if (this._db) {
                            context$1$0.next = 13;
                            break;
                        }
                        context$1$0.next = 7;
                        return regeneratorRuntime.awrap(this.UserFunction('database.init'));
                    case 7:
                        DatabaseF = context$1$0.sent;
                        context$1$0.next = 10;
                        return regeneratorRuntime.awrap(DatabaseF.load());
                    case 10:
                        context$1$0.next = 12;
                        return regeneratorRuntime.awrap(DatabaseF.execute(this));
                    case 12:
                        this._db = context$1$0.sent;
                    case 13:
                        context$1$0.next = 15;
                        return regeneratorRuntime.awrap(this._db.table(prefix + name));
                    case 15:
                        return context$1$0.abrupt('return', context$1$0.sent);
                    case 16:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this);
        })
    });
}
exports.default = Database_User;