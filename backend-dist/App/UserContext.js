var VaravelNamespace = core.org.voxsoftware.Varavel;
var $mod$21 = core.VW.Ecma2015.Utils.module(require('url'));
var Fs = core.System.IO.Fs;
var $mod$22 = core.VW.Ecma2015.Utils.module(require('fs'));
var $mod$23 = core.VW.Ecma2015.Utils.module(require('path'));
var times = 0;
var $mod$24 = core.VW.Ecma2015.Utils.module(require('live-plugin-manager'));
var $mod$25 = core.VW.Ecma2015.Utils.module(require('os'));
var $mod$26 = core.VW.Ecma2015.Utils.module(require('child_process'));
var $mod$27 = core.VW.Ecma2015.Utils.module(require('semver'));
{
    var UserContext = function UserContext() {
        UserContext.$constructor ? UserContext.$constructor.apply(this, arguments) : UserContext.$superClass && UserContext.$superClass.apply(this, arguments);
    };
    Object.defineProperty(UserContext, '$constructor', {
        enumerable: false,
        value: function (args, idsite) {
            this.idSite = idsite;
        }
    });
    Object.defineProperty(UserContext, 'get', {
        enumerable: false,
        value: function (args, site) {
            UserContext.c = UserContext.c || {};
            if (!UserContext.c[site])
                UserContext.c[site] = new UserContext(args, site);
            return UserContext.c[site];
        }
    });
    Object.defineProperty(UserContext.prototype, 'getHomeDir', {
        enumerable: false,
        value: function () {
            var home = $mod$25.default.homedir();
            var kowi = $mod$23.default.join(home, 'kowi');
            if (!Fs.sync.exists(kowi)) {
                Fs.sync.mkdir(kowi);
            }
            kowi = $mod$23.default.join(kowi, 'user-context');
            if (!Fs.sync.exists(kowi))
                Fs.sync.mkdir(kowi);
            var paths1 = this.idSite.split('/');
            for (var i = 0; i < paths1.length; i++) {
                kowi = $mod$23.default.join(kowi, paths1[i]);
                if (!Fs.sync.exists(kowi))
                    Fs.sync.mkdir(kowi);
            }
            return kowi;
        }
    });
    Object.defineProperty(UserContext.prototype, 'getModeNpm', {
        enumerable: false,
        value: function () {
            return Varavel.Funcs.env('NPM_MODE', 0);
            var path = $mod$23.default.join(global.appDir, '..', 'npm');
            if (Fs.sync.exists(path))
                return path;
            return;
        }
    });
    Object.defineProperty(UserContext.prototype, 'require', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0(pack, version, file) {
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        context$1$0.next = 2;
                        return regeneratorRuntime.awrap(this.resolve(pack, version, file));
                    case 2:
                        context$1$0.t0 = context$1$0.sent;
                        return context$1$0.abrupt('return', require(context$1$0.t0));
                    case 4:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this);
        })
    });
    Object.defineProperty(UserContext.prototype, 'resolve', {
        enumerable: false,
        value: (typeof regeneratorRuntime != 'object' ? core.VW.Ecma2015.Parser : undefined, function callee$0$0(pack, version, file) {
            var home, packagefile, data, resolved, kowi, needreplace, stat, stat2, needcreate, r1, task, p, mode, manager;
            return regeneratorRuntime.async(function callee$0$0$(context$1$0) {
                while (1)
                    switch (context$1$0.prev = context$1$0.next) {
                    case 0:
                        home = process.env.HOME;
                        kowi = this.getHomeDir();
                        packagefile = $mod$23.default.join(kowi, 'package.json');
                        if (!version) {
                            version = pack.indexOf('@');
                            if (version >= 0) {
                                version = pack.substring(version + 1);
                                pack = pack.substring(0, version);
                                if (version == 'latest')
                                    version = '*';
                            } else {
                                version = '*';
                            }
                        }
                        if (Fs.sync.exists(packagefile)) {
                            context$1$0.next = 11;
                            break;
                        }
                        data = {
                            name: 'kowi-usercontext-' + this.idSite.replace(/\//ig, '_'),
                            dependencies: {}
                        };
                        data.dependencies[pack] = version;
                        needreplace = true;
                        needcreate = true;
                        context$1$0.next = 27;
                        break;
                    case 11:
                        stat = UserContext.stats[packagefile];
                        context$1$0.next = 14;
                        return regeneratorRuntime.awrap(Fs.async.stat(packagefile));
                    case 14:
                        stat2 = context$1$0.sent;
                        if (stat) {
                            if (stat2.mtime.getTime() <= stat.mtime.getTime())
                                data = stat;
                        }
                        if (data) {
                            context$1$0.next = 24;
                            break;
                        }
                        context$1$0.t0 = JSON;
                        context$1$0.next = 20;
                        return regeneratorRuntime.awrap(Fs.async.readFile(packagefile));
                    case 20:
                        context$1$0.t1 = context$1$0.sent;
                        data = context$1$0.t0.parse.call(context$1$0.t0, context$1$0.t1);
                        data.mtime = stat2.mtime;
                        UserContext.stats[packagefile] = data;
                    case 24:
                        data.dependencies = data.dependencies || [];
                        if (data.dependencies[pack]) {
                            if (data.dependencies[pack] != version) {
                                if (data.dependencies[pack].startsWith('^'))
                                    needreplace = !$mod$27.default.satisfies(data.dependencies[pack].substring(1), version);
                                else
                                    needreplace = true;
                            }
                        } else {
                            needreplace = true;
                        }
                        data.dependencies[pack] = version;
                    case 27:
                        if (file) {
                            file = $mod$23.default.join(pack, file);
                        } else {
                            file = pack;
                        }
                        resolved = $mod$23.default.join(kowi, 'node_modules', file);
                        if (needreplace) {
                            context$1$0.next = 41;
                            break;
                        }
                        context$1$0.prev = 30;
                        r1 = require.resolve(resolved);
                        if (!Fs.sync.exists(r1)) {
                            context$1$0.next = 34;
                            break;
                        }
                        return context$1$0.abrupt('return', r1);
                    case 34:
                        context$1$0.next = 41;
                        break;
                    case 36:
                        context$1$0.prev = 36;
                        context$1$0.t2 = context$1$0['catch'](30);
                        vw.info('ERROR:', context$1$0.t2);
                        if (!r1) {
                            context$1$0.next = 41;
                            break;
                        }
                        throw context$1$0.t2;
                    case 41:
                        if (!needcreate) {
                            context$1$0.next = 44;
                            break;
                        }
                        context$1$0.next = 44;
                        return regeneratorRuntime.awrap(Fs.async.writeFile(packagefile, JSON.stringify(data)));
                    case 44:
                        task = new core.VW.Task();
                        mode = this.getModeNpm();
                        if (!(mode === 0)) {
                            context$1$0.next = 52;
                            break;
                        }
                        manager = new $mod$24.PluginManager({
                            pluginsPath: $mod$23.default.join(kowi, 'node_modules'),
                            cwd: kowi,
                            hostRequire: function () {
                            }
                        });
                        context$1$0.next = 50;
                        return regeneratorRuntime.awrap(manager.install(pack, version));
                    case 50:
                        context$1$0.next = 59;
                        break;
                    case 52:
                        if (mode) {
                            p = $mod$26.default.spawn(process.argv[0], [
                                mode + '/bin/npm-cli.js',
                                'install',
                                pack + '@' + (version != '*' ? version : 'latest')
                            ], { cwd: kowi });
                        } else {
                            p = $mod$26.default.spawn('npm', [
                                'install',
                                pack + '@' + (version != '*' ? version : 'latest')
                            ], { cwd: kowi });
                        }
                        p.stdout.on('data', function (data) {
                            vw.warning(data.toString());
                        });
                        p.stderr.on('data', function (data) {
                            vw.error(data.toString());
                        });
                        p.on('error', function (er) {
                            task.exception = er;
                            task.finish();
                        });
                        p.on('exit', function () {
                            task.finish();
                        });
                        context$1$0.next = 59;
                        return regeneratorRuntime.awrap(task);
                    case 59:
                        return context$1$0.abrupt('return', resolved);
                    case 60:
                    case 'end':
                        return context$1$0.stop();
                    }
            }, null, this, [[
                    30,
                    36
                ]]);
        })
    });
    Object.defineProperty(UserContext.prototype, 'availableFolders', {
        enumerable: false,
        value: function () {
            return [
                $mod$23.default.join(process.env.HOME, 'kowi', 'user-context', this.idSite),
                $mod$23.default.join(global.appDir, 'node_modules')
            ];
        }
    });
}
UserContext.stats = {};
exports.default = UserContext;