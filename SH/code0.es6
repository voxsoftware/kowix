/*global core*/
import Child from 'child_process'
import Path from 'path'
require(__dirname + "/../../develop-tools")
var Fs= core.System.IO.Fs
class Code{
    
    async mail(){
    }
    
    
    async exec(cmd, args, options){
        var task= new core.VW.Task()
        var opt= {
            stdio:'inherit'
        }
        for(var id in options){
            opt[id]= options[id]
        }
        var p= Child.spawn(cmd, args, opt)
        p.on("exit", function(){
            task.finish()
        })
        p.on("error", function(er){
            console.error("Error in: ",cmd,args,er)
            task.finish()
        })
        return task 
    }
    
    
    
    async execute(){
        var envPath= Path.join(__dirname,"..", "..","kowix", "backend-dist", "env.dt")
        var config= new core.org.voxsoftware.Developing.Configuration(envPath)
        // FIRST GIT PULL ...
        try{
            
            await this.exec("git", ["reset", "--hard", "HEAD"], {
                cwd: Path.join(__dirname, "..")
            })
                
            await this.exec("git", ["pull"], {
                cwd: Path.join(__dirname, "..")
            })
            
            
            await this.exec("git", ["reset", "--hard", "HEAD"], {
                cwd: Path.join(__dirname, "..","..","varavel")
            })
                
            await this.exec("git", ["pull"], {
                cwd: Path.join(__dirname, "..","..","varavel")
            })
            
            
            await require(Path.join(__dirname, "code1.es6")).execute(config, this)
            
            
            
            
        }
        catch(e){
            await Fs.async.writeFile("/root/nginx/last_error", e.stack)
        }
    }
    
    
}

var e= new Code()
e.execute()