import Child from 'child_process'
import Path from 'path'
import Os from 'os'
import fs from 'fs'
var Fs= core.System.IO.Fs
class Code{
    
    
    async removeUploads(path){
        if(!Fs.sync.exists(path))
            return 
            
        var files= await Fs.async.readdir(path)
        var stat, ufile, file, mtime
        for(var i=0;i<files.length;i++){
            file= files[i]
            ufile= Path.join(path, file)
            try{
                stat= await Fs.async.stat(ufile)
                mtime= new Date(stat.mtime)
                mtime= mtime.getTime()
                if(stat.isFile()){
                    if(Date.now()-mtime>= (10*60*1000)){
                        await Fs.async.truncate(ufile, 0)
                        await Fs.async.unlink(ufile)
                    }
                }
                else {
                    await this.removeUploads(ufile)
                }
                    
            }catch(e){
                console.error("Error truncating: ", ufile, e)
            }
            
        }
    }
    
    async removeLogs(path, normaluser){
        if(!Fs.sync.exists(path))
            return 
            
        var files= await Fs.async.readdir(path)
        var stat, ufile, file, gb5
        gb5= 5*1024*1024*1024
        for(var i=0;i<files.length;i++){
            file= files[i]
            ufile= Path.join(path, file)
            if(file.indexOf(".log")>=0){
                try{
                    stat= await Fs.async.stat(ufile)
                    if(stat.isFile() && stat.size>gb5){
                        //if(!normaluser)
                            await Fs.async.truncate(ufile, 0)
                        /*else 
                            await this.exec("su", ["james","-c", "truncate -s0 " + ufile])
                        */
                    }
                    
                    if(file.indexOf(".gz")>=0 )
                        await Fs.async.unlink(ufile)
                }catch(e){
                    console.error("Error truncating: ", ufile, e)
                }
            }
        }
    }
    
    
    async makeRequest(uri, body, method, site){
        
        
        var url= "http://127.0.0.1:" + this.config.get("SERVER_PORT") + "/site/"+(site||"kowi")+"/" + uri
        var req= new core.VW.Http.Request(url)
        req.method=method || 'POST'
        req.body= body
        req.contentType="application/json"
        var response= await req.getResponseAsync()
        if(!response || !response.body){
            throw new core.System.Exception("Empty response")
        }
        
        
        if(typeof response.body=="string"){
            response.body= JSON.parse(response.body)
        }
        
        if(response.body && response.body.error){
            var e= new core.System.Exception(response.body.error.message)
            for(var id in response.body.error){
                e[id]= response.body.error[id]
            }
            throw e
        }
        
        return response.body
    }
    
    
    async exec(cmd, args, options){
        var task= new core.VW.Task()
        var opt= {
            stdio:'inherit'
        }
        for(var id in options){
            opt[id]= options[id]
        }
        if(!opt.cwd){
            opt.cwd= Path.join(__dirname, "..", "..")
        }
        task.result={}
        var p= Child.spawn(cmd, args, opt)
        p.on("error", function(er){
            console.error("Error in: ",cmd,args,er)
        })
        if(p.stdout){
            p.stdout.on("data", function(d){
                task.result.stdout=task.result.stdout||''
                task.result.stdout+= d.toString("utf8")
            })
        }
        if(p.stderr){
            p.stderr.on("data", function(d){
                task.result.stderr=task.result.stderr||''
                task.result.stderr+= d.toString("utf8")
            })
        }
        p.on("exit", function(){
            task.finish()
        })
        return task 
    }
    
    async compileEs6(path){
        var files=await Fs.async.readdir(path)
        
        var ufile,ufile2,parser,ast,stat,stat2
        var stat 
        
        var parser=new core.VW.Ecma2015.Parser()
        for(var i=0;i<files.length;i++){
            ufile=Path.join(path,files[i])
            stat= await Fs.async.stat(ufile)
            if(stat.isDirectory()){
                await this.compileEs6(ufile)
            }
            else if(ufile.endsWith(".es6")){
                try{
                    
                    ufile2=Path.join(path,Path.basename(files[i],".es6")+".js")
                    if(Fs.sync.exists(ufile2)){
                        stat2=await Fs.async.stat(ufile2)
                        if(stat.mtime.getTime()<=stat2.mtime.getTime())
                            continue 
                    }
                    ast=parser.parse(await Fs.async.readFile(ufile,'utf8'))
                    await Fs.async.writeFile(ufile2,ast.code)
                    console.info("File converted:",ufile)
                }catch(e){
                    console.error("Error compiling:",ufile,e)
                }
            }
        }
    }
    
    async copiarSites(){
        var nginx= Path.join(__dirname,"..", "..", "nginx")
        if(!Fs.sync.exists(nginx))
            return 
            
            
        var sites= Path.join(nginx,"sites-enabled")
        var currentVersion= Path.join(nginx,"version")
        var installed= "/root/nginx/version"
        var installedversion,actionfile, code, file,up, file1, file2,content,content2, y
        currentVersion= await Fs.async.readFile(currentVersion,'utf8')
        
        
        
        var fs1= await Fs.async.readdir(nginx)
        var f1,file3,file4, res,times, stat
        for(var i=0;i<fs1.length;i++){
            
            f1=fs1[i]
            file3= Path.join(nginx,f1)
            
            if(f1.startsWith("action.")){
                file4= "/root/nginx/"+f1
                
                if(!Fs.sync.exists(file4)){
                    // get actions ...
                    //actionfile= Path.join(nginx,"action." + currentVersion + ".json")
                    code= require(file3)
                    if(code.files && code.files.length){
                        // replace files ...
                        for(var i=0;i<code.files.length;i++){
                            file=code.files[i]
                            file1= Path.join(sites, file)
                            file2= "/root/nginx/sites-enabled/"+file
                            await Fs.async.writeFile(file2, await Fs.async.readFile(file1))
                            up= true
                        }
                    }
                    if(code.site_files && code.site_files.length){
                        for(var i=0;i<code.site_files.length;i++){
                            file=code.site_files[i]
                            file1= Path.join(sites, file)
                            file2= "/root/nginx/sites-enabled/"+file
                            content=''
                            if(Fs.sync.exists(file2)){
                                content= await Fs.async.readFile(file2, 'utf8')
                                y=content.indexOf("listen 443 ssl; # managed by Certbot")
                                if(y>=0){
                                    content=content.substring(y).split(/\r\n|\r|\n/)
                                    content=content.filter((a)=> a &&a.trim().endsWith("# managed by Certbot"))
                                    content=content.join("\n")
                                }
                                else{
                                    content=''
                                }
                            }
                            content2= await Fs.async.readFile(file1, 'utf8')
                            content2= content2.replace("# LETSENCRYPT SECTION", content)
                            await Fs.async.writeFile(file2, content2)
                            up= true
                        }
                    }
                    if(code.delete_files && code.delete_files.length){
                        // replace files ...
                        for(var i=0;i<code.delete_files.length;i++){
                            file=code.delete_files[i]
                            file2= "/root/nginx/sites-enabled/"+file
                            if(Fs.sync.exists(file2))
                                await Fs.async.unlink(file2)
                            up= true
                        }
                    }
                    /*
                    if(up){
                        file2="/root/nginx/sites-enabled/_kowi_upstream_local"
                        if(!Fs.sync.exists(file2)){
                            file1= Path.join(sites, "_kowi_upstream_local")
                            await Fs.async.writeFile(file2, await Fs.async.readFile(file1))
                        }
                        
                    }*/
                    await Fs.async.writeFile(file4, await Fs.async.readFile(file3))
                }
            }
            
            
        }
        
        
        file2='/root/nginx/bw-info'
        if(!Fs.sync.exists(file2)){
            await Fs.async.writeFile(file2, "kowix-bw")
        }
        else{
            stat= await Fs.async.stat(file2)
            
            /*
            if(stat.mtime< (Date.now() - (3.2*3600*1000))){
                content= await Fs.async.readFile(file2,'utf8')
                await this.exec("pm2", ["reload", content])
                await core.VW.Task.sleep(1000)
                await Fs.async.writeFile(file2, content=="kowix-bw"?"kowix-bw-2":"kowix-bw")    
            }*/
            
            
        }
        
        
        if(up){
            
            /*
            file2="/root/nginx/sites-enabled/_kowi_upstream_local"
            if(!Fs.sync.exists(file2)){
                file1= Path.join(sites, "_kowi_upstream_local")
                await Fs.async.writeFile(file2, await Fs.async.readFile(file1))
            }*/
            
        }
        
        
        
        
        if(up){
            // restart nginx ...
            times=0
            while(times<5){
                res=await this.exec("/usr/sbin/service", ["nginx", "reload"], {
                    //stdio:'pipe'
                })
                if((res.stdout && res.stdout.indexOf("failed")>=0) || (res.stderr && res.stderr.indexOf("failed")>=0))   {
                    await core.VW.Task.sleep(500)
                    times++
                }
                else{
                    break 
                }
            }
            file2="/root/nginx/nginx_reloaded"
            await Fs.async.writeFile(file2, (new Date()).toGMTString())
        }
        
        
        
    }
    
    env(obj){
        var o= {}
        for(var id in process.env){
            o[id]=process.env[id]
        }
        for(var id in obj){
            o[id]=obj[id]
        }
        return o
    }
    async execute(config){
        
        this.config= config
        // IF NECESARRY REOPEN PROCESS ...
        var path=  Path.join(__dirname, "..", "..", "kowix", "manualstart.js")
        var path2= Path.join(__dirname, "..", "..", "kowix-bw", "manualstart.js")
        var path3= Path.join(__dirname, "..", "..", "kowix-bw-2", "manualstart.js")
        
        
        var path4='/root/maxmem'
        var maxmem= "450M"
        if(Fs.sync.exists(path4)){
            maxmem= Fs.sync.readFile(path4,'utf8')
            maxmem= maxmem.trim()
        }
        
        
        path4='/root/cpu'
        var numcpus= Math.max(Os.cpus().length,2) //Math.max(Os.cpus().length,2)
        var controls
        if(Fs.sync.exists(path4)){
            numcpus= Fs.sync.readFile(path4,'utf8')
            numcpus= parseInt(numcpus.trim())
        }
        
        try{
            
            if(config.DEV_MODE!=1){
                await this.exec("pm2", ["start", "-i", numcpus.toString(), path, "--name",  "kowix", "--max-memory-restart", maxmem])
                await this.exec("pm2", ["start", "-i", numcpus.toString(), path, "--name",  "kowix-bw", "--max-memory-restart", maxmem], {env:this.env({SERVER_PORT: 3017})})
                await this.exec("pm2", ["start", "-i", numcpus.toString(), path, "--name",  "kowix-bw-2", "--max-memory-restart", maxmem], {env:this.env({SERVER_PORT: 3019})})
                
                if(Fs.sync.exists("/root/kowix.control")){
                    controls= await Fs.async.readFile("/root/kowix.control",'utf8')
                    controls= parseInt(controls)
                    for(var i=0;i<controls;i++)
                        await this.exec("pm2", ["start", "-i", 1, path, "--name",  "kowix.control." + i, "--max-memory-restart", maxmem], {env:this.env({SERVER_PORT: 4001+i})})
                }
            }
            
    
            var path3= "/root/nginx"
            if(!Fs.sync.exists(path3)){
                Fs.sync.mkdir(path3)
            }
            if(!Fs.sync.exists(Path.join(path3,"sites-enabled"))){
             
                //await this.exec("cp", ["-a", "/etc/nginx/sites-enabled", "/root/nginx"])
                //await this.exec("umount", ["/etc/nginx/sites-enabled"])
                //await this.exec("ln", ["-s","/root/nginx/sites-enabled","/etc/nginx/sites-enabled"])
            }
            if(!Fs.sync.exists(Path.join(path3,"letsencrypt"))){
                await this.exec("cp", ["-a", "/etc/letsencrypt", "/root/nginx"])
                await this.exec("rm", ["-r", "/etc/letsencrypt"])
                await this.exec("ln", ["-s","/root/nginx/letsencrypt","/etc/letsencrypt"])
                
            }
            
            
            try{
                if(!Fs.sync.exists("/root/nginx/c1")){
                    var content= await Fs.async.readFile("/etc/nginx/nginx.conf","utf8")
                    content=content.replace("include /etc/nginx/sites-enabled/*;", "include /root/nginx/sites-enabled/*;")
                    await Fs.async.writeFile("/etc/nginx/nginx.conf", content)
                    await Fs.async.writeFile("/root/nginx/c1","true")
                }
            }catch(e){
                vw.error("Error configuring nginx: ", e)
            }
            
           
            
            // REMOVE LOGS 
            await this.removeLogs("/var/log/nginx")
            await this.removeLogs("/var/log/mongodb")
            await this.removeLogs(process.env.HOME+"/.pm2/logs", true)
            
            
            var reload, reloadMain,c1,c2
            path=  Path.join(__dirname, "..", "..", "kowix", "version")
            path2=  Path.join("/root", "nginx", "kowix-version")
            
            
            if(Fs.sync.exists(path)){
                c1= Fs.sync.readFile(path,'utf8')
                if(c1){
                    if(Fs.sync.exists(path2)){
                        c2= Fs.sync.readFile(path2,'utf8')
                    }
                    if(c2!=c1){
                        reloadMain= true 
                        Fs.sync.writeFile(path2, c1)
                    }
                }
            }
            
            
            
            try{
                vw.warning("HERE -------")
                var code= await this.makeRequest("api/function/c/license.get.domains", {}, "POST", "kowix")
                if(code && code.code){
                    
                    if(this.config.get("KOWI_CODE")!= code.code){
                        this.config.set("KOWI_CODE",code.code)
                        await this.config.save()
                        
                        // reload all 
                        reload= true
                        
                    }
                    
                }
            }catch(e){
                console.error("REQUEST ERROR:",e)
            }
                
            try{
                await this.makeRequest("api/function/c/cron.0tasks", {}, "POST", "kowix")
            }catch(e){
                //console.error("REQUEST ERROR KOWIX:",e)
            }
           
           
        }
        catch(e){
            await Fs.async.writeFile("/root/nginx/last_error", e.stack)
        }
        
        
        if(reload){
            await this.exec("pm2",["reload","kowix"])
            await this.exec("pm2",["reload","kowix-bw"])
            await this.exec("pm2",["reload","kowix-bw-2"])
        }
        else if(reloadMain){
            await this.exec("pm2",["reload","kowix"])
        }
        
        
         // COPIAR NEW SITES 
        try{
            await this.copiarSites()
        }catch(e){
            console.error("Sites copy error:", e)
            await Fs.async.writeFile("/root/nginx/last_error", e.stack)
        }
    }
    
    
}
module.exports.execute= function(){
    var c= new Code()
    return c.execute.apply(c,arguments)
}
