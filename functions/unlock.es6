
var F= async function(body){
    var port= require(__dirname  + "/../0port.js")
    var locks = global.publicContext.Locks= global.publicContext.Locks||{}
    
    if(port<4000){
        // este método no se puede ejecutar en un puerto menor al 4000
        throw global.Exception.create("Failed executing locking").putCode("FAILED_PORT")
    }
    
    
    try{
        var lock= locks[body.code]
        if(lock && lock.cid == body.cid){
            delete locks[body.code]
        }
    }
    catch(e){
        throw e 
    }finally{
        
    }
 
}