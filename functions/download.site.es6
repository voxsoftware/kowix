/*global core*/
import Path from 'path'
var Fs= core.System.IO.Fs


var F= async function(body){
    
    var global= F.global 
    global.context.response.setTimeout(180000)
    
    var port= require(__dirname  + "/../0port.js")
    if(port<4000){
        
        var req= new core.VW.Http.Request("http://127.0.0.1:4001/site/kowix/api/function/c/download.site")
        req.method='POST'
        req.body=body 
        req.contentType='application/json'
        try{
            var response= await req.getResponseAsync()
        }catch(e){
            
        }finally{
            req.innerRequest && (delete req.innerRequest.callback)
        }
        return 
        
    }
    
    
    
    global.publicContext.loadSites= global.publicContext.loadSites||{}
    
    while(global.publicContext.loadSites[body.site]){
        await core.VW.Task.sleep(20)
    }
    
    try{
        global.publicContext.loadSites[body.site]= true 
        var appFolder= __dirname + "/../../domains_v2"
        var appFolder1= __dirname + "/../../sites_v2"
        if(!Fs.sync.exists(appFolder))
            await Fs.async.mkdir(appFolder)
        
        var file= Path.join(appFolder, body.site)
        var file1= Path.join(appFolder1, body.site)
        if(Fs.sync.exists(file1)){
            return 
        }
        
        var url1='http://kowi.kodhe.com/site/'
        var url= url1+body.site + "/api/function/c/app.content?key="+encodeURIComponent(body.key)
        
        console.log("URL for download:", url)
        await global.UserFunction("app.cache").invoke({
            name: body.site,
            url
        })
        
        if(Fs.sync.exists(file1)){
            return 
        }
        await Fs.async.writeFile(file, "sites_v2/"+body.site)
    }catch(e){
        throw e 
    }
    finally{
        global.publicContext.loadSites[body.site]= false
    }
}

module.exports=function(global){
	F.global= global 
	return F
}