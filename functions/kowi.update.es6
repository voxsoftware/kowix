module.exports= function(global){
    return async function(){
        var table= await global.Database.table("k_manager")
        var info= await table.findOne({
            type:'updating'
        })
        if(!info){
            await table.insert({
                type:'updating',
                value: Date.now()
            })
        }
        else{
            await table.update({
                type:'updating'
            }, {
                $set:{
                    value: Date.now()
                }
            })
        }
    }
}