/*global core*/

import Path from 'path'
var Fs= core.System.IO.Fs
import fs from 'fs'
import Zlib from 'zlib'
var F= async function(body){
    var global= F.global 
    var url= body.url
    
    var fsi= require("fs-extra")
    var cr= function(func){
		return function(){
			var task= new core.VW.Task()
			var args= Array.prototype.slice.call(arguments)
			args.push(function(er, data){
				if(er)
					task.exception= er 
				task.result= data
				return task.finish()
			})
			func.apply(fsi, args)
			return task 
		}
	}
    var fse={
    	copy: cr(fsi.copy),
    	mkdir: cr(fsi.mkdir),
    	rename: cr(fsi.rename),
    	remove: cr(fsi.remove)
    }
    
    try{
    	global.publicContext.is_caching= true 
    	
    	global.publicContext.caching=global.publicContext.caching||{}
    	if(global.publicContext.caching[body.name])
    		return 
    	
    	global.publicContext.caching[body.name]= true
    	
    	
    	
	    
	    var appFolder= __dirname + "/../../sites_v2"
	    if(!Fs.sync.exists(appFolder))
	    	await Fs.async.mkdir(appFolder)
	    
	    var versiondir= __dirname + "/../../old_versions"
	    
	    
	    
	    var tapp= Path.join(appFolder, global.uniqid())
	    var tapp2= Path.join(appFolder, body.name)
	    
	    
	    
	    
	    var currentConfigFile= Path.join(tapp2, "__app__cache.json")
	    var config ,version


	    var tempfile= Path.join(global.UserContext.getHomeDir(), global.uniqid())
	    if(Fs.sync.exists(currentConfigFile)){
	        config= await Fs.async.readFile(currentConfigFile,'utf8')
	        config= JSON.parse(config)
	        if(url)
	        	config.url= url
	    }
	    else{
	        config= {
	            url, 
	            updated: 0,
	            version: '0'
	        }
	    }
	    
	    
	    var req= new core.VW.Http.Request(config.url)
	    
	    req.headers["accept-encoding"]='gzip'
	    req.userAgent='Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.81 Safari/537.36'
	    var st, task, updated=0
	    task= new core.VW.Task()
	    try{
	        
	        req.method='POST'
	        req.body= {
	            updated:config.updated
	        }
	        req.analizeResponse= false
	        req.beginGetResponse()
	        
	        st= fs.createWriteStream(tempfile)
	        req.innerRequest.on("response",function(r){
	            updated= parseInt(r.headers["x-app-updated"])
	            version= r.headers["x-app-version"]
	            
	            if(r.headers["content-encoding"] &&r.headers["content-encoding"]=="gzip"){
	            	var gzip = Zlib.createGunzip()
	            	req.innerRequest.pipe(gzip)
	            	gzip.on("error",function(e){
	            		task.exception= e 
	            		task.finish()
	            	})
	            	gzip.pipe(st)
	            }
	            else{
	            	req.innerRequest.pipe(st)
	            }
	        })
	        
	        
	        
	        
	        
	        st.on("finish", function(){
	            task.finish()
	        })
	        st.on("error", function(er){
	            task.exception= er 
	            task.finish()
	        })
	        req.innerRequest.on("error", function(er){
	            task.exception= er 
	            task.finish()
	        })
	        await task 
	        
	        
	    }catch(e){
	        throw e
	    }finally{
	        if(req.innerRequest)
	            delete req.innerRequest.callback
	    }
	    
	    
	    if(!Fs.sync.exists(tempfile)){
	        throw global.Exception.create("Updating from url failed")
	    }
	    
	    if(updated<= config.updated)
	        return 
	        
	        
	    
	    var stream, stream2, breader, len1, file, len2, len3, buf1, size, buf2, len4, cstat
	    
	    
	    if(!Fs.sync.exists(tapp)){
	        Fs.sync.mkdir(tapp)
	    }
	    
	    
	    try{
	        
	        stream= new core.System.IO.FileStream(tempfile, core.System.IO.FileMode.Open, core.System.IO.FileAccess.Read)
	        stream.position=0 
	        breader= new core.System.IO.BinaryReader(stream)
	        
	        size= await Fs.async.stat(tempfile)
	        size= size.size
	        
	        while(stream.position<size){
	            len1= await breader.readInt32Async()
	            len3= await breader.readInt32Async()
	            //file= await breader.readStringAsync()
	            
	            buf2= new Buffer(len3)
	            len2= await stream.readAsync(buf2,0,buf2.length)
	            if(len2!= buf2.length)  
	                throw global.Exception.create("Updating from url failed. File: " + file  + ", failed getting data")
	            
	            
	            file= buf2.toString() 
	            len4= await breader.readInt32Async()
	            buf2= new Buffer(len4)
	            len2= await stream.readAsync(buf2,0,buf2.length)
	            if(len2!= buf2.length)  
	                throw global.Exception.create("Updating from url failed. File: " + file  + ", failed getting stat")
	             
	            
	            cstat= JSON.parse(buf2.toString())
	               
	            
	            
	            if(file.startsWith("\0")){
	                file= file.substring(1)
	                
	                if(!Fs.sync.exists(Path.join(tapp, file)))
	                    Fs.sync.mkdir(Path.join(tapp, file))
	                
	                
	            }
	            else{
	                
	            
	                
	                buf1= new Buffer(len1)
	                len2 = await stream.readAsync(buf1,0,buf1.length)
	                if(len2!= buf1.length)  
	                    throw global.Exception.create("Updating from url failed. File: " + file  + ", failed getting data")
	                
	                stream2= new core.System.IO.FileStream(Path.join(tapp, file), core.System.IO.FileMode.OpenOrCreate, core.System.IO.FileAccess.Write)
	                await stream2.writeAsync(buf1, 0, buf1.length)
	                stream2.close()
	                stream2= null
	            }
	        }
	        
	        
	        
	        
	    }catch(e){
	        
	        if(Fs.sync.exists(tapp))
	            await fse.remove(tapp)
	            
	            
	        throw e 
	    }
	    finally{
	        if(stream)
	            stream.close()
	           
	        if(stream2)
	            stream2.close()
	    }
	    
	    
	    if(!Fs.sync.exists(versiondir))
	        Fs.sync.mkdir(versiondir)
	        
	    if(Fs.sync.exists(tapp2)){
	        await Fs.async.writeFile(Path.join(tapp2, "__app__disabled"), "1")
	        
	        
	        var newdir= Path.join(versiondir, body.name + "_v" + (config.version ||""))
	        if(Fs.sync.exists(newdir))
	            await fse.remove(newdir)
	        
	        
	        await fse.copy(tapp2, newdir)
	        await fse.remove(tapp2)
	    }
	    
	    await Fs.async.rename(tapp, tapp2)
	    
	    
	    config.updated= updated
	    config.version= version
	    await Fs.async.writeFile(currentConfigFile,core.safeJSON.stringify(config))
	    
	    
	    return {
	        path: tapp,
	        updated, 
	        name: body.name
	    }
    }catch(e){
    	throw e
    }
    finally{
    	
    	global.publicContext.is_caching= false
    	global.publicContext.caching[body.name]= false
    	if(Fs.sync.exists(tapp))
    		await fse.remove(tapp)
    	
    	if(Fs.sync.exists(tempfile))
    		await Fs.async.unlink(tempfile)
    }
}

module.exports= function(global){
    F.global= global 
    return F 
}