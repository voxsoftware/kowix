/*global core*/
import Child from 'child_process'
var Global
import Path from 'path'
var Fs= core.System.IO.Fs

class Domains {
	
	
	static exec(cmd,args, options){
		var task= new core.VW.Task()
		task.result={}
		var p= this.p= Child.spawn(cmd,args,options)
		p.stdout.on("data",function(buf){
			task.result.stdout=task.result.stdout||[]
			task.result.stdout.push(buf)
		})
		p.on("error",function(er){
			task.exception= er
			task.finish()
		})
		p.on("exit",function(){
			if(task.result.stdout)
				task.result.stdout= Buffer.concat(task.result.stdout)
			task.finish()
		})
		return task 
	}
	
	
	async static compileEs6(path){
        var files=await Fs.async.readdir(path)
        
        var ufile,ufile2,parser,ast,stat,stat2
        parser=new core.VW.Ecma2015.Parser()
        for(var i=0;i<files.length;i++){
        	ufile=Path.join(path,files[i])
        	stat=await Fs.async.stat(ufile)
        	if(stat.isDirectory()){
        		await this.compileEs6(ufile)
        	}
            else if(files[i].endsWith(".es6")){
                try{
                    
                    ufile2=Path.join(path,Path.basename(files[i],".es6")+".js")
                    if(Fs.sync.exists(ufile2)){
                        
                        stat2=await Fs.async.stat(ufile2)
                        if(stat.mtime.getTime()<=stat2.mtime.getTime())
                            continue 
                    }
                    ast=parser.parse(await Fs.async.readFile(ufile,'utf8'))
                    await Fs.async.writeFile(ufile2,ast.code)
                    console.info("File converted:",ufile)
                }catch(e){
                    console.error("Error compiling:",ufile,e)
                }
            }
        }
    }
    
    
	
	
	async static main(body){
		var req
		try{
			req= new core.VW.Http.Request("http://kowi.kodhe.com/site/kowi/api/function/c/license.domains")
			var response=await req.getResponseAsync()
			if(!response.body){
				throw new core.System.Exception("Failed get data")
			}
			if(typeof response.body=="string")
				response.body=JSON.parse(response.body)
			
			
			if(response.body && response.body.error){
				throw response.body.error 
			}
			
			
			var project, folder, dirname, domains, domainFolder, content, f 
			domainFolder= Path.join(process.env.HOME, "projects","domains")
			
			
			if(response.body){
				
				
				// git pull projects ...
				for(var i=0;i<response.body.projects.length;i++){
					project= response.body.projects[i]
					dirname= Path.join(process.env.HOME, "projects")
					folder= Path.join(dirname, project.name)
					
					if(!Fs.sync.exists(folder)){
						var task= this.exec("git", ["clone", project.git, project.name], {
							cwd: dirname
						})
						vw.warning("cloning project: ", project)
						if(project.password){
							var p= this.p 
							while(!task.result.stdout){
								if(task.exception)
									break 
								await core.VW.Task.sleep(100)
							}
							p.stdin.write(project.password+"\n")	
						}
						await task 
					}
					else{
						
						await this.exec("git", ["reset","--hard","HEAD"], {
							cwd: folder
						})
						
						var task= this.exec("git", ["pull"], {
							cwd: folder
						})
						if(project.password){
							var p= this.p 
							while(!task.result.stdout){
								if(task.exception)
									break 
								await core.VW.Task.sleep(100)
							}
							p.stdin.write(project.password+"\n")	
						}
						await task 
					}
					
					
					if(Fs.sync.exists(folder)){
						if(project.branch){
							await this.exec("git", ["checkout", project.branch], {
								cwd: folder
							})
						}
						
						this.compileEs6(Path.join(folder,"functions"))
						
						domains=Path.join(folder, "domains")
						if(Fs.sync.exists(domains)){
							content= await Fs.async.read(domains, 'utf8')
							content= content.split("\n")
							for(var y=0;y<content.length;y++){
								if(!Fs.sync.exists(domainFolder)){
									Fs.sync.mkdir(domainFolder)
								}
								
								f= Path.join(domainFolder, content[y])
								if(!Fs.sync.exists(f))
									await Fs.async.writeFile(f, project.name)
							}
						}	
						
					}
					
				}
				
				for(var i=0;i<response.body.projects.length;i++){
					project= response.body.projects[i]
					try{
						await this.makeRequest("api/function/c/cron.0tasks", {}, "GET", project.name )
					}catch(e)
					{}
				}
				
				
				// GENERATE KOWI_CODE  A PARTIR DE LA IP 
				var code= response.body.ip.replace(/\./ig,function(){return "0"})
				code= parseInt(code).toString(24).toUpperCase()
				code= `IP_${code}`
				return {
					code
				}
				
			}
			
		}catch(e){
			throw e
		}
		finally{
			if(req&&req.innerRequest)
				delete req.innerRequest.callback
		}
	}
	
	async static makeRequest(uri, body, method, site){
        try{
	        var url= "http://127.0.0.1:" + (process.env.SERVER_PORT || Varavel.Funcs.env("SERVER_PORT")) + "/site/"+(site||"kowix")+"/" + uri
	        var req= new core.VW.Http.Request(url)
	        req.method=method || 'POST'
	        req.body= body
	        req.contentType="application/json"
	        var response= await req.getResponseAsync()
	        if(!response || !response.body){
	            throw new core.System.Exception("Empty response")
	        }
	        
	        
	        if(typeof response.body=="string"){
	            response.body= JSON.parse(response.body)
	        }
	        
	        if(response.body && response.body.error){
	            var e= new core.System.Exception(response.body.error.message)
	            for(var id in response.body.error){
	                e[id]= response.body.error[id]
	            }
	            throw e
	        }
	        
	        return response.body
        }catch(e){
        	throw e
        }finally{
        	if(req&&req.innerRequest)
        		delete req.innerRequest.callback
        }
    }
    
	
	
	
}


module.exports=function(global){
	Global= global 
	return Domains.main.bind(Domains)
}