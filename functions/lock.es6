
var F= async function(body){
    var port= require(__dirname  + "/../0port.js")
    var locks = global.publicContext.Locks= global.publicContext.Locks||{}
    var locksw= global.publicContext.LocksW= global.publicContext.LocksW||{}
    
    
    if(port<4000){
        // este método no se puede ejecutar en un puerto menor al 4000
        throw global.Exception.create("Failed executing locking").putCode("FAILED_PORT")
    }
    
    
    var time=Date.now()
    try{
        if(locksw[body.code]===undefined)
            locksw[body.code]= 0
        
        locksw[body.code]++
        
        if(locksw[body.code]>200)
            throw global.Exception.create("Failed locking. There are many pending locks").putCode("LOCK_EXCEEDED")
        
        while(locks[body.code]){
            if(body.timeout + time< Date.now())
                throw global.Exception.create("Failed locking resource").putCode("LOCK_TIMEOUT")
            
            await core.VW.Task.sleep(20)
        }
        locks[body.code]= {
            cid:  body.cid,
            time: Date.now(),
            maxLockedTime: body.maxLockedTime
        }
    }
    catch(e){
        throw e 
    }finally{
        locksw[body.code]--
    }
 
}