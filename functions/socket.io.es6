/*global core*/
var F


F=async function(body){
	var server= core.org.voxsoftware.Varavel.Project.Application.current.tcpServer.$httpServer.$server.$server
	var global= F.global
	
	var socketio= await global.UserContext.require("socket.io","2.1.1")
	var io
	io= global.publicContext.socketIO
	
	if(!io){
		io= socketio({
		  serveClient: false,
		})
		global.publicContext.socketIO=io
		
		io.on('connection', function(client){
			
			var attach=async function(event, data){
				
				
				console.info("Socket::",event,data)
				
				if(client.attached)
					return 
					
					
				if(data && data.site){
					
					client.attached= true
					
					/*
					//  execute function as site 
					var task= global.context.ExecuteFunctionAsSite(data.site, "socket.io.client", {
						event,
						client,
						data
					})
					*/
					
					var context= await global.getSiteContext(data.site)
					try{
						return await context.userFunction("socket.io.client").invoke({
							event, 
							client,
							data
						})
					}catch(e){
						try{client.close()}catch(e){}
					}
					
					/*
					if(task && task.catch){
						task.catch(function(er){
							try{client.close()}catch(e){}
						})
					}
					
					return task */
				}
			}
			client.on("event", attach.bind(attach,"event"))
			client.on("attach", attach.bind(attach, "attach"))
			
			client.on("disconnect", function(){
				//client.removeAllListeners()
				client=null
			})
			
			
		});
	}
	if(!io._atached){
		io.attach(server)
		io._atached= true 
	}
	
	
	if(!body|| !body.internallcall){
		await global.context.continueAsync()
		var task= new core.VW.Task()
		global.context.response.on("finish",task.finish.bind(task ))
		await task 
	}
	
	
}

module.exports=function(global){
	F.global=global 
	return F
}